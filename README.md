## About project

Naves runs client-written code, so the user does not have to maintain and set up the server-side of the application. We provide a serverless solution that allows you to run code without provisioning or managing servers, maintaining event integrations and runtime management. We enable storing and editing of data as well as sharing it with others. Within the Naves box, the data is immediately accessible. We offer a variety of subscription programs that provide you with individualized service tailored to your specific needs.

## Project status

Project is currently in development mode.

## Installation and setup instructions

You will need `node` and `npm` installed to serve the application.
Clone down the repository.
Run `npm install` in terminal to install dependencies needed.
To run tests, type `npm test` command.
To serve the application on localhost, run `npm start` . If the browser doesn't open automatically, navigate to `http://localhost:3000/`.
To run storybook, type `npm run storybook`.

## Technologies used
Front-end part of the project is implemented in React framework with Typescript as a programming language.
For styling we are using Sass as a technology.

## Server
Back-end part of the application is not live yet. Source code can be found at `https://bitbucket.org/pavlejacovic/naves_repository_be/src/main/`;