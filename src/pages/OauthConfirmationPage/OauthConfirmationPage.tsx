import React from 'react';
import { useHistory } from 'react-router-dom';
import LocalStorageService from '../../services/local-storage.service';

const OauthConfirmationPage: React.FC = () => {
  const history = useHistory();
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const loginToken: string | null = urlParams.get('token');
  if (loginToken) {
    LocalStorageService.setToken(loginToken);
    history.push('/');
  } else {
    history.push('/login');
  }
  return null;
};

export default OauthConfirmationPage;
