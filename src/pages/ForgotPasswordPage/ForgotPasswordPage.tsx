import React, { useState, FormEvent } from 'react';
import { useHistory } from 'react-router-dom';
import { UserInfo } from '../../models/user.interface';
import { EMAIL_REQUIRED, INVALID_EMAIL_FORMAT } from '../../utils/constants/validationMessages/validation-messages';
import isValidEmail from '../../utils/validations/inputValidationsForEmail/inputValidationsForEmail';
import AlertMessage, { AlertMessageStyle } from '../../components/AlertMessage/AlertMessage';
import { Button, ButtonStyle } from '../../components/Button/Button';
import styles from './ForgotPasswordPage.module.scss';
import panelStyles from '../../utils/styles/Panel.module.scss';
import InputField from '../../components/InputField/InputField';
import AuthenticationService from '../../services/authentication.service';

const ForgotPasswordPage: React.FC = (): JSX.Element => {
  const history = useHistory();
  const [email, setEmail] = useState<string>('');
  const [emailRequiredMessage, setEmailRequiredMessage] = useState<string>('');
  const [invalidEmailFormatMessage, setInvalidEmailFormatMessage] = useState<string>('');
  const [statusCode, setStatusCode] = useState<number | null>(null);
  const [messageForClient, setMessageForClient] = useState<string>('Enter your email, and receive a password reset link.');

  const backToLogin = (): void => history.push('/login');

  const resetForm = (): void => {
    setInvalidEmailFormatMessage('');
    setEmailRequiredMessage('');
  };

  const handleOnChangeInput = (event: React.ChangeEvent<HTMLInputElement>): void => {
    resetForm();
    setEmail(event.target.value);
  };
  const handleSendEmailLinkSubmit = (e: FormEvent<HTMLFormElement>): void => {
    e.preventDefault();
    if (email) {
      if (!isValidEmail(email)) {
        setInvalidEmailFormatMessage(INVALID_EMAIL_FORMAT);
        return;
      }
      const user: UserInfo = { email };
      AuthenticationService.forgotPassword(user).then((response) => {
        setStatusCode(response.status);
        setMessageForClient(response.data);
      });
    } else {
      setEmailRequiredMessage(EMAIL_REQUIRED);
    }
  };

  return (
    <form className={panelStyles['Panel']} onSubmit={handleSendEmailLinkSubmit}>
      <p className={styles['ForgotPasswordPage-Message']}>{messageForClient}</p>
      {statusCode !== 200
        && (
          <>
            <div className={panelStyles['Panel-Field']}>
              <InputField
                id="email"
                name="email"
                type="text"
                placeholder="Enter email.."
                value={email}
                inputWarning={emailRequiredMessage || invalidEmailFormatMessage}
                onChange={handleOnChangeInput}
              />
            </div>
            {emailRequiredMessage && (
              <AlertMessage messageStyle={AlertMessageStyle.WARNING}>{emailRequiredMessage}</AlertMessage>
            )}
            {invalidEmailFormatMessage && (
              <AlertMessage messageStyle={AlertMessageStyle.WARNING}>
                {invalidEmailFormatMessage}
              </AlertMessage>
            )}
            <Button type="submit" customStyles={panelStyles['Panel-Button']} dataTestId="sendLinkBtn">Send link</Button>
          </>
        )}
      <Button type="button" customStyles={panelStyles['Panel-Button']} dataTestId="backToLoginBtn" onClick={backToLogin} buttonStyle={ButtonStyle.SECONDARY}>Back to login</Button>
    </form>
  );
};

export default ForgotPasswordPage;
