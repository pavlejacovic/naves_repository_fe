import React, { useEffect, useState } from 'react';
import { Col, Container, Row } from 'react-grid-system';
import { MdEdit } from 'react-icons/md';
import { Button, ButtonStyle } from '../../components/Button/Button';
import ChangePasswordModal from '../../components/ChangePasswordModal/ChangePasswordModal';
import EditProfileModal from '../../components/EditProfileModal/EditProfileModal';
import Header from '../../components/Header/Header';
import { ReceivedUsersInfo } from '../../models/user.interface';
import AuthenticationService from '../../services/authentication.service';
import styles from './UserProfilePage.module.scss';

const UserProfilePage: React.FC = (): JSX.Element => {
  const [user, setUser] = useState<ReceivedUsersInfo | null>(null);
  const [isEditProfileModalVisible, setIsEditProfileModalVisible] = useState<boolean>(false);
  const [isEditPasswordModalVisible, setIsEditpasswordModalVisible] = useState<boolean>(false);

  const handleEditProfileModalClick = (): void => setIsEditProfileModalVisible(true);
  const handleChangePasswordClick = ():void => setIsEditpasswordModalVisible(true);

  useEffect(() => {
    AuthenticationService.getUserInformation()
      .then((response) => {
        setUser(response.data);
      });
  }, [!isEditProfileModalVisible]);

  return (
    <Container fluid className={styles['UserProfilePage']}>
      <Row justify="center">
        <Col>
          <Header title="Profile" />
        </Col>
      </Row>
      {user && (
        <Container>
          <Row justify="center" align="center">
            <Col lg={12} className={styles['UserProfilePage-SectionTitle']}>General Informations</Col>
          </Row>
          <Row justify="end" align="center" className={styles['UserProfilePage-Infos']}>
            <Col lg={11} />
            <Col lg={1} className={styles['UserProfilePage-Infos-Edit-Button']} title="Edit" onClick={handleEditProfileModalClick}>
              <MdEdit />
            </Col>
            <Col lg={2} className={styles['UserProfilePage-Infos-Info']}>Name:</Col>
            <Col lg={10} className={styles['UserProfilePage-Infos-Info']}><b>{user.name}</b></Col>
            <Col lg={2} className={styles['UserProfilePage-Infos-Info']}>Surname:</Col>
            <Col lg={10} className={styles['UserProfilePage-Infos-Info']}><b>{user.surname}</b></Col>
            <Col lg={2} className={styles['UserProfilePage-Infos-Info']}>Email:</Col>
            <Col lg={10} className={styles['UserProfilePage-Infos-Info']}><b>{user.email}</b></Col>
            <Col lg={2} className={styles['UserProfilePage-Infos-Info']}>Subscribed:</Col>
            <Col lg={10} className={styles['UserProfilePage-Infos-Info']}><b>{user.subscribed ? 'Yes' : 'No'}</b></Col>
          </Row>
          <Row justify="start" align="center">
            <Col lg={2} title="Edit">
              <Button type="button" buttonStyle={ButtonStyle.SECONDARY} customStyles={styles['UserProfilePage-Infos-Password-Button']} onClick={handleChangePasswordClick}>Change Password</Button>
            </Col>
          </Row>
          <EditProfileModal modalIsVisible={isEditProfileModalVisible} setModalIsVisible={setIsEditProfileModalVisible} text="Edit profile" user={user} />
          <ChangePasswordModal modalIsVisible={isEditPasswordModalVisible} setModalIsVisible={setIsEditpasswordModalVisible} text="Change password" />
        </Container>
      )}
    </Container>
  );
};

export default UserProfilePage;
