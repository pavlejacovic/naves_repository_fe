import React, {
  useState,
  useContext,
  ChangeEvent
} from 'react';
import { Col, Container, Row } from 'react-grid-system';
import AlertMessage from '../../components/AlertMessage/AlertMessage';
import Header from '../../components/Header/Header';
import ProjectFolder from '../../components/ProjectFolder/ProjectFolder';
import styles from './SharedWithMePage.module.scss';
import Select from '../../components/Select/Select';
import InputField from '../../components/InputField/InputField';
import { PROJECT_PER_PAGE_VALUES, SORT_SHARED_PROJECT_VALUES } from '../../utils/constants/selectObjects/selectObjects';
import { ProjectsContext } from '../../Contexts/ProjectsContext';
import Pagination from '../../components/Pagination/Pagination';
import useUpdatePageSortValue from '../../utils/hooks/useUpdateURLSortValue';
import { useUpdateBasePage } from '../../utils/hooks/useLastPage';
import { QueryParamsShareProjectContext } from '../../Contexts/QueryParamsShareProjectContext';

const SharedWithMePage: React.FC = (): JSX.Element => {
  const {
    projectsPerPage,
    setProjectsPerPage,
    sharedWithMeProjects,
    sharedProjectsOnPage
  } = useContext(ProjectsContext);
  const [searchProjectValue, setSearchProjectValue] = useState<string>('');
  const { page } = useContext(QueryParamsShareProjectContext);

  const filteredProjects = sharedWithMeProjects.filter((project) => (
    project.projectName.toLowerCase().includes(searchProjectValue.toLowerCase())));

  const handleSelectProjectPerPage = (event: ChangeEvent<HTMLSelectElement>) => {
    setProjectsPerPage(Number(event.target.value));
  };

  const handleSearchProjectChange = (event: ChangeEvent<HTMLInputElement>) => {
    setSearchProjectValue(event.target.value);
  };

  const handleSortProject = (event: ChangeEvent<HTMLSelectElement>) => {
    useUpdatePageSortValue(event);
  };

  useUpdateBasePage(sharedWithMeProjects.length, projectsPerPage);

  return (
    <Container fluid className={styles['SharedWithMePage']}>
      <Row>
        <Col>
          <Header title="My projects">
            <Row justify="end" align="center">
              <Col lg="content" md={6} xs={12}>
                <Select id="sortProjects" onChange={handleSortProject}>
                  <option hidden defaultValue="id">Sort:</option>
                  {Object.entries(SORT_SHARED_PROJECT_VALUES)
                    .map(([value, label]: string[]) => <option key={`${value}key`} value={value}>{label}</option>)}
                </Select>
              </Col>
              <Col lg="content" md={6} xs={12}>
                <Select id="projectsPerPage" onChange={handleSelectProjectPerPage}>
                  <option hidden defaultValue="12">Projects per page:</option>
                  {Object.entries(PROJECT_PER_PAGE_VALUES)
                    .map(([value, label]: string[]) => <option key={`${value}key`} value={value}>{label}</option>)}
                </Select>
              </Col>
              <Col lg="content" md={6} xs={12}>
                <InputField
                  id="searchProject"
                  name="searchProject"
                  type="text"
                  placeholder="Search project..."
                  dataTestId="searchProjectInputTest"
                  value={searchProjectValue}
                  onChange={handleSearchProjectChange}
                />
              </Col>
            </Row>
          </Header>
        </Col>
      </Row>
      <Row>
        {searchProjectValue
          ? filteredProjects.map((project) => (
            <Col xl={3} lg={6} md={12} key={project.id}>
              <Row justify="center" className={styles['SharedWithMePage-ProjectFolder']}>
                <ProjectFolder
                  writePermission={project.writePermission}
                  linkTo={`/shared-with-me/${project.id}`}
                  projectId={project.id}
                  projectName={project.projectName || ''}
                  projectOwner={project.owner?.email}
                  owner={false}
                />
              </Row>
            </Col>
          ))
          : sharedProjectsOnPage.map((project) => (
            <Col xl={3} lg={6} md={12} key={project.id}>
              <Row justify="center" className={styles['SharedWithMePage-ProjectFolder']}>
                <ProjectFolder
                  writePermission={project.writePermission}
                  linkTo={`/shared-with-me/${project.id}`}
                  projectId={project.id}
                  projectName={project.projectName || ''}
                  projectOwner={project.owner?.email}
                  owner={false}
                />
              </Row>
            </Col>
          ))}
        {!sharedWithMeProjects.length
          && (
            <AlertMessage customStyles={styles['SharedWithMePage-AlertMessage']}>There are no shared projects with you.</AlertMessage>
          )}
      </Row>
      {(sharedWithMeProjects.length > projectsPerPage && !searchProjectValue)
        && (
          <Pagination
            itemsPerPage={projectsPerPage}
            totalNumberOfItems={sharedWithMeProjects.length}
            pageNumber={page}
          />
        )}
    </Container>
  );
};

export default SharedWithMePage;
