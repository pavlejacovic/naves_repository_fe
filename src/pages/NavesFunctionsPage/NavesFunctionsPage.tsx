import React, {
  useContext,
  ChangeEvent,
  useState
} from 'react';
import { Col, Container, Row } from 'react-grid-system';
import AlertMessage from '../../components/AlertMessage/AlertMessage';
import { Button, ButtonStyle } from '../../components/Button/Button';
import Header from '../../components/Header/Header';
import styles from './NavesFunctionsPage.module.scss';
import NavesFunction from '../../components/NavesFunction/NavesFunction';
import SaveFunctionModal from '../../components/SaveFunctionModal/SaveFunctionModal';
import Pagination from '../../components/Pagination/Pagination';
import { NavesFunctionsContext } from '../../Contexts/NavesFunctionsContext';
import InputField from '../../components/InputField/InputField';
import { NavesFunctionInfo } from '../../models/function.interface';
import { useUpdateBasePage } from '../../utils/hooks/useLastPage';
import Select from '../../components/Select/Select';
import { SORT_FUNCTION_VALUES } from '../../utils/constants/selectObjects/selectObjects';
import useUpdatePageSortValue from '../../utils/hooks/useUpdateURLSortValue';
import { QueryParamsFunctionsContext } from '../../Contexts/QueryParamsFunctionContext';

const NavesFunctionsPage: React.FC = (): JSX.Element => {
  const [isCreateFunctionModalVisible, setIsCreateFunctionModalVisible] = useState<boolean>(false);
  const [searchFunctionValue, setSearchFunctionValue] = useState<string>('');
  const {
    functionsOnPage,
    functionsPerPage,
    functions
  } = useContext(NavesFunctionsContext);
  const { pageNumber } = useContext(QueryParamsFunctionsContext);

  const filteredFunctions: NavesFunctionInfo[] = functions.filter((singleFunction) => (
    singleFunction.functionName.toLowerCase().includes(searchFunctionValue.toLowerCase())));

  const handleOpenCreateFunctionModalClick = (): void => setIsCreateFunctionModalVisible(true);

  const handleSearchFunctionsChange = (event: ChangeEvent<HTMLInputElement>) => {
    setSearchFunctionValue(event.target.value);
  };

  const handleSortFunctions = (event: ChangeEvent<HTMLSelectElement>) => {
    useUpdatePageSortValue(event);
  };

  useUpdateBasePage(functions.length, functionsPerPage);

  return (
    <Container fluid className={styles['NavesFunctionsPage']}>
      <Row>
        <Col>
          <Header title="Naves functions">
            <Row>
              <Col lg="content" md={6} xs={12}>
                <Select id="sortFunctions" onChange={handleSortFunctions}>
                  <option hidden defaultValue="id">Sort:</option>
                  {Object.entries(SORT_FUNCTION_VALUES)
                    .map(([value, label]: string[]) => <option key={`${value}key`} value={value}>{label}</option>)}
                </Select>
              </Col>
              <Col lg="content" md={6} xs={12}>
                <InputField
                  id="searchFunctions"
                  name="searchFunctions"
                  type="text"
                  placeholder="Search functions..."
                  dataTestId="searchFunctionsInputTest"
                  value={searchFunctionValue}
                  onChange={handleSearchFunctionsChange}
                />
              </Col>
              <Col md="content" sm={6} xs={12}><Button type="button" buttonStyle={ButtonStyle.SECONDARY} dataTestId="createNewFunction" onClick={handleOpenCreateFunctionModalClick}>Create new</Button></Col>
            </Row>
          </Header>
        </Col>
      </Row>
      {functions.length !== 0 && (
        <Container fluid className={styles['NavesFunctionsPage-Content']}>
          <Row justify="center">
            <Col md={12}>
              <header>
                <Row justify="center" className={styles['NavesFunctionsPage-Content-Header']}>
                  <Col lg={1} />
                  <Col lg={3}><b>Name</b></Col>
                  <Col lg={2}><b>Access</b></Col>
                  <Col lg={2}><b>Trigger</b></Col>
                  <Col lg={3}><b>Link</b></Col>
                  <Col lg={1} />
                </Row>
              </header>
              <ul className={styles['NavesFunctionsPage-Content-Function']}>
                {searchFunctionValue
                  ? filteredFunctions.map((navesFunction: NavesFunctionInfo, index: number) => (
                    <NavesFunction navesFunction={navesFunction} index={index} key={navesFunction.functionId} />))
                  : functionsOnPage.map((navesFunction, index) => (
                    <NavesFunction navesFunction={navesFunction} index={index} key={navesFunction.functionId} />
                  ))}
              </ul>
            </Col>
          </Row>
        </Container>
      )}
      {
        functions.length === 0
        && (
          <AlertMessage customStyles={styles['NavesFunctionsPage-AlertMessage']}>There are no functions.</AlertMessage>
        )
      }
      <SaveFunctionModal
        title="Create new function"
        modalIsVisible={isCreateFunctionModalVisible}
        setModalIsVisible={setIsCreateFunctionModalVisible}
      />
      {(functions.length > functionsPerPage && !searchFunctionValue)
        && (
          <Pagination
            itemsPerPage={functionsPerPage}
            totalNumberOfItems={functions.length}
            pageNumber={pageNumber}
          />
        )}
    </Container>
  );
};

export default NavesFunctionsPage;
