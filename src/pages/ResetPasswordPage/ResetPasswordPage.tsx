import React, { useState, FormEvent } from 'react';
import { toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';
import AuthenticationService from '../../services/authentication.service';
import { UserInfo } from '../../models/user.interface';
import { ALL_FILEDS_REQUIRED, PASSWORD_DONT_MATCH } from '../../utils/constants/validationMessages/validation-messages';
import isValidPassword from '../../utils/validations/inputValidationsForPassword/inputValidationsPassword';
import AlertMessage, { AlertMessageStyle } from '../../components/AlertMessage/AlertMessage';
import { Button, ButtonStyle } from '../../components/Button/Button';
import InputField from '../../components/InputField/InputField';
import styles from './ResetPasswordPage.module.scss';
import panelStyles from '../../utils/styles/Panel.module.scss';

const ResetPasswordPage: React.FC = (): JSX.Element => {
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const passwordResetToken = urlParams.get('token');
  const history = useHistory();
  const [password, setPassword] = useState<string>('');
  const [confirmPassword, setConfirmPassword] = useState<string>('');
  const [allFiledsRequiredMessage, setAllFiledsRequiredMessage] = useState<string>('');
  const [incorrectPasswordMessage, setIncorrectPasswordMessage] = useState<string>('');
  const [passwordDoesntMatchMessage, setPasswordDoesntMatchMessage] = useState<string>('');
  const [statusCode, setStatusCode] = useState<number | null>(null);
  const [messageForClient, setMessageForClient] = useState<string>('Enter your new password');

  const user: UserInfo = {
    password,
    passwordResetToken
  };
  const backToLogin = (): void => {
    history.push('/login');
  };
  const resetForm = () => {
    setAllFiledsRequiredMessage('');
    setIncorrectPasswordMessage('');
    setPasswordDoesntMatchMessage('');
  };
  const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setPassword(event.target.value);
    resetForm();
  };
  const handleConfirmPasswordChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setConfirmPassword(event.target.value);
    resetForm();
  };
  const handleChangePasswordSubmit = (e: FormEvent<HTMLFormElement>): void => {
    e.preventDefault();
    if (password && confirmPassword) {
      if (!isValidPassword(password)) {
        if (password === confirmPassword) {
          AuthenticationService.resetPassword(user).then((response) => {
            setStatusCode(response.status);
            setMessageForClient(response.data);
          }).catch((error) => {
            setStatusCode(error.response.status);
            setMessageForClient(error.response.data);
            toast.error(error.response.data);
          });
        } else {
          setPasswordDoesntMatchMessage(PASSWORD_DONT_MATCH);
        }
      } else {
        setIncorrectPasswordMessage(isValidPassword(password));
      }
    } else {
      setAllFiledsRequiredMessage(ALL_FILEDS_REQUIRED);
    }
  };

  return (
    <div>
      <form className={panelStyles['Panel']} onSubmit={handleChangePasswordSubmit}>
        <p className={styles['ResetPassword-Message']}>{messageForClient}</p>
        {(statusCode === null)
          && (
            <>
              <div className={panelStyles['Panel-Field']}>
                <label htmlFor="password" className={panelStyles['Panel-InputLabel']}>Password</label>
                <InputField
                  id="password"
                  name="password"
                  type="password"
                  placeholder="Example!#*125"
                  value={password}
                  inputWarning={
                    (allFiledsRequiredMessage && !password) || incorrectPasswordMessage || passwordDoesntMatchMessage
                  }
                  onChange={handlePasswordChange}
                />
              </div>
              {incorrectPasswordMessage && (
                <AlertMessage messageStyle={AlertMessageStyle.WARNING}>{incorrectPasswordMessage}</AlertMessage>
              )}
              <div className={panelStyles['Panel-Field']}>
                <label htmlFor="confirmPassword" className={panelStyles['Panel-InputLabel']}>Confirm Password</label>
                <InputField
                  id="confirmPassword"
                  name="confirmPassword"
                  type="password"
                  placeholder="Example!#*125"
                  value={confirmPassword}
                  inputWarning={(allFiledsRequiredMessage && !confirmPassword) || passwordDoesntMatchMessage}
                  onChange={handleConfirmPasswordChange}
                />
              </div>
              {passwordDoesntMatchMessage && (
                <AlertMessage messageStyle={AlertMessageStyle.WARNING}>
                  {passwordDoesntMatchMessage}
                </AlertMessage>
              )}
              {allFiledsRequiredMessage && (
                <AlertMessage messageStyle={AlertMessageStyle.WARNING}>{allFiledsRequiredMessage}</AlertMessage>
              )}
              <Button type="submit" customStyles={panelStyles['Panel-Button']} dataTestId="saveNewPasswordBtn">Save new password</Button>
            </>
          )}
        <Button type="button" customStyles={panelStyles['Panel-Button']} onClick={backToLogin} buttonStyle={ButtonStyle.SECONDARY} dataTestId="backToLoginButton">Back to login</Button>
      </form>
    </div>
  );
};

export default ResetPasswordPage;
