import React, { ChangeEvent, useContext, useState } from 'react';
import { Col, Container, Row } from 'react-grid-system';
import Statistics from '../../components/Statistics/Statistics';
import Header from '../../components/Header/Header';
import styles from './DashboardPage.module.scss';
import ProjectRow from '../../components/ProjectRow/ProjectRow';
import { AdminDashboardInfoContext } from '../../Contexts/AdminDashboardInfoContext';
import InputField from '../../components/InputField/InputField';
import Pagination from '../../components/Pagination/Pagination';
import { QueryParamsAdminDashboardContext } from '../../Contexts/QueryParamsAdminPageContext';

const DashboardPage = (): JSX.Element => {
  const [searchProjectValue, setSearchProjectValue] = useState<string>('');
  const { projectsInfo, projectsInfoPageSize, projectsInfoPerPage } = useContext(AdminDashboardInfoContext);
  const { pageNumber } = useContext(QueryParamsAdminDashboardContext);

  const handleSearchProjectChange = (event: ChangeEvent<HTMLInputElement>) => {
    setSearchProjectValue(event.target.value);
  };

  const filteredProjects = projectsInfo.filter((projectInfo) => (
    projectInfo.project.projectName.toLowerCase().includes(searchProjectValue.toLowerCase())));

  return (
    <Container fluid className={styles['DashboardPage']}>
      <Row justify="center">
        <Col>
          <Header title="Dashboard">
            <Col xs={12}>
              <InputField
                id="searchDashboardProject"
                name="searchDashboardProject"
                type="text"
                placeholder="Search project..."
                dataTestId="searchDashboardProjectInputTest"
                value={searchProjectValue}
                onChange={handleSearchProjectChange}
              />
            </Col>
          </Header>
        </Col>
      </Row>
      <Row justify="center" align="center" className={styles['DashboardPage-Row']}>
        <Col>
          <Statistics customStyles={styles['DashboardPage-Statistics']} />
        </Col>
      </Row>
      <Container fluid className={styles['DashboardPage-Content']}>
        <Row justify="center">
          <Col>
            <header>
              <Row justify="start" align="center" className={styles['DashboardPage-Content-Header']}>
                <Col>
                  <Row justify="between" align="center" className={styles['DashboardPage-Content-HeaderTitles']}>
                    <Col lg={3}><b>Project name</b></Col>
                    <Col lg={3}><b>Owner</b></Col>
                    <Col lg={3}><b>Size</b></Col>
                    <Col lg={3}><b>Shared with</b></Col>
                  </Row>
                </Col>
              </Row>
            </header>
            <ul className={styles['DashboardPage-Content-Files']}>
              {searchProjectValue
                ? filteredProjects.map((projectInfo) => (
                  <Row justify="start" align="center" className={styles['DashboardPage-Content-Row']} key={projectInfo?.project?.id}>
                    <Col key={projectInfo?.project?.id}>
                      <ProjectRow dataTestId={`project${projectInfo.project.id}`} projectInfo={projectInfo} />
                    </Col>
                  </Row>
                ))
                : projectsInfoPerPage.map((projectInfo) => (
                  <Row justify="start" align="center" className={styles['DashboardPage-Content-Row']} key={projectInfo?.project?.id}>
                    <Col key={projectInfo?.project?.id}>
                      <ProjectRow dataTestId={`project${projectInfo.project.id}`} projectInfo={projectInfo} />
                    </Col>
                  </Row>
                ))}
            </ul>
          </Col>
        </Row>
      </Container>
      {(projectsInfo.length > projectsInfoPageSize && !searchProjectValue)
        && (
        <Pagination
          itemsPerPage={projectsInfoPageSize}
          totalNumberOfItems={projectsInfo.length}
          pageNumber={pageNumber}
        />
        )}
    </Container>
  );
};

export default DashboardPage;
