import React, {
  MouseEvent,
  useState,
  ChangeEvent,
  useContext,
  useEffect
} from 'react';
import { Col, Container, Row } from 'react-grid-system';
import { MdFileUpload } from 'react-icons/md';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import AlertMessage, { AlertMessageStyle } from '../../components/AlertMessage/AlertMessage';
import { Button } from '../../components/Button/Button';
import File from '../../components/File/File';
import Folder from '../../components/Folder/Folder';
import Header from '../../components/Header/Header';
import Modal from '../../components/Modal/Modal';
import { FileData } from '../../models/file.interface';
import SaveModal from '../../components/SaveModal/SaveModal';
import { ProjectPageContext } from '../../Contexts/ProjectPageContext';
import BoxService from '../../services/box.service';
import fileSaverService from '../../services/file-saver.service';
import {
  FILES_ARE_NOT_SPECIFIED,
  SPACE_EXCEEDED,
  FILES_FOR_DOWNLOAD_REQUIRED,
  FILES_FOR_DELETE_REQUIRED,
  MAXIMUM_FILES_NUMBER
} from '../../utils/constants/validationMessages/validation-messages';
import { splitPath, trimPath } from '../../utils/functions/stringManipulations/stringManipulations/stringManipulations';
import bytesToMegaBytes from '../../utils/functions/unitsConversions/unitsConversions';
import styles from './ProjectPage.module.scss';
import { AuthorizedPagesContext } from '../../Contexts/AuthorizedPagesContext';
import Checkbox from '../../components/Checkbox/Checkbox';
import ConfirmModal from '../../components/ConfirmModal/ConfirmModal';
import { QUESTION_DELETING_FILES } from '../../utils/constants/questions/questions';

const ProjectPage: React.FC = (): JSX.Element => {
  const history = useHistory();
  const {
    project,
    fetchFoldersAndFiles,
    checkedFilesAndFolders,
    setCheckedFilesAndFolders
  } = useContext(ProjectPageContext);
  const { pathToTheSource } = useContext(AuthorizedPagesContext);
  const [isUploadModalVisible, setIsUploadModalVisible] = useState<boolean>(false);
  const [isConfirmModalVisible, setIsConfirmModalVisible] = useState<boolean>(false);
  const [filesForUploadNotSpecified, setFilesForUploadNotSpecified] = useState<boolean>(false);
  const [isCreateDirectoryModalVisible, setIsCreateDirectoryModalVisible] = useState<boolean>(false);
  const [chosenFiles, setChosenFiles] = useState<FileList | null>(null);
  const [checkedAllFilesAndFolders, setCheckedAllFilesAndFolders] = useState<boolean>(false);
  const repackagedChosenFiles: FileData[] = [];
  const splitedPath = splitPath(pathToTheSource);
  const pathToFile = splitedPath[1] || '';
  const projectId = parseInt(splitedPath[0], 10);

  const resetPage = (): void => {
    setIsUploadModalVisible(false);
    setChosenFiles(null);
  };

  useEffect(() => {
    fetchFoldersAndFiles(pathToTheSource);
  }, [pathToTheSource]);

  useEffect(() => {
    if (checkedFilesAndFolders.length && checkedFilesAndFolders.every((file) => file.checked)) {
      setCheckedAllFilesAndFolders(true);
    } else {
      setCheckedAllFilesAndFolders(false);
    }
  }, [checkedFilesAndFolders]);

  const uploadFiles = (): void => {
    BoxService.uploadFiles(projectId, { files: repackagedChosenFiles, path: pathToFile })
      .then((response) => {
        response.data.forEach((file: any) => {
          toast.success(file.message);
        });
        resetPage();
        fetchFoldersAndFiles(pathToTheSource);
      });
    setIsUploadModalVisible(false);
  };

  const checkFreeSpaceInProject = (): void => {
    const sizeForUpload: number = repackagedChosenFiles.reduce((a, b) => a + b.size, 0);
    BoxService.getProjectSpace(projectId, pathToFile)
      .then((response) => {
        if (typeof response.data.allowedSize === 'number' && typeof response.data.currentSize === 'number') {
          const freeSpace: number = response.data.allowedSize - response.data.currentSize;
          if (sizeForUpload <= freeSpace) {
            uploadFiles();
          } else {
            toast.error(`${SPACE_EXCEEDED}${bytesToMegaBytes(freeSpace)}`);
          }
        }
      });
  };

  const handleSendChosenFilesOnClick = (event: MouseEvent<HTMLButtonElement>): void => {
    event.preventDefault();
    if (chosenFiles) {
      Object.keys(chosenFiles).forEach((file: any) => repackagedChosenFiles.push(chosenFiles[file]));
      if (repackagedChosenFiles.length > 10) {
        toast.warning(MAXIMUM_FILES_NUMBER);
        return;
      }
      checkFreeSpaceInProject();
    } else {
      setFilesForUploadNotSpecified(true);
    }
  };

  const handleChosenFiles = (event: ChangeEvent<HTMLInputElement>) => {
    setFilesForUploadNotSpecified(false);
    setChosenFiles(event.target.files);
  };

  const handleUploadModalIsVisible = (event: MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    setChosenFiles(null);
    setFilesForUploadNotSpecified(false);
    setIsUploadModalVisible(true);
  };

  const handleCreateDirectoryModalIsVisible = () => setIsCreateDirectoryModalVisible(true);

  const handleCreateDirectoryInsideProject = (
    directoryName: string,
    setInputText: React.Dispatch<React.SetStateAction<string>>
  ): void => {
    const path = `${pathToFile ? (`${decodeURIComponent(pathToFile)}`) : '/'}`;
    if (project.project.id) {
      BoxService.createDirectoryInsideProject(path, directoryName, project.project.id).then(
        (response) => {
          setIsCreateDirectoryModalVisible(false);
          setInputText('');
          toast.success(response.data);
          resetPage();
          fetchFoldersAndFiles(pathToTheSource);
        }
      ).catch(
        () => {
          setInputText('');
          setIsCreateDirectoryModalVisible(false);
        }
      );
    } else {
      history.push('/projects');
    }
  };

  const handleCheckAllFilesOnChange = (e: ChangeEvent<HTMLInputElement>) => {
    setCheckedAllFilesAndFolders(e.target.checked);
    setCheckedFilesAndFolders(checkedFilesAndFolders.map(
      (file) => (
        { path: file.path, checked: e.target.checked }
      )
    ));
  };

  const handleFileCheckOnChange = (e: ChangeEvent<HTMLInputElement>) => {
    setCheckedFilesAndFolders(checkedFilesAndFolders.map(
      (file) => {
        if (file.path === e.target.value) {
          return {
            path: file.path,
            checked: e.target.checked
          };
        }
        return file;
      }
    ));
  };

  const downloadFilesAndFolders = () => {
    const filesToDownload = checkedFilesAndFolders.filter((file) => file.checked);
    if (filesToDownload.length !== 0) {
      BoxService.downloadAsZip(project.project.id || 0, filesToDownload.map(
        (fileToDownload) => ({ path: fileToDownload.path })
      )).then(
        (response) => {
          fileSaverService.downloadZip(response.data, project.project.projectName);
        }
      );
    } else {
      toast.warning(FILES_FOR_DOWNLOAD_REQUIRED);
    }
  };

  const handleDeleteButtonClick = (): void => {
    const filesToDelete = checkedFilesAndFolders.filter((file) => file.checked);
    if (filesToDelete.length === 0) {
      toast.warning(FILES_FOR_DELETE_REQUIRED);
      return;
    }
    setIsConfirmModalVisible(true);
  };

  const handleDeleteFilesAndFolders = (): void => {
    const filesToDelete = checkedFilesAndFolders.filter((file) => file.checked);
    if (filesToDelete.length !== 0 && project.project.id) {
      BoxService.deleteFileOrFolder(project.project.id, filesToDelete.map(
        (fileToDelete) => ({ path: fileToDelete.path })
      )).then(
        (response) => {
          response.data.map((data: { message: string }) => toast.info(data.message));
          fetchFoldersAndFiles(pathToTheSource);
        }
      ).catch((error) => error.response.data.map((data: { message: string }) => toast.info(data.message)));
    }
    setIsConfirmModalVisible(false);
  };

  const handleCloseConfirmModalClick = () => setIsConfirmModalVisible(false);

  const displayResponseMessage = (): JSX.Element | null => {
    if (!project.folders.length && !project.files.length) {
      if (pathToFile) {
        return <AlertMessage customStyles={styles['ProjectPage-Message']}>The folder is empty.</AlertMessage>;
      }
      return <AlertMessage customStyles={styles['ProjectPage-Message']}>The project is empty.</AlertMessage>;
    }
    return null;
  };

  return (
    <Container fluid className={styles['ProjectPage']}>
      <Row>
        <Col>
          <Header title={`${project.project.projectName}${pathToFile ? `/${pathToFile}` : ''}`}>
            <Row>
              {project.writePermission
              && (
              <>
                <Col md={3} sm={4} xs={12}><Button dataTestId="createDirectoryBtn" type="button" onClick={handleCreateDirectoryModalIsVisible}>Create directory</Button></Col>
                <Col md={3} sm={4} xs={12}><Button dataTestId="opetModalForUploadBtn" type="button" onClick={handleUploadModalIsVisible}>Upload</Button></Col>
                <Col md={3} sm={4} xs={12}><Button type="button" onClick={handleDeleteButtonClick}>Delete</Button></Col>
              </>
              )}
              <Col md={3} sm={6} xs={12}><Button type="button" onClick={downloadFilesAndFolders}>Download</Button></Col>
            </Row>
          </Header>
        </Col>
      </Row>
      {(project.folders.length !== 0 || project.files.length !== 0) && (
        <Container fluid className={styles['ProjectPage-Content']}>
          <Row justify="center">
            <Col md={12}>
              <header>
                <Row justify="start" align="center" className={styles['ProjectPage-Content-Header']}>
                  <Col md={1}>
                    <Checkbox dataTestId="check-all-checkboxes" checked={checkedAllFilesAndFolders} handleOnChange={handleCheckAllFilesOnChange} />
                  </Col>
                  <Col md={11}>
                    <Row justify="between" align="center" className={styles['ProjectPage-Content-HeaderTitles']}>
                      <Col lg={3} offset={{ lg: 1 }}><b>Name</b></Col>
                      <Col lg={3}><b>Uploaded by</b></Col>
                      <Col lg={3}><b>Creation date</b></Col>
                      <Col lg={2}><b>File size</b></Col>
                    </Row>
                  </Col>
                </Row>
              </header>
              <ul className={styles['ProjectPage-Content-Files']}>
                {project.folders.map((folder, index) => (
                  <Row justify="start" align="center" key={`row${folder.folderPath}`} className={styles['Project-Content-Row']}>
                    <Col md={1} key={`col${folder.folderPath}`}>
                      <Checkbox
                        dataTestId={`cB${folder.folderPath}`}
                        key={`cB${folder.folderPath}`}
                        checked={checkedFilesAndFolders[index]?.checked || false}
                        handleOnChange={handleFileCheckOnChange}
                        value={`${folder.folderPath}`}
                      />
                    </Col>
                    <Col md={11}>
                      <Folder writePermission={project.writePermission} key={folder.folderPath} projectId={project.project.id} dataTestId={`${folder.folderPath}${index}`} folder={folder} />
                    </Col>
                  </Row>
                ))}
                {project.files.map((file, index) => (
                  <Row justify="start" align="center" key={`row${file.filePath}`} className={styles['Project-Content-Row']}>
                    <Col md={1} key={`col${file.filePath}`}>
                      <Checkbox
                        dataTestId={`cB${file.filePath}`}
                        key={`cB${file.filePath}`}
                        checked={checkedFilesAndFolders[project.folders.length + index]?.checked || false}
                        handleOnChange={handleFileCheckOnChange}
                        value={`${splitPath(pathToTheSource)[1] || ''}/${trimPath(file?.filePath)}`}
                      />
                    </Col>
                    <Col md={11}>
                      <File writePermission={project.writePermission} key={file.filePath} projectId={file.parentProject.id} dataTestId={`${file.filePath}${index}`} file={file} text="..." />
                    </Col>
                  </Row>
                ))}
              </ul>
            </Col>
          </Row>
        </Container>
      )}
      {displayResponseMessage()}
      <Modal
        modalIsVisible={isUploadModalVisible}
        setModalIsVisible={setIsUploadModalVisible}
        actions={
          (
            <>
              <Col md="content" sm={12}><Button type="button" dataTestId="sendChosenFilesBtn" customStyles={styles['Project-Page-Modal-Button']} onClick={handleSendChosenFilesOnClick}>Upload</Button></Col>
            </>
          )
        }
      >
        <div className={styles['ProjectPage-Modal']}>
          <input id="uploadFile" type="file" multiple className={styles['ProjectPage-Modal-InputField']} onChange={handleChosenFiles} />
          <label htmlFor="uploadFile" className={styles['ProjectPage-Modal-InputField-Label']}>
            <MdFileUpload />
            Browse
          </label>
          {filesForUploadNotSpecified
            && <AlertMessage messageStyle={AlertMessageStyle.WARNING} customStyles={styles['ProjectPage-Modal-Message']}>{FILES_ARE_NOT_SPECIFIED}</AlertMessage>}
          {chosenFiles && (
            <ul className={styles['Project-Page-Modal-ChosenFiles']}>
              <li>Chosen files:</li>
              {
                Object.keys(chosenFiles).map((file: any) => (
                  <li key={chosenFiles[file].name} data-test-id={chosenFiles[file].name} className={styles['Project-Page-Modal-ChosenFiles-File']}>{chosenFiles[file].name}</li>
                ))
              }
            </ul>
          )}
        </div>
      </Modal>
      <SaveModal
        modalIsVisible={isCreateDirectoryModalVisible}
        setModalIsVisible={setIsCreateDirectoryModalVisible}
        title="Create directory"
        inputPlaceholder="Enter directory name"
        onSave={handleCreateDirectoryInsideProject}
        dataTestId="newDirectory"
      >
        <Button type="submit">Save directory</Button>
      </SaveModal>
      <ConfirmModal
        text={QUESTION_DELETING_FILES}
        modalIsVisible={isConfirmModalVisible}
        setModalIsVisible={setIsConfirmModalVisible}
        onConfirm={handleDeleteFilesAndFolders}
        onClose={handleCloseConfirmModalClick}
      />
    </Container>
  );
};

export default ProjectPage;
