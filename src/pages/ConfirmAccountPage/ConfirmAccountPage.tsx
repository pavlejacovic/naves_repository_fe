import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import AuthenticationService from '../../services/authentication.service';

const ConfirmAccountPage: React.FC = (): null => {
  const history = useHistory();
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const confirmationToken: string | null = urlParams.get('token');

  useEffect(() => {
    if (confirmationToken) {
      AuthenticationService.verifyUser({ token: confirmationToken }).then((response) => {
        if (response.status === 200) {
          history.push('/login?status=verified');
        }
      }).catch((error) => {
        history.push(`/register?status=notverified&message=${error.response.data}`);
      });
    } else history.push('/register');
  }, []);

  return null;
};

export default ConfirmAccountPage;
