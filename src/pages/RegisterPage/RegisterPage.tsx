import React, { useState, FormEvent } from 'react';
import AuthenticationService from '../../services/authentication.service';
import { Button, ButtonStyle } from '../../components/Button/Button';
import InputField from '../../components/InputField/InputField';
import styles from './RegisterPage.module.scss';
import panelStyles from '../../utils/styles/Panel.module.scss';
import AlertMessage, { AlertMessageStyle } from '../../components/AlertMessage/AlertMessage';
import {
  ALL_FILEDS_REQUIRED,
  INVALID_EMAIL_FORMAT,
  PASSWORD_DONT_MATCH,
  TERMS_AND_CONDITIONS_REQUIRED
} from '../../utils/constants/validationMessages/validation-messages';
import isValidEmail from '../../utils/validations/inputValidationsForEmail/inputValidationsForEmail';
import isValidPassword from '../../utils/validations/inputValidationsForPassword/inputValidationsPassword';
import Modal from '../../components/Modal/Modal';
import RULES from '../../utils/constants/termsAndConditions/TermsAndConditions';
import { SUBSCRIBE_MESSAGE } from '../../utils/constants/questions/questions';

const RegisterPage: React.FC = (): JSX.Element => {
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const verifiedStatus: string | null = urlParams.get('status');
  const verifiedMessage: string | null = urlParams.get('message');

  const [signUpResponse, setSignUpResponse] = useState<string>('');
  const [userExist, setUserExist] = useState<string>('');
  const [name, setName] = useState<string>('');
  const [surname, setSurname] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [confirmPassword, setConfirmPassword] = useState<string>('');
  const [subscribed, setSubscribed] = useState<boolean>(false);
  const [termsAreComfirmed, setTermsAreComfirmed] = useState<boolean>(false);
  const [termsModalIsVisible, setTermsModalIsVisible] = useState<boolean>(false);
  const [termsAreNotAcceptedMessage, setTermsAreNotAcceptedMessage] = useState<string>('');
  const [allFiledsRequiredMessage, setAllFiledsRequiredMessage] = useState<string>('');
  const [emailFormatIncorrectMessage, setEmailFormatIncorrectMessage] = useState<string>('');
  const [passwordFormatIncorrectMessage, setPasswordFormatIncorrectMessage] = useState<string>('');
  const [passwordDoesntMatchMessage, setPasswordDoesntMatchMessage] = useState<string>('');

  const resetForm = () => {
    setUserExist('');
    setTermsAreNotAcceptedMessage('');
    setTermsModalIsVisible(false);
    setAllFiledsRequiredMessage('');
    setEmailFormatIncorrectMessage('');
    setPasswordFormatIncorrectMessage('');
    setPasswordDoesntMatchMessage('');
  };
  const onChangeSetName = (event: React.ChangeEvent<HTMLInputElement>): void => {
    resetForm();
    setName(event.target.value);
  };
  const onChangeSetSurame = (event: React.ChangeEvent<HTMLInputElement>): void => {
    resetForm();
    setSurname(event.target.value);
  };
  const onChangeSetEmail = (event: React.ChangeEvent<HTMLInputElement>): void => {
    resetForm();
    setEmail(event.target.value);
  };
  const onChangeSetPassword = (event: React.ChangeEvent<HTMLInputElement>): void => {
    resetForm();
    setPassword(event.target.value);
  };
  const handleConfirmPasswordChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    resetForm();
    setConfirmPassword(event.target.value);
  };
  const onChangeSetSubscribe = (event: React.ChangeEvent<HTMLInputElement>): void => {
    resetForm();
    setSubscribed(event.target.checked);
  };
  const onChangeSetTerms = (): void => {
    setTermsAreComfirmed(!termsAreComfirmed);
  };

  const readTermsAndConditions = (event: React.MouseEvent<HTMLAnchorElement>): void => {
    event.preventDefault();
    setTermsModalIsVisible(true);
  };

  const handleSignUpSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    resetForm();
    if (name.trim() && surname.trim() && email && password && confirmPassword) {
      if (isValidEmail(email) && !isValidPassword(password)) {
        if (password === confirmPassword) {
          if (termsAreComfirmed) {
            AuthenticationService.registerUser({
              name: name.trim(),
              surname: surname.trim(),
              email: email,
              password: password,
              subscribed: subscribed
            }).then((response) => {
              setSignUpResponse(response.data);
            }).catch((error) => {
              if (error.response.status === 409) {
                setUserExist(error.response.data);
                setPassword('');
                setConfirmPassword('');
              }
            });
          } else {
            setTermsAreNotAcceptedMessage(TERMS_AND_CONDITIONS_REQUIRED);
          }
        } else {
          setPasswordDoesntMatchMessage(PASSWORD_DONT_MATCH);
        }
      } else {
        if (!isValidEmail(email)) {
          setEmailFormatIncorrectMessage(INVALID_EMAIL_FORMAT);
        }
        if (isValidPassword(password)) {
          setPasswordFormatIncorrectMessage(isValidPassword(password));
        }
      }
    } else {
      setAllFiledsRequiredMessage(ALL_FILEDS_REQUIRED);
    }
  };

  const registration = () => (
    <>
      <form className={panelStyles['Panel']} onSubmit={handleSignUpSubmit}>
        {verifiedStatus === 'notverified' && <AlertMessage messageStyle={AlertMessageStyle.WARNING}>{verifiedMessage}</AlertMessage>}
        <span className={panelStyles['Panel-Title']}>Register</span>
        <div className={panelStyles['Panel-Field']}>
          <label htmlFor="name" className={panelStyles['Panel-InputLabel']}>First name</label>
          <InputField
            id="name"
            name="name"
            type="text"
            placeholder="Jonathon"
            value={name}
            inputWarning={(allFiledsRequiredMessage && !name)}
            onChange={onChangeSetName}
          />
        </div>
        <div className={panelStyles['Panel-Field']}>
          <label htmlFor="lastName" className={panelStyles['Panel-InputLabel']}>Last name</label>
          <InputField
            id="lastName"
            name="lastName"
            type="text"
            placeholder="James"
            value={surname}
            inputWarning={(allFiledsRequiredMessage && !surname)}
            onChange={onChangeSetSurame}
          />
        </div>
        <div className={panelStyles['Panel-Field']}>
          <label htmlFor="email" className={panelStyles['Panel-InputLabel']}>E-mail</label>
          <InputField
            id="email"
            name="email"
            type="text"
            placeholder="example@example.com"
            value={email}
            inputWarning={(allFiledsRequiredMessage && !email) || emailFormatIncorrectMessage || userExist}
            onChange={onChangeSetEmail}
          />
        </div>
        {emailFormatIncorrectMessage && (
          <AlertMessage messageStyle={AlertMessageStyle.WARNING}>{emailFormatIncorrectMessage}</AlertMessage>
        )}
        {userExist && (
          <AlertMessage messageStyle={AlertMessageStyle.WARNING}>{userExist}</AlertMessage>
        )}
        <div className={panelStyles['Panel-Field']}>
          <label htmlFor="password" className={panelStyles['Panel-InputLabel']}>Password</label>
          <InputField
            id="password"
            name="password"
            type="password"
            placeholder="Exampleofpassword!1"
            value={password}
            inputWarning={
              (allFiledsRequiredMessage && !password) || passwordFormatIncorrectMessage || passwordDoesntMatchMessage
            }
            onChange={onChangeSetPassword}
          />
        </div>
        {passwordFormatIncorrectMessage && (
          <AlertMessage messageStyle={AlertMessageStyle.WARNING}>{passwordFormatIncorrectMessage}</AlertMessage>
        )}
        <div className={panelStyles['Panel-Field']}>
          <label htmlFor="confirmPassword" className={panelStyles['Panel-InputLabel']}>Confirm Password</label>
          <InputField
            id="confirmPassword"
            name="confirmPassword"
            type="password"
            placeholder="Exampleofpassword!1"
            value={confirmPassword}
            inputWarning={(allFiledsRequiredMessage && !confirmPassword) || passwordDoesntMatchMessage}
            onChange={handleConfirmPasswordChange}
          />
        </div>
        {passwordDoesntMatchMessage && (
          <AlertMessage messageStyle={AlertMessageStyle.WARNING}>{passwordDoesntMatchMessage}</AlertMessage>
        )}
        {allFiledsRequiredMessage && (
          <AlertMessage messageStyle={AlertMessageStyle.WARNING}>{allFiledsRequiredMessage}</AlertMessage>
        )}
        <div className={`${styles['RegisterForm-Checkbox']} ${panelStyles['Panel-Field']}`}>
          <input
            id="subscribe"
            type="checkbox"
            onChange={onChangeSetSubscribe}
          />
          <label className={styles['RegisterForm-CheckboxTitle']} htmlFor="subscribe">{SUBSCRIBE_MESSAGE}</label>
        </div>
        <div className={`${styles['RegisterForm-Checkbox']} ${panelStyles['Panel-Field']}`}>
          <input
            id="terms-conditions"
            type="checkbox"
            value="true"
            checked={termsAreComfirmed}
            onChange={onChangeSetTerms}
          />
          <label className={styles['RegisterForm-CheckboxTitle']} htmlFor="terms-conditions">
            I accept  &nbsp;
            <a href=" " onClick={readTermsAndConditions}>Terms of Use</a>
          </label>
        </div>
        {termsAreNotAcceptedMessage && (
          <AlertMessage messageStyle={AlertMessageStyle.WARNING}>{termsAreNotAcceptedMessage}</AlertMessage>
        )}
        <Button type="submit" customStyles={panelStyles['Panel-Button']} dataTestId="signUpButton">Sign up</Button>
        <Button type="button" customStyles={panelStyles['Panel-Button']} dataTestId="backToLoginLink" linkTo="/login" buttonStyle={ButtonStyle.SECONDARY}>Back to login</Button>
      </form>
      <Modal setModalIsVisible={setTermsModalIsVisible} modalIsVisible={termsModalIsVisible}>
        <div className={styles['Terms-Content']}>
          {RULES}
        </div>
      </Modal>
    </>
  );

  return (
    <div>
      {!signUpResponse ? registration() : (
        <AlertMessage customStyles={styles['RegisterForm-Response']}>{signUpResponse}</AlertMessage>
      )}
    </div>
  );
};

export default RegisterPage;
