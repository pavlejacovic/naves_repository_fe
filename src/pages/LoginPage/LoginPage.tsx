import React, { FormEvent, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import AuthenticationService from '../../services/authentication.service';
import { Button, ButtonStyle } from '../../components/Button/Button';
import panelStyles from '../../utils/styles/Panel.module.scss';
import InputField from '../../components/InputField/InputField';
import styles from './LoginPage.module.scss';
import AlertMessage, { AlertMessageStyle } from '../../components/AlertMessage/AlertMessage';
import { EMAIL_AND_PASSWORD_REQUIRED } from '../../utils/constants/validationMessages/validation-messages';

import googleImage from '../../assets/img/google_logo.svg';
import LocalStorageService from '../../services/local-storage.service';

const LoginPage: React.FC = (): JSX.Element => {
  const history = useHistory();
  const baseURL = process.env.REACT_APP_BACKEND_URL;
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const verifiedStatus: string | null = urlParams.get('status');
  const verifiedMessage = 'You have successfully verified your email';
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [emailAndPasswordRequiredMessage, setEmailAndPasswordRequiredMessage] = useState<string>('');
  const [apiErrorMessage, setApiErrorMessage] = useState<string>('');

  const resetForm = () => {
    setApiErrorMessage('');
    setEmailAndPasswordRequiredMessage('');
  };

  function forwardToHomePage(): void {
    history.push('/');
  }

  const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    resetForm();
    setPassword(event.target.value);
  };
  const handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    resetForm();
    setEmail(event.target.value);
  };

  const handleSignInSubmit = (e: FormEvent<HTMLFormElement>): void => {
    e.preventDefault();
    resetForm();
    if (email && password) {
      AuthenticationService.loginUser({ email: email, password: password })
        .then(({ data: { jwt } }) => {
          LocalStorageService.setToken(jwt);
          forwardToHomePage();
        }).catch((error) => {
          switch (error.response.status) {
            case 403: {
              setApiErrorMessage(error.response.data);
              break;
            }
            default: {
              setApiErrorMessage('');
              break;
            }
          }
        });
    } else {
      setEmailAndPasswordRequiredMessage(EMAIL_AND_PASSWORD_REQUIRED);
    }
  };

  const handleSignInGoogleClick = () => {
    window.location.href = `${baseURL}/auth/oauth2/authorization/google`;
  };

  return (
    <form className={panelStyles['Panel']} onSubmit={handleSignInSubmit}>
      {verifiedStatus === 'verified' && (
        <AlertMessage messageStyle={AlertMessageStyle.SUCCESS}>{verifiedMessage}</AlertMessage>
      )}
      <span className={panelStyles['Panel-Title']}>Login</span>
      <div className={panelStyles['Panel-Field']}>
        <label htmlFor="email" className={panelStyles['Panel-InputLabel']}>E-mail</label>
        <InputField
          id="email"
          name="email"
          placeholder="Enter e-mail"
          type="text"
          value={email}
          inputWarning={(!email && emailAndPasswordRequiredMessage)}
          onChange={handleEmailChange}
        />
      </div>
      <div className={panelStyles['Panel-Field']}>
        <label htmlFor="password" className={panelStyles['Panel-InputLabel']}>Password</label>
        <InputField
          id="password"
          name="password"
          placeholder="Enter password"
          type="password"
          value={password}
          inputWarning={(!password && emailAndPasswordRequiredMessage)}
          onChange={handlePasswordChange}
        />
      </div>
      {(emailAndPasswordRequiredMessage) && (
        <AlertMessage messageStyle={AlertMessageStyle.WARNING}>
          {emailAndPasswordRequiredMessage}
        </AlertMessage>
      )}
      <Button type="submit" customStyles={panelStyles['Panel-Button']} data-test-id="signInBtn">Sign in</Button>
      <Button type="button" customStyles={styles['Login-GoogleButton']} data-test-id="googleBtn" onClick={handleSignInGoogleClick} buttonStyle={ButtonStyle.GOOGLE}>
        <img alt="googleLogo" src={googleImage} className={styles['Login-GoogleImage']} />
        <span className={styles['Login-GoogleText']}><b>Sign in with Google</b></span>
      </Button>
      {apiErrorMessage && (
        <AlertMessage messageStyle={AlertMessageStyle.WARNING}>{apiErrorMessage}</AlertMessage>
      )}
      <Link to="/reset" className={`${styles['Login-Link']} ${panelStyles['Panel-Link']}`}>Forgot password?</Link>
      <Button type="button" customStyles={panelStyles['Panel-Button']} dataTestId="SignUpLink" linkTo="/register" buttonStyle={ButtonStyle.SECONDARY}>Sign up</Button>
    </form>
  );
};

export default LoginPage;
