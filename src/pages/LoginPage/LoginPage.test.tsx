import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import LoginPage from './LoginPage';
import '@testing-library/jest-dom';

describe('Login component tests', () => {
  let container: HTMLDivElement;

  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
    ReactDOM.render(<BrowserRouter><LoginPage /></BrowserRouter>, container);
  });

  afterEach(() => {
    document.body.removeChild(container);
    container.remove();
  });

  it('should render component correctly', () => {
    expect(container.querySelector("[id='email']"));
    expect(container.querySelector("[id='password]']"));

    const LABELS = container.querySelectorAll('label');
    expect(LABELS).toHaveLength(2);
    expect(LABELS[0]).toHaveAttribute('for', 'email');
    expect(LABELS[1]).toHaveAttribute('for', 'password');

    expect(container.querySelector('button')).toHaveAttribute('type', 'submit');
  });
});
