import React, {
  useState,
  useContext,
  ChangeEvent
} from 'react';
import { Col, Container, Row } from 'react-grid-system';
import { toast } from 'react-toastify';
import AlertMessage from '../../components/AlertMessage/AlertMessage';
import { Button, ButtonStyle } from '../../components/Button/Button';
import Header from '../../components/Header/Header';
import ProjectFolder from '../../components/ProjectFolder/ProjectFolder';
import { ProjectsContext } from '../../Contexts/ProjectsContext';
import BoxService from '../../services/box.service';
import styles from './MyProjectsPage.module.scss';
import SaveModal from '../../components/SaveModal/SaveModal';
import Select from '../../components/Select/Select';
import InputField from '../../components/InputField/InputField';
import Pagination from '../../components/Pagination/Pagination';
import { PROJECT_PER_PAGE_VALUES, SORT_PROJECT_VALUES } from '../../utils/constants/selectObjects/selectObjects';
import { useUpdateBasePage } from '../../utils/hooks/useLastPage';
import { QueryParamsProjectContext } from '../../Contexts/QueryParamsProjectContext';
import useUpdatePageSortValue from '../../utils/hooks/useUpdateURLSortValue';

const MyProjectsPage: React.FC = (): JSX.Element => {
  const [isCreateProjectModalVisible, setIsCreateProjectModalVisible] = useState<boolean>(false);
  const {
    projectsPerPage,
    setProjectsPerPage,
    projects,
    fetchProjectsOnPage,
    fetchAllMyProjects,
    projectsOnPage
  } = useContext(ProjectsContext);
  const [searchProjectValue, setSearchProjectValue] = useState<string>('');
  const { pageNumber } = useContext(QueryParamsProjectContext);

  const filteredProjects = projects.filter((project) => (
    project.projectName.toLowerCase().includes(searchProjectValue.toLowerCase())));

  const openCreateNewProjectModal = (): void => {
    setIsCreateProjectModalVisible(true);
  };

  const handleSelectProjectPerPage = (event: ChangeEvent<HTMLSelectElement>) => {
    setProjectsPerPage(Number(event.target.value));
  };

  const handleCreateProject = (
    projectName: string,
    setInputText: React.Dispatch<React.SetStateAction<string>>
  ): void => {
    BoxService.createNewProject(projectName).then(
      (response) => {
        setIsCreateProjectModalVisible(false);
        setInputText('');
        fetchProjectsOnPage();
        fetchAllMyProjects();
        toast.success(response.data);
      }
    ).catch(
      () => {
        setInputText('');
        setIsCreateProjectModalVisible(false);
      }
    );
  };

  const handleSearchProjectChange = (event: ChangeEvent<HTMLInputElement>) => {
    setSearchProjectValue(event.target.value);
  };

  const handleSortProject = (event: ChangeEvent<HTMLSelectElement>) => {
    useUpdatePageSortValue(event);
  };

  useUpdateBasePage(projects.length, projectsPerPage);

  return (
    <Container fluid className={styles['MyProjectsPage']}>
      <Row>
        <Col>
          <Header title="My projects">
            <Row justify="end" align="center">
              <Col lg="content" md={6} xs={12}>
                <Select id="sortProjects" onChange={handleSortProject}>
                  <option hidden defaultValue="id">Sort:</option>
                  {Object.entries(SORT_PROJECT_VALUES)
                    .map(([value, label]: string[]) => <option key={`${value}key`} value={value}>{label}</option>)}
                </Select>
              </Col>
              <Col lg="content" md={6} xs={12}>
                <Select id="projectsPerPage" onChange={handleSelectProjectPerPage}>
                  <option hidden defaultValue="12">Projects per page:</option>
                  {Object.entries(PROJECT_PER_PAGE_VALUES)
                    .map(([value, label]: string[]) => <option key={`${value}key`} value={value}>{label}</option>)}
                </Select>
              </Col>
              <Col lg="content" md={6} xs={12}>
                <InputField
                  id="searchProject"
                  name="searchProject"
                  type="text"
                  placeholder="Search project..."
                  dataTestId="searchProjectInputTest"
                  value={searchProjectValue}
                  onChange={handleSearchProjectChange}
                />
              </Col>
              <Col lg="content" md={6} xs={12}><Button type="button" buttonStyle={ButtonStyle.SECONDARY} onClick={openCreateNewProjectModal} dataTestId="createNewProject">Create new</Button></Col>
            </Row>
          </Header>
        </Col>
      </Row>
      <Row>
        {searchProjectValue
          ? filteredProjects.map((project) => (
            <Col xl={3} lg={6} md={12} key={project.id}>
              <Row justify="center" className={styles['MyProjectsPage-ProjectFolder']}>
                <ProjectFolder owner writePermission linkTo={`/projects/${project.id}`} projectId={project.id} projectName={project.projectName || ''} />
              </Row>
            </Col>
          ))
          : projectsOnPage.map((project) => (
            <Col xl={3} lg={6} md={12} key={project.id}>
              <Row justify="center" className={styles['MyProjectsPage-ProjectFolder']}>
                <ProjectFolder owner writePermission linkTo={`/projects/${project.id}`} projectId={project.id} projectName={project.projectName || ''} />
              </Row>
            </Col>
          ))}
        {!projects.length
          && (
            <AlertMessage customStyles={styles['MyProjectsPage-AlertMessage']}>There are no projects.</AlertMessage>
          )}
      </Row>
      <SaveModal
        title="Create new project"
        inputPlaceholder="Enter project name here..."
        modalIsVisible={isCreateProjectModalVisible}
        setModalIsVisible={setIsCreateProjectModalVisible}
        onSave={handleCreateProject}
        customStyle={styles['MyProjectsPage-Modal-ProjectFolderPanel']}
      >
        <Button type="submit" dataTestId="SaveNewProject">Save project</Button>
      </SaveModal>
      {(projects.length > projectsPerPage && !searchProjectValue)
        && (
          <Pagination
            itemsPerPage={projectsPerPage}
            totalNumberOfItems={projects.length}
            pageNumber={pageNumber}
          />
        )}
    </Container>
  );
};

export default MyProjectsPage;
