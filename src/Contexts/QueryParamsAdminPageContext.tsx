import React,
{
  createContext
} from 'react';
import getPageNumberFromURL from '../utils/functions/queryParamsFunctions/queryParamsFunctions';

interface QueryParamsAdminDashboardContextContentInterface {
  pageNumber: number,

}

export const QueryParamsAdminDashboardContext = createContext<QueryParamsAdminDashboardContextContentInterface>({
  pageNumber: 1
});

const QueryParamsAdminDashboardContextProvider: React.FC = ({ children }) => {
  const pageNumber: number = getPageNumberFromURL();

  return (
    <QueryParamsAdminDashboardContext.Provider value={
      {
        pageNumber
      }
    }
    >
      {children}
    </QueryParamsAdminDashboardContext.Provider>
  );
};

export default QueryParamsAdminDashboardContextProvider;
