import React,
{
  createContext
} from 'react';
import { PROJECT_SORT_VALUES } from '../utils/constants/sortConstants/sortConstants';
import getPageNumberFromURL, { getSortValueFromURL } from '../utils/functions/queryParamsFunctions/queryParamsFunctions';

interface QueryParamsContextContentInterface {
  pageNumber: number,
  sortProjectValue: string,
}

export const QueryParamsProjectContext = createContext<QueryParamsContextContentInterface>({
  pageNumber: 1,
  sortProjectValue: ''
});

const QueryParamsProjectContextProvider: React.FC = ({ children }) => {
  const pageNumber: number = getPageNumberFromURL();
  const sortProjectValue: string = getSortValueFromURL(PROJECT_SORT_VALUES);

  return (
    <QueryParamsProjectContext.Provider value={
      {
        pageNumber,
        sortProjectValue
      }
    }
    >
      {children}
    </QueryParamsProjectContext.Provider>
  );
};

export default QueryParamsProjectContextProvider;
