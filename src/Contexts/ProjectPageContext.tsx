import React,
{
  createContext,
  useContext,
  useEffect,
  useState
} from 'react';
import { ProjectContent } from '../models/project.interface';
import BoxService from '../services/box.service';
import { splitPath, trimPath } from '../utils/functions/stringManipulations/stringManipulations/stringManipulations';
import { AuthorizedPagesContext } from './AuthorizedPagesContext';

const EMPTY_PROJECT = {
  project: { projectName: 'Unknown', id: 0 },
  files: [],
  folders: []
};

interface FilesChecked {
  path: string;
  checked: boolean;
}

interface GlobalProjectPageContent {
  project: ProjectContent,
  fetchFoldersAndFiles: (path: string) => void,
  errorMessage: string,
  setErrorMessage: (value: string) => void,
  checkedFilesAndFolders: FilesChecked[],
  setCheckedFilesAndFolders: (value: FilesChecked[]) => void
}

export const ProjectPageContext = createContext<GlobalProjectPageContent>({
  project: EMPTY_PROJECT,
  errorMessage: '',
  setErrorMessage: () => undefined,
  fetchFoldersAndFiles: () => undefined,
  checkedFilesAndFolders: [],
  setCheckedFilesAndFolders: () => undefined
});

const ProjectPageContextProvider: React.FC = ({ children }) => {
  const [project, setProject] = useState<ProjectContent>(EMPTY_PROJECT);
  const [errorMessage, setErrorMessage] = useState<string>('');
  const { pathToTheSource, currentPath } = useContext(AuthorizedPagesContext);
  const [checkedFilesAndFolders, setCheckedFilesAndFolders] = useState<FilesChecked[]>([]);
  const fetchFoldersAndFiles = (path: string): void => {
    const fullPath = splitPath(path);
    if (fullPath[0]) {
      BoxService.fetchAllFoldersAndFilesInProject(fullPath)
        .then((response) => {
          setProject(response.data);
          const checkedFilesInitialState: FilesChecked[] = [];
          response.data.folders.forEach((folder) => {
            checkedFilesInitialState.push({ path: `${folder.folderPath}`, checked: false });
          });
          response.data.files.forEach((file) => {
            checkedFilesInitialState.push({ path: `${splitPath(currentPath)[1] || ''}/${trimPath(file.filePath)}`, checked: false });
          });
          setCheckedFilesAndFolders(checkedFilesInitialState);
        });
    }
  };

  useEffect(() => {
    fetchFoldersAndFiles(pathToTheSource);
  }, [pathToTheSource]);

  return (
    <ProjectPageContext.Provider value={
      {
        project,
        fetchFoldersAndFiles,
        errorMessage,
        setErrorMessage,
        checkedFilesAndFolders,
        setCheckedFilesAndFolders
      }
    }
    >
      {children}
    </ProjectPageContext.Provider>
  );
};

export default ProjectPageContextProvider;
