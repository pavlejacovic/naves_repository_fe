import React,
{
  createContext,
  useState,
  useEffect,
  useContext
} from 'react';
import { ProjectInfo } from '../models/project.interface';
import boxService from '../services/box.service';
import { QueryParamsAdminDashboardContext } from './QueryParamsAdminPageContext';

interface AdminDashboardInfoContextContentInterface {
     projectsInfo: ProjectInfo[];
     projectsInfoPerPage: ProjectInfo[];
     projectsInfoPageSize: number;
     setProjectsInfoPageSize: (value: number) => void
}

export const AdminDashboardInfoContext = createContext<AdminDashboardInfoContextContentInterface>({
  projectsInfo: [],
  projectsInfoPerPage: [],
  projectsInfoPageSize: 8,
  setProjectsInfoPageSize: () => undefined
});

const AdminDashboardInfoContextProvider: React.FC = ({ children }) => {
  const [projectsInfo, setProjectsInfo] = useState<ProjectInfo[]>([]);
  const [projectsInfoPerPage, setProjectsInfoPerPage] = useState<ProjectInfo[]>([]);
  const [projectsInfoPageSize, setProjectsInfoPageSize] = useState<number>(8);
  const { pageNumber } = useContext(QueryParamsAdminDashboardContext);

  useEffect(() => {
    boxService.getProjectsForAdmin().then((response) => {
      setProjectsInfo(response.data);
    });
  }, []);

  useEffect(() => {
    boxService.getProjectsForAdminPerPage(pageNumber - 1, projectsInfoPageSize).then((response) => {
      setProjectsInfoPerPage(response.data);
    });
  }, [pageNumber]);

  return (
    <AdminDashboardInfoContext.Provider value={
      {
        projectsInfo,
        projectsInfoPageSize,
        projectsInfoPerPage,
        setProjectsInfoPageSize
      }
    }
    >
      {children}
    </AdminDashboardInfoContext.Provider>
  );
};

export default AdminDashboardInfoContextProvider;
