import React,
{
  createContext,
  useState,
  useEffect,
  useContext
} from 'react';
import { NavesFunctionInfo } from '../models/function.interface';
import navesFunctionsService from '../services/naves-functions.service';
import { QueryParamsFunctionsContext } from './QueryParamsFunctionContext';

interface NavesFunctionsContextContentInterface {
  functions: Array<NavesFunctionInfo>,
  functionsOnPage: Array<NavesFunctionInfo>,
  fetchAllMyFunctions(): void,
  fetchFunctionsOnPage(): void,
  functionsPerPage: number,
  setFunctionsPerPage: (value: number) => void
}

export const NavesFunctionsContext = createContext<NavesFunctionsContextContentInterface>({
  functions: [],
  fetchAllMyFunctions: () => undefined,
  functionsOnPage: [],
  fetchFunctionsOnPage: () => undefined,
  functionsPerPage: 12,
  setFunctionsPerPage: () => undefined
});

const NavesFunctionsContextProvider: React.FC = ({ children }) => {
  const [functions, setFunctions] = useState<Array<NavesFunctionInfo>>([]);
  const [functionsOnPage, setFunctionsOnPage] = useState<Array<NavesFunctionInfo>>([]);
  const [functionsPerPage, setFunctionsPerPage] = useState<number>(12);
  const { pageNumber, sortFunctionValue } = useContext(QueryParamsFunctionsContext);

  const fetchAllMyFunctions = (): void => {
    navesFunctionsService.getAllMyFunctions().then((response) => {
      setFunctions(response.data);
    });
  };

  useEffect(() => {
    fetchAllMyFunctions();
  }, []);

  const fetchFunctionsOnPage = (): void => {
    navesFunctionsService.getFunctionsPerPage(functionsPerPage, pageNumber - 1, sortFunctionValue).then((response) => {
      setFunctionsOnPage(response.data);
    });
  };

  useEffect(() => {
    fetchFunctionsOnPage();
  }, [pageNumber, functionsPerPage, sortFunctionValue]);

  return (
    <NavesFunctionsContext.Provider value={
      {
        functions,
        functionsOnPage,
        fetchAllMyFunctions,
        fetchFunctionsOnPage,
        functionsPerPage,
        setFunctionsPerPage
      }
    }
    >
      {children}
    </NavesFunctionsContext.Provider>
  );
};

export default NavesFunctionsContextProvider;
