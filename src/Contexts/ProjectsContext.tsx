import React,
{
  createContext,
  useState,
  useEffect,
  useContext
} from 'react';
import { toast } from 'react-toastify';
import { Project, ProjectUsers } from '../models/project.interface';
import BoxService from '../services/box.service';
import { QueryParamsProjectContext } from './QueryParamsProjectContext';
import { QueryParamsShareProjectContext } from './QueryParamsShareProjectContext';

interface GlobalProjectsContent {
  projectsPerPage: number;
  setProjectsPerPage: (value: number) => void;
  projects: Project[],
  sharedWithMeProjects: Project[],
  fetchAllMyProjects(): void,
  fetchAllSharedProjects(): void,
  projectsOnPage: Project[],
  sharedProjectsOnPage: Project[],
  fetchProjectsOnPage: () => void,
  fetchSharedWithMeProjectsOnPage: () => void,
  projectUsers: ProjectUsers[],
  fetchProjectUsers: (projectId: number) => void
}

export const ProjectsContext = createContext<GlobalProjectsContent>({
  projectsPerPage: 12,
  setProjectsPerPage: () => undefined,
  projects: [],
  sharedWithMeProjects: [],
  sharedProjectsOnPage: [],
  fetchAllMyProjects: () => undefined,
  fetchAllSharedProjects: () => undefined,
  fetchSharedWithMeProjectsOnPage: () => undefined,
  projectsOnPage: [],
  fetchProjectsOnPage: () => undefined,
  projectUsers: [],
  fetchProjectUsers: () => undefined
});

const ProjectsContextProvider: React.FC = ({ children }) => {
  const [projects, setProjects] = useState<Project[]>([]);
  const [sharedWithMeProjects, setSharedWithMeProjects] = useState<Project[]>([]);
  const [projectsOnPage, setProjectsOnPage] = useState<Project[]>([]);
  const [sharedProjectsOnPage, setSharedProjectsOnPage] = useState<Project[]>([]);
  const [projectsPerPage, setProjectsPerPage] = useState<number>(12);
  const { pageNumber, sortProjectValue } = useContext(QueryParamsProjectContext);
  const { page, sortSharedProjectValue } = useContext(QueryParamsShareProjectContext);
  const [projectUsers, setProjectUsers] = useState<ProjectUsers[]>([]);

  const fetchAllMyProjects = (): void => {
    BoxService.fetchAllMyProjects().then((response) => {
      setProjects(response.data);
    });
  };

  useEffect(() => {
    fetchAllMyProjects();
  }, []);

  const fetchAllSharedProjects = (): void => {
    BoxService.getAllSharedWithMeProjects().then((response) => {
      setSharedWithMeProjects(response.data.map((project) => {
        const projectObj = project.project;
        projectObj.writePermission = project.writePermission;
        return projectObj;
      }));
    });
  };

  useEffect(() => {
    fetchAllSharedProjects();
  }, []);

  const fetchProjectsOnPage = (): void => {
    BoxService.getProjectsPerPage(projectsPerPage, pageNumber - 1, sortProjectValue).then((response) => {
      setProjectsOnPage(response.data.content);
    }).catch((error) => toast.error(error.response.data.content));
  };

  useEffect(() => {
    fetchProjectsOnPage();
  }, [pageNumber, projectsPerPage, sortProjectValue]);

  const fetchSharedWithMeProjectsOnPage = (): void => {
    BoxService.getSharedWithMeProjectsPerPage(projectsPerPage, page - 1, sortSharedProjectValue)
      .then((response) => {
        setSharedProjectsOnPage(response.data.content.map((project: Project) => {
          const projectObj = project.project;
          projectObj.writePermission = project.writePermission;
          return projectObj;
        }));
      }).catch((error) => toast.error(error.response.data.content));
  };

  useEffect(() => {
    fetchSharedWithMeProjectsOnPage();
  }, [page, projectsPerPage, sortSharedProjectValue]);

  const fetchProjectUsers = (projectId: number) => {
    BoxService.getProjectUsers(projectId).then((response) => setProjectUsers(response.data));
  };

  return (
    <ProjectsContext.Provider value={
      {
        projectsPerPage,
        setProjectsPerPage,
        projects,
        sharedWithMeProjects,
        sharedProjectsOnPage,
        fetchAllSharedProjects,
        fetchSharedWithMeProjectsOnPage,
        fetchAllMyProjects,
        projectsOnPage,
        fetchProjectsOnPage,
        projectUsers,
        fetchProjectUsers
      }
    }
    >
      {children}
    </ProjectsContext.Provider>
  );
};

export default ProjectsContextProvider;
