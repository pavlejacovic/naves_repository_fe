import React,
{
  createContext,
  useState,
  useEffect
} from 'react';

interface GlobalAuthorizedPagesContext {
  currentPath: string,
  pathToTheSource: string
}

export const AuthorizedPagesContext = createContext<GlobalAuthorizedPagesContext>({
  currentPath: '',
  pathToTheSource: ''
});

const AuthorizedPagesContextProvider: React.FC = ({ children }) => {
  const currentPath = decodeURIComponent(window.location.pathname);
  const [pathToTheSource, setPathToTheSource] = useState<string>(currentPath);

  useEffect(() => {
    if (pathToTheSource !== currentPath) {
      setPathToTheSource(currentPath);
    }
  }, [currentPath]);

  return (
    <AuthorizedPagesContext.Provider value={
      {
        currentPath,
        pathToTheSource
      }
    }
    >
      {children}
    </AuthorizedPagesContext.Provider>
  );
};

export default AuthorizedPagesContextProvider;
