import React,
{
  createContext
} from 'react';
import { FUNCTION_SORT_VALUES } from '../utils/constants/sortConstants/sortConstants';
import getPageNumberFromURL, { getSortValueFromURL } from '../utils/functions/queryParamsFunctions/queryParamsFunctions';

interface QueryParamsContextContentInterface {
  pageNumber: number,
  sortFunctionValue: string
}

export const QueryParamsFunctionsContext = createContext<QueryParamsContextContentInterface>({
  pageNumber: 1,
  sortFunctionValue: ''
});

const QueryParamsFunctionsContextProvider: React.FC = ({ children }) => {
  const pageNumber: number = getPageNumberFromURL();
  const sortFunctionValue: string = getSortValueFromURL(FUNCTION_SORT_VALUES);

  return (
    <QueryParamsFunctionsContext.Provider value={
      {
        pageNumber,
        sortFunctionValue
      }
    }
    >
      {children}
    </QueryParamsFunctionsContext.Provider>
  );
};

export default QueryParamsFunctionsContextProvider;
