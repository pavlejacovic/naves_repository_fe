import React,
{
  createContext
} from 'react';
import { PROJECT_SHARED_SORT_VALUES } from '../utils/constants/sortConstants/sortConstants';
import getPageNumberFromURL, { getSortValueFromURL } from '../utils/functions/queryParamsFunctions/queryParamsFunctions';

interface QueryParamsShareContextContentInterface {
  page: number,
  sortSharedProjectValue: string
}

export const QueryParamsShareProjectContext = createContext<QueryParamsShareContextContentInterface>({
  page: 1,
  sortSharedProjectValue: ''
});

const QueryParamsShareProjectContextProvider: React.FC = ({ children }) => {
  const page: number = getPageNumberFromURL();
  const sortSharedProjectValue: string = getSortValueFromURL(PROJECT_SHARED_SORT_VALUES);

  return (
    <QueryParamsShareProjectContext.Provider value={
      {
        page,
        sortSharedProjectValue
      }
    }
    >
      {children}
    </QueryParamsShareProjectContext.Provider>
  );
};

export default QueryParamsShareProjectContextProvider;
