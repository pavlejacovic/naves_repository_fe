import React from 'react';
import './App.scss';
import {
  Redirect,
  Router,
  Switch
} from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import LoginPage from './pages/LoginPage/LoginPage';
import ConfirmAccountPage from './pages/ConfirmAccountPage/ConfirmAccountPage';
import ForgotPasswordPage from './pages/ForgotPasswordPage/ForgotPasswordPage';
import ResetPasswordPage from './pages/ResetPasswordPage/ResetPasswordPage';
import RegisterPage from './pages/RegisterPage/RegisterPage';
import history from './utils/constants/history/history';
import OauthConfirmationPage from './pages/OauthConfirmationPage/OauthConfirmationPage';
import AuthorizedRoute from './auth/AuthorizedRoute';
import AuthenticationRoute from './auth/AuthenticationRoute';
import ProjectPage from './pages/ProjectPage/ProjectPage';
import MyProjectsPage from './pages/MyProjectsPage/MyProjectsPage';
import SharedWithMePage from './pages/SharedWithMePage/SharedWithMePage';
import 'react-toastify/dist/ReactToastify.css';
import ProjectsContextProvider from './Contexts/ProjectsContext';
import ProjectPageContextProvider from './Contexts/ProjectPageContext';
import NavesFunctionsPage from './pages/NavesFunctionsPage/NavesFunctionsPage';
import NavesFunctionsContextProvider from './Contexts/NavesFunctionsContext';
import UserProfilePage from './pages/UserProfilePage/UserProfilePage';
import QueryParamsFunctionsContextProvider from './Contexts/QueryParamsFunctionContext';
import QueryParamsProjectContextProvider from './Contexts/QueryParamsProjectContext';
import DashboardPage from './pages/DashboardPage/DashboardPage';
import QueryParamsShareProjectContextProvider from './Contexts/QueryParamsShareProjectContext';
import AdminDashboardInfoContextProvider from './Contexts/AdminDashboardInfoContext';
import QueryParamsAdminDashboardContextProvider from './Contexts/QueryParamsAdminPageContext';

function App() {
  return (
    <div className="App">
      <Router history={history}>
        <Switch>
          <AuthenticationRoute path="/login" component={LoginPage} />
          <AuthenticationRoute path="/confirm-account" component={ConfirmAccountPage} />
          <AuthenticationRoute path="/reset" component={ForgotPasswordPage} />
          <AuthenticationRoute path="/reset_password" component={ResetPasswordPage} />
          <AuthenticationRoute path="/register" component={RegisterPage} />
          <AuthenticationRoute path="/oauth" component={OauthConfirmationPage} />
          <AuthorizedRoute exact path="/projects/*">
            <ProjectPageContextProvider>
              <ProjectPage />
            </ProjectPageContextProvider>
          </AuthorizedRoute>
          <AuthorizedRoute exact path="/shared-with-me/*">
            <ProjectPageContextProvider>
              <ProjectPage />
            </ProjectPageContextProvider>
          </AuthorizedRoute>
          <AuthorizedRoute exact path="/projects">
            <QueryParamsProjectContextProvider>
              <ProjectsContextProvider>
                <MyProjectsPage />
              </ProjectsContextProvider>
            </QueryParamsProjectContextProvider>
          </AuthorizedRoute>
          <AuthorizedRoute exact path="/shared-with-me">
            <QueryParamsShareProjectContextProvider>
              <ProjectsContextProvider>
                <SharedWithMePage />
              </ProjectsContextProvider>
            </QueryParamsShareProjectContextProvider>
          </AuthorizedRoute>
          <AuthorizedRoute exact path="/naves-functions">
            <QueryParamsFunctionsContextProvider>
              <ProjectsContextProvider>
                <NavesFunctionsContextProvider>
                  <NavesFunctionsPage />
                </NavesFunctionsContextProvider>
              </ProjectsContextProvider>
            </QueryParamsFunctionsContextProvider>
          </AuthorizedRoute>
          <AuthorizedRoute exact path="/admin">
            <QueryParamsAdminDashboardContextProvider>
              <AdminDashboardInfoContextProvider>
                <DashboardPage />
              </AdminDashboardInfoContextProvider>
            </QueryParamsAdminDashboardContextProvider>
          </AuthorizedRoute>
          <AuthorizedRoute exact path="/profile" component={UserProfilePage} />
          <Redirect from="*" to="/projects" />
        </Switch>
      </Router>
      <ToastContainer autoClose={3000} hideProgressBar position="bottom-right" theme="colored" />
    </div>
  );
}

export default App;
