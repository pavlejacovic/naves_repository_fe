import axios from 'axios';
import { toast } from 'react-toastify';
import history from '../utils/constants/history/history';
import LocalStorageService from '../services/local-storage.service';

const baseURL = process.env.REACT_APP_BACKEND_URL;

const NOT_TOASTED_ERRORS: Object = {
  418: true,
  403: true
};

const http = axios.create({ baseURL: baseURL });

http.interceptors.request.use(
  (request) => {
    if (LocalStorageService.hasToken()) {
      request.headers['Authorization'] = localStorage.getItem('token');
    }
    return request;
  },
  (error) => Promise.reject(error)
);

http.interceptors.response.use(
  (response) => response,
  (error) => {
    if (error.response.status === 401) {
      if (LocalStorageService.hasToken()) {
        LocalStorageService.removeToken();
      }
      history.push('/login');
    } else if (error.response.status in NOT_TOASTED_ERRORS) {
      return Promise.reject(error);
    } else {
      toast.error(error.response.data);
    }
    return Promise.reject(error);
  }
);

export default http;
