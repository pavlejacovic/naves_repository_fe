import { useEffect } from 'react';
import history from '../constants/history/history';
import { changeURLParamsLink, getBaseRoute } from '../functions/queryParamsFunctions/queryParamsFunctions';

const useUpdateCurrentPage = (totalNumberOfPages: number, currentPageNumber: number) => {
  useEffect(() => {
    if (totalNumberOfPages < currentPageNumber && totalNumberOfPages) {
      history.push(changeURLParamsLink('page', totalNumberOfPages.toString()));
    }
  }, [totalNumberOfPages]);
};

export const useUpdateBasePage = (totalNumberOfItems: number, itemsPerPage: Number) => {
  useEffect(() => {
    if (totalNumberOfItems === itemsPerPage) {
      history.push(getBaseRoute());
    }
  }, [totalNumberOfItems]);
};

export default useUpdateCurrentPage;
