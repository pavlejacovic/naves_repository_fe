import { ChangeEvent } from 'react';
import history from '../constants/history/history';
import { changeURLParamsLink } from '../functions/queryParamsFunctions/queryParamsFunctions';

const useUpdatePageSortValue = (event: ChangeEvent<HTMLSelectElement>) => {
  const sortValue: string = event.target.value;
  history.push(changeURLParamsLink('sort', sortValue));
};

export default useUpdatePageSortValue;
