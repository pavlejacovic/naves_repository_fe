const dateConversion = (date: string | null): string => new Date(date || 0).toISOString().slice(0, 10);

export default dateConversion;
