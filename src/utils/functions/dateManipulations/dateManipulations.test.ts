import dateConversion from './dateManipulations';

describe('date conversion', () => {
  it('should return date in format "YYYY-MM-DD" ', () => {
    expect(dateConversion('2021-09-01T09:38:13')).toBe('2021-09-01');
  });

  it('should return date in format "YYYY-MM-DD" ', () => {
    expect(dateConversion('2000-01-01T00:00:00')).toBe('2000-01-01');
  });

  it('should return date in format "YYYY-MM-DD" ', () => {
    expect(dateConversion('1994-11-09T09:38:13')).toBe('1994-11-09');
  });

  it('should return date in format "YYYY-MM-DD" ', () => {
    expect(dateConversion('2002-22-22T09:38:13')).toBe('2222-22-22');
  });

  it('should return date in format "YYYY-MM-DD" ', () => {
    expect(dateConversion('2000-11-01T09:38:13')).toBe('2000-11-01');
  });
});
