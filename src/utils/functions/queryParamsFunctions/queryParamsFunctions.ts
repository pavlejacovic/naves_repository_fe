import { useLocation } from 'react-router-dom';

const getPageNumberFromURL = (): number => {
  const location = useLocation();
  const urlParams = new URLSearchParams(location.search);
  const pageNumber: number = Number(urlParams.get('page'));
  if (!pageNumber) {
    return 1;
  }
  return pageNumber;
};

export const getSortValueFromURL = (possibleSortValues: string[]): string => {
  const location = useLocation();
  const urlParams = new URLSearchParams(location.search);
  const sortValue: string | null = urlParams.get('sort');

  if (sortValue && possibleSortValues.includes(sortValue)) {
    return sortValue;
  }

  return possibleSortValues[0];
};

export const changeURLParamsLink = (searchParam: string, value: string): string => {
  const url = new URL(window.location.href);
  const { searchParams } = url;
  searchParams.set(`${searchParam}`, `${value}`);
  url.search = searchParams.toString();
  const path = `${url.pathname}${url.search}`;
  return decodeURIComponent(path);
};

export const getBaseRoute = (): string => {
  const url = new URL(window.location.href);
  const path = `${url.pathname}`;
  return decodeURIComponent(path);
};

export default getPageNumberFromURL;
