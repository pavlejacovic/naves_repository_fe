import getClassName from './classesHelper';

describe('joining class names into a single string', () => {
  it('should return string with class names', () => {
    expect(getClassName('Header','Header-Button', {'Button': true, 'Checkbox': false})).toBe('Header Header-Button Button');
  });

  it('should return string with class names', () => {
    expect(getClassName('Header','Header-Button', {'Button': true, 'Checkbox': true})).toBe('Header Header-Button Button Checkbox');
  });

  it('should return string with class names', () => {
    expect(getClassName('Header','Header-Button', {'Button': false, 'Checkbox': false})).toBe('Header Header-Button');
  });

  it('should return string with class names', () => {
    expect(getClassName('Header','Header-Button')).toBe('Header Header-Button');
  });

  it('should return string with class names', () => {
    expect(getClassName({'Button': true, 'Checkbox': false})).toBe('Button');
  });

  it('should return string with class names', () => {
    expect(getClassName({'Button': true, 'Checkbox': false, 'Input': true})).toBe('Button Input');
  });

  it('should return empty string if there are no classes passed', () => {
    expect(getClassName()).toBe('');
  });
});
