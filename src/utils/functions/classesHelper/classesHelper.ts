/* *
* This function should join class names into a single string.
*
* @param classes - unlimited number of classes, which can be in string or object format.
*                 if the passed argument is in object format, keys should be classes which should be considered for joining string,
*                 and values should be conditions based on which those classes will be joined in string or not
* @returns string - string with classes
* */

function getClassName(...classes: any[]): string {
  const result = [];
  for (let i = 0; i < classes.length; i++) {
    if (typeof classes[i] === 'object') {
      const keys = Object.keys(classes[i]);
      keys.forEach((key) => {
        if (classes[i][key]) {
          result.push(key);
        }
      });
    } else if (typeof classes[i] === 'string') {
      result.push(classes[i]);
    }
  }
  return result.join(' ');
}

export default getClassName;
