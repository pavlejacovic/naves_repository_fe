const bytesToMegaBytes = (bytes: number | undefined): string | null => {
  if (bytes) {
    return `${(bytes / (1024 * 1024)).toFixed(2)} MB`;
  }
  return null;
};

export default bytesToMegaBytes;
