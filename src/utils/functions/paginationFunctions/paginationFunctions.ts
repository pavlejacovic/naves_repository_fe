const countNumberOfPages = (totalNumberOfItems: number, itemsPerPage: number): number => (
  Math.ceil(totalNumberOfItems / itemsPerPage));

export default countNumberOfPages;
