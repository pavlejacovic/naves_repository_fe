import { getFileNameFromFile } from './stringManipulations';

describe('get file name from file name with extension', () => {
  it('should return name of the file without extension', () => {
    expect(getFileNameFromFile('hello.tsx')).toBe('hello');
  });

  it('should return name of the file without extension', () => {
    expect(getFileNameFromFile('hello/.pdf')).toBe('hello/');
  });

  it('should return name of the file without extension', () => {
    expect(getFileNameFromFile('hello..jpg')).toBe('hello.');
  });

  it('should return name of the file without extension', () => {
    expect(getFileNameFromFile('h.e.l.l.o.png')).toBe('h.e.l.l.o');
  });

  it('should return name of the file without extension', () => {
    expect(getFileNameFromFile('&(*&...tsx')).toBe('&(*&..');
  });

  it('should return name of the file without extension', () => {
    expect(getFileNameFromFile('pdf.jpg.png.doc.zip.tsx')).toBe('pdf.jpg.png.doc.zip');
  });
});
