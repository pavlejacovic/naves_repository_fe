import { splitPath } from './stringManipulations';

const baseURL = process.env.REACT_APP_BACKEND_URL;

describe('splited url', () => {
  it('should return array with domain and path', () => {
    expect(splitPath(`${baseURL}/SphericalDelightfulActivecontent#script.js`)[0]).toBe('localhost:9191');
    expect(splitPath(`${baseURL}/SphericalDelightfulActivecontent#script.js`)[1]).toBe('SphericalDelightfulActivecontent#script.js');
  });

  it('should return array with domain and path', () => {
    expect(splitPath(`${baseURL}/projects/1/hello/hi`)[0]).toBe('localhost:9191');
    expect(splitPath(`${baseURL}/projects/1/hello/hi`)[1]).toBe('projects/1/hello/hi');
  });

  it('should return array with domain', () => {
    expect(splitPath(`${baseURL}`)[0]).toBe('localhost:9191');
    expect(splitPath(`${baseURL}`)[1]).toBe(undefined);
  });

  it('should return array with domain and path', () => {
    expect(splitPath(`${baseURL}/js/default.asp`)[0]).toBe('localhost:9191');
    expect(splitPath(`${baseURL}/js/default.asp`)[1]).toBe('js/default.asp');
  });

  it('should return empty array', () => {
    expect(splitPath('')[0]).toBe(undefined);
  });
});
