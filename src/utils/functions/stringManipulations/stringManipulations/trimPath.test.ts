import { trimPath } from './stringManipulations';

const baseURL = process.env.REACT_APP_BACKEND_URL;

describe('name of file is', () => {
  it('should return name of file', () => {
    expect(trimPath(`${baseURL}/SphericalDelightfulActivecontent#script.js`)).toBe('SphericalDelightfulActivecontent#script.js');
  });

  it('should return name of file', () => {
    expect(trimPath(`${baseURL}/js/default.asp`)).toBe('default.asp');
  });

  it('should return name of folder', () => {
    expect(trimPath(`${baseURL}/projects/1/hello/hi`)).toBe('hi');
  });

  it('should return name of folder', () => {
    expect(trimPath(`${baseURL}/projects/1/hello`)).toBe('hello');
  });

  it('should return empty string', () => {
    expect(trimPath(undefined)).toBe('');
  });
});
