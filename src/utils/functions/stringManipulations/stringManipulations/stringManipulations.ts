/* *
* This function provides last part of the path - folder or file which is required.
*
* @param path - received path which leads to some folder or file
* @returns string - part of the path after last '/' character
* */
const trimPath = (path: string | undefined): string => {
  const pathParts = path?.split('\\');
  const expected = pathParts?.pop();
  if (expected) {
    return expected;
  }
  return '';
};
const trimPathReverse = (path: string | undefined): string => {
  const pathParts = path?.split('\\');
  const expected = pathParts?.pop();
  if (expected) {
    return expected;
  }
  return '';
};

/* *
* This function provides path to the source which is required.
*
* @param url - full url address
* @returns array - with two strings, first one is domain and the second one is path to the source
* */
const splitPath = (path: string): Array<string> => {
  const pathParts = path.split('/');
  const pathToTheSource = pathParts?.slice(2);
  if (pathToTheSource.length === 1) {
    return pathToTheSource;
  }
  if (pathToTheSource.length > 1) {
    const pathToTheDirectory = pathToTheSource.splice(1);
    pathToTheSource.push(pathToTheDirectory.join('/'));
    return pathToTheSource;
  }
  return [];
};

/* *
* This function takes the file name with extension as a parameter - type string
*
* @returns file name - type string
* */

const getFileNameFromFile = (fileNameWithExtension: string): string => {
  if (fileNameWithExtension.includes('.')) {
    const dotExtensionIndex: number = fileNameWithExtension.lastIndexOf('.');
    const fileName: string = fileNameWithExtension.slice(0, dotExtensionIndex);
    return fileName;
  }
  return fileNameWithExtension;
};

/* *
* This function takes the file name with extension as a parameter - type string
*
* @returns file extension - type string
* */
const getFileExtensionFromFile = (fileNameWithExtension: string): string | null => {
  if (fileNameWithExtension.includes('.')) {
    const dotExtensionIndex: number = fileNameWithExtension.lastIndexOf('.');
    const fileExtension: string = fileNameWithExtension.slice(dotExtensionIndex + 1);
    return fileExtension;
  }
  return null;
};

export {
  trimPath,
  splitPath,
  trimPathReverse,
  getFileExtensionFromFile,
  getFileNameFromFile
};
