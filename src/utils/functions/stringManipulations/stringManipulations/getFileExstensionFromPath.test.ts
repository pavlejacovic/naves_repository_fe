import { getFileExtensionFromFile } from './stringManipulations';

describe('get file extension from file name with extension', () => {
  it('should return name of the file without extension', () => {
    expect(getFileExtensionFromFile('hello.tsx')).toBe('tsx');
  });

  it('should return name of the file without extension', () => {
    expect(getFileExtensionFromFile('hello/.pdf')).toBe('pdf');
  });

  it('should return name of the file without extension', () => {
    expect(getFileExtensionFromFile('hello..jpg')).toBe('jpg');
  });

  it('should return name of the file without extension', () => {
    expect(getFileExtensionFromFile('h.e.l.l.o.png')).toBe('png');
  });

  it('should return name of the file without extension', () => {
    expect(getFileExtensionFromFile('&(*&...tsx')).toBe('tsx');
  });

  it('should return name of the file without extension', () => {
    expect(getFileExtensionFromFile('pdf.jpg.png.doc.zip.tsx')).toBe('tsx');
  });
});
