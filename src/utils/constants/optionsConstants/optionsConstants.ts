export const TRIGGER_OPTIONS = [
  { value: 'POST', label: 'POST' },
  { value: 'GET', label: 'GET' },
  { value: 'BUCKET', label: 'BUCKET' }
];

export const ACCESS_OPTIONS = [
  { value: true, label: 'PUBLIC' },
  { value: false, label: 'PRIVATE' }
];

export const BUCKET_TYPE_OPTIONS = [
  { value: 'BUCKET_UPLOAD_FILE', label: 'UPLOAD' },
  { value: 'BUCKET_DOWNLOAD_FILE', label: 'DOWNLOAD' },
  { value: 'BUCKET_DELETE_FILE', label: 'DELETE' }
];

export const LANGUAGE_OPTIONS = [
  { value: 'JAVA', label: 'Java' },
  { value: 'CSHARP', label: 'C#' }
];
