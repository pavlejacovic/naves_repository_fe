const SORT_QUERY_PARAMS: string = '&sort=';
export const FUNCTION_SORT_VALUES: string[] = ['functionId,desc', 'functionId', 'functionName', 'functionName,desc'];
export const PROJECT_SORT_VALUES: string[] = ['id,desc', 'id', 'projectName', 'projectName,desc', 'projectSize', 'projectSize,desc'];
export const PROJECT_SHARED_SORT_VALUES: string[] = ['project.id,desc', 'project.id', 'project.projectName', 'project.projectName,desc', 'project.projectSize,desc', 'project.projectSize'];

export default SORT_QUERY_PARAMS;
