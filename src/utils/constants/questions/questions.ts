const QUESTION_DELETING_FILE: string = 'Are you sure you want to delete this file?';
const QUESTION_DELETING_FOLDER: string = 'Are you sure you want to delete this folder?';
const QUESTION_DELETING_FILES: string = 'Are you sure you want to delete checked files';
const SIGN_OUT_QUESTION: string = 'Are you sure you want to sign out?';
const SUBSCRIBE_MESSAGE: string = 'Do you want to receive mail about usage statistics?';
const QUESTION_DELETING_NAVES_FUNCTION: string = 'Are you sure you want to delete this function';
const QUESTION_DELETING_PERMISSION: string = 'Are you sure you want to remove permission for ';

export {
  QUESTION_DELETING_FILES,
  QUESTION_DELETING_FILE,
  QUESTION_DELETING_FOLDER,
  QUESTION_DELETING_NAVES_FUNCTION,
  QUESTION_DELETING_PERMISSION,
  SIGN_OUT_QUESTION,
  SUBSCRIBE_MESSAGE
};
