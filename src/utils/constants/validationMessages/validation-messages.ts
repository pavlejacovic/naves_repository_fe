const ALL_FILEDS_REQUIRED = 'All fileds are required.';
const EMAIL_AND_PASSWORD_REQUIRED = 'Enter both - email and password.';
const INVALID_EMAIL_FORMAT = 'Invalid email format.';
const EMAIL_REQUIRED = ' Please enter your email address.';
const PASSWORD_DONT_MATCH = "Passwords don't match";
const TERMS_AND_CONDITIONS_REQUIRED = 'Terms and conditions must be accepted to proceed.';
const FIELD_IS_REQUIRED = 'This field is required!';

const PASSWORD_LENGTH_REQUIRED: string = 'The password must contain minimum 8 characters.';
const PASSWORD_NUMBER_REQUIRED: string = 'The password must contain at least one number.';
const PASSWORD_UPPERCASE_REQUIRED: string = 'The password must contain at least one uppercase letter.';
const PASSWORD_LOWERCASE_REQUIRED: string = 'The password must contain at least one lowercase letter.';
const PASSWORD_SPECIAL_CHARACTER_REQUIRED: string = 'The password must contain at least one of the characters !@#$%^&*';

const FILES_FOR_DOWNLOAD_REQUIRED: string = 'Please check the files you want to download.';
const FILES_FOR_DELETE_REQUIRED: string = 'Please check the files you want to delete.';
const SPACE_EXCEEDED: string = "You don't have enough space for uploading. Available space is ";
const FILES_ARE_NOT_SPECIFIED: string = "You didn't specify any file for upload";
const LANGUAGE_IS_NOT_SELECTED: string = "You didn't select programming language";
const MAXIMUM_FILES_NUMBER: string = 'Maximum number of files for upload is 10.';

const NO_PERMISSION_WERE_GIVEN: string = 'You are not sharing this project with anyone';
const MAXIMUM_DAYS_REQUIRED: string = 'Maximum number of days in the past is 365.';
const FUNCTION_NAME_REQUIREMENTS: string = 'Function name can only contain lowercase characters(a-z), numbers(0-9) and dash(-)';
const JAR_FILE_IS_REQUIRED: string = 'Jar file is required!';
const LANGUAGE_IS_REQUIRED: string = 'Language is required!';
const FILE_EXTENSION_FOR_JAVA: string = 'For Java language you must specify file with .jar extension!';
const FILE_EXTENSION_FOR_CSHARP: string = 'For C# language you must specify file with .zip extension!';

export {
  ALL_FILEDS_REQUIRED,
  INVALID_EMAIL_FORMAT,
  EMAIL_REQUIRED,
  EMAIL_AND_PASSWORD_REQUIRED,
  PASSWORD_DONT_MATCH,
  TERMS_AND_CONDITIONS_REQUIRED,
  PASSWORD_LOWERCASE_REQUIRED,
  PASSWORD_UPPERCASE_REQUIRED,
  PASSWORD_SPECIAL_CHARACTER_REQUIRED,
  PASSWORD_NUMBER_REQUIRED,
  PASSWORD_LENGTH_REQUIRED,
  FIELD_IS_REQUIRED,
  FILES_FOR_DOWNLOAD_REQUIRED,
  SPACE_EXCEEDED,
  FILES_ARE_NOT_SPECIFIED,
  FUNCTION_NAME_REQUIREMENTS,
  JAR_FILE_IS_REQUIRED,
  FILES_FOR_DELETE_REQUIRED,
  NO_PERMISSION_WERE_GIVEN,
  MAXIMUM_FILES_NUMBER,
  LANGUAGE_IS_REQUIRED,
  FILE_EXTENSION_FOR_JAVA,
  FILE_EXTENSION_FOR_CSHARP,
  MAXIMUM_DAYS_REQUIRED,
  LANGUAGE_IS_NOT_SELECTED
};
