export const SORT_PROJECT_VALUES: object = {
  'id,desc': 'newest to oldest',
  id: 'oldest to newest',
  projectName: 'name ascending',
  'projectName,desc': 'name descending',
  'projectSize,desc': 'size decreasing',
  projectSize: 'size increasing'
};

export const SORT_SHARED_PROJECT_VALUES: object = {
  'project.id,desc': 'newest to oldest',
  'project.id': 'oldest to newest',
  'project.projectName': 'name ascending',
  'project.projectName,desc': 'name descending',
  'project.projectSize,desc': 'size decreasing',
  'project.projectSize': 'size increasing'
};

export const SORT_FUNCTION_VALUES: object = {
  'functionId,desc': 'newest to oldest',
  functionId: 'oldest to newest',
  functionName: 'name ascending',
  'functionName,desc': 'name descending'
};

export const PROJECT_PER_PAGE_VALUES: object = {
  12: 12,
  24: 24,
  48: 48
};

export const PERMISSION_VALUES: object = {
  WRITE: 'Write',
  READ_ONLY: 'Read',
  DELETE: 'Delete'
};
