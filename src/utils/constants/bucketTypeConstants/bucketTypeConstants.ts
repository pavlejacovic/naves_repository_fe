const bucketOption = (value: string): string => {
  switch (value) {
    case 'BUCKET_UPLOAD_FILE':
      return 'UPLOAD';
    case 'BUCKET_DOWNLOAD_FILE':
      return 'DOWNLOAD';
    case 'BUCKET_DELETE_FILE':
      return 'DELETE';
    default:
      return '';
  }
};

export default bucketOption;
