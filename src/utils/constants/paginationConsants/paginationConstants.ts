const STARTING_PAGE: number = 1;
const MAX_PAGES: number = 5;
const STARTING_PAGE_ROUTE: string = `projects?page=${STARTING_PAGE}`;
const PROJECTS_NAVIGATION_ROUTE: string = 'projects?page=';
const PROJECTS_BASE_ROUTE: string = 'projects';
const FUNCTIONS_NAVIGATION_ROUTE: string = 'naves-functions?page=';
const FUNCTIONS_BASE_ROUTE: string = 'naves-functions';

export {
  STARTING_PAGE,
  MAX_PAGES,
  STARTING_PAGE_ROUTE,
  PROJECTS_NAVIGATION_ROUTE,
  PROJECTS_BASE_ROUTE,
  FUNCTIONS_NAVIGATION_ROUTE,
  FUNCTIONS_BASE_ROUTE
};
