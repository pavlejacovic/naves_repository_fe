import isValidEmail from './inputValidationsForEmail';

describe('isValidEmail', () => {
  it('should validate negatively empty email ', () => {
    expect(isValidEmail('')).toBe(false);
  });

  it('should validate negatively single character email', () => {
    expect(isValidEmail('a')).toBe(false);
  });

  it('should validate positively regular email', () => {
    expect(isValidEmail('test@test.com')).toBe(true);
  });

  it('should validate regular positively email with numbers in it', () => {
    expect(isValidEmail('test555@test.com')).toBe(true);
  });

  it('should validate positively regular email with special characters in it', () => {
    expect(isValidEmail('test!!!@test.com')).toBe(true);
  });

  it('should validate positively regular email with special characters and numbers in it', () => {
    expect(isValidEmail('test!!!555@test.com')).toBe(true);
  });

  it('should negatively validate email without @', () => {
    expect(isValidEmail('testtest.com')).toBe(false);
  });

  it('should negatively validate email with doots before @', () => {
    expect(isValidEmail('test..@test.com')).toBe(false);
  });

  it('should negatively validate email with two @', () => {
    expect(isValidEmail('@test@gmail.com')).toBe(false);
  });

  it('should negatively validate email with two @, one next to other', () => {
    expect(isValidEmail('test@@gmail.com')).toBe(false);
  });

  it('should negatively validate email with empty space', () => {
    expect(isValidEmail('test test@gmail.com')).toBe(false);
  });

  it('should negatively validate email with two doots after @', () => {
    expect(isValidEmail('test@gmail..com')).toBe(false);
  });

  it('should negatively validate email with doot at the end', () => {
    expect(isValidEmail('test@gmail.com.')).toBe(false);
  });

  it('should negatively validate email with special characters or numbers after @', () => {
    expect(isValidEmail('test@aaa!3.s!2')).toBe(false);
  });

  it('should negatively validate email with special characters after @', () => {
    expect(isValidEmail('test@aaa!.com')).toBe(false);
  });

  it('should positively validate email with number after @', () => {
    expect(isValidEmail('test@aa8.com')).toBe(true);
  });

  it('should validate email without letters before @', () => {
    expect(isValidEmail('!!!!!!@test.com')).toBe(true);
  });

  it('should positively validate email without letters before @', () => {
    expect(isValidEmail('555!!!@test.com')).toBe(true);
  });

  it('should negatively validate email without anything after @', () => {
    expect(isValidEmail('test@')).toBe(false);
  });

  it('should positively validate email with more than three letters in domain', () => {
    expect(isValidEmail('test@test.test')).toBe(true);
  });

  it('should positively validate email with one letter in domain name', () => {
    expect(isValidEmail('test@t.com')).toBe(true);
  });

  it('should positively validate email with just numbers in domain name', () => {
    expect(isValidEmail('test@555.com')).toBe(true);
  });

  it('should negatively validate email with just special characters in domain name', () => {
    expect(isValidEmail('test@!!!!.com')).toBe(false);
  });

  it('should negatively validate email with just special characters in domain', () => {
    expect(isValidEmail('test@test.!!!')).toBe(false);
  });

  it('should positively validate email with just special characters in domain', () => {
    expect(isValidEmail('test@test.555')).toBe(true);
  });

  it('should positively validate email with uppercase letters', () => {
    expect(isValidEmail('TEST@test.com')).toBe(true);
  });

  it('should positively validate email with uppercase letters in domain name', () => {
    expect(isValidEmail('test@TEST.com')).toBe(true);
  });

  it('should positively validate email with uppercase letters in domain', () => {
    expect(isValidEmail('test@test.COM')).toBe(true);
  });

  it('should positively validate email with all uppercase letters', () => {
    expect(isValidEmail('TEST@TEST.COM')).toBe(true);
  });

  it('should positively validate email with combination uppercase and lowercase letters', () => {
    expect(isValidEmail('TEST@TEST2.COM')).toBe(true);
  });
});
