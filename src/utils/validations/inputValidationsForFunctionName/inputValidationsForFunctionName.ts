const FUNCTION_NAME_REGEX: RegExp = new RegExp('^[a-z0-9-]+$');

const isFunctionNameValid = (name: string): boolean => FUNCTION_NAME_REGEX.test(name);

export default isFunctionNameValid;
