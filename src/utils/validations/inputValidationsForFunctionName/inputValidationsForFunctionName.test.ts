import isFunctionNameValid from './inputValidationsForFunctionName';

describe('isFunctionNameValid', () => {
  it('should validate negatively name with uppercase letters', () => {
    expect(isFunctionNameValid('THISISATESTNAME')).toBe(false);
  });

  it('should validate negatively name with spaces in it', () => {
    expect(isFunctionNameValid('THIS IS A TEST NAME')).toBe(false);
  });

  it('should validate negatively name with uppercase and lowercase letters', () => {
    expect(isFunctionNameValid('THISisAtestwNAME')).toBe(false);
  });

  it('should validate negatively name with special characters', () => {
    expect(isFunctionNameValid('thisisatestname!')).toBe(false);
  });

  it('should validate positively name with lowercase letters', () => {
    expect(isFunctionNameValid('thisisatestname')).toBe(true);
  });

  it('should validate positively name with lowercase letters and numbers', () => {
    expect(isFunctionNameValid('thisisatestname123')).toBe(true);
  });

  it('should validate positively name with lowercase letters, numbers and -', () => {
    expect(isFunctionNameValid('this-is-a-test-name-123')).toBe(true);
  });

  it('should validate positively name with numbers and -', () => {
    expect(isFunctionNameValid('1-2-3-4-5')).toBe(true);
  });

  it('should validate positively name with just -', () => {
    expect(isFunctionNameValid('-')).toBe(true);
  });
});
