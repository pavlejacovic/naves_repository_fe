import {
  PASSWORD_LENGTH_REQUIRED,
  PASSWORD_NUMBER_REQUIRED,
  PASSWORD_LOWERCASE_REQUIRED,
  PASSWORD_UPPERCASE_REQUIRED,
  PASSWORD_SPECIAL_CHARACTER_REQUIRED
} from '../../constants/validationMessages/validation-messages';

const isInvalidPassword = (password: string): string => {
  if (password.length < 8) {
    return PASSWORD_LENGTH_REQUIRED;
  }
  if (!(/(?=.*[0-9])/.test(password))) {
    return PASSWORD_NUMBER_REQUIRED;
  }
  if (!(/(?=.*[a-z])/.test(password))) {
    return PASSWORD_LOWERCASE_REQUIRED;
  }
  if (!(/(?=.*[A-Z])/.test(password))) {
    return PASSWORD_UPPERCASE_REQUIRED;
  }
  if (!(/(?=.*[!@#$%^&*])/.test(password))) {
    return PASSWORD_SPECIAL_CHARACTER_REQUIRED;
  }
  return '';
};

export default isInvalidPassword;
