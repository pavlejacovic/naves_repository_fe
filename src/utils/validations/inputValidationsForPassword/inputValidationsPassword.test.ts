import isInvalidPassword from './inputValidationsPassword';
import {
  PASSWORD_LENGTH_REQUIRED,
  PASSWORD_LOWERCASE_REQUIRED,
  PASSWORD_NUMBER_REQUIRED,
  PASSWORD_SPECIAL_CHARACTER_REQUIRED,
  PASSWORD_UPPERCASE_REQUIRED
} from '../../constants/validationMessages/validation-messages';

describe('isInvalidPassword', () => {
  it('should contain at least 8 characters', () => {
    expect(isInvalidPassword('')).toBe(PASSWORD_LENGTH_REQUIRED);
  });

  it('should contain at least 8 characters', () => {
    expect(isInvalidPassword('A!123aa')).toBe(PASSWORD_LENGTH_REQUIRED);
  });

  it('should contain at least one number', () => {
    expect(isInvalidPassword('Aa!aaaaaaaaa')).toBe(PASSWORD_NUMBER_REQUIRED);
  });

  it('should contain at least one uppercase letter', () => {
    expect(isInvalidPassword('a!aaaa123')).toBe(PASSWORD_UPPERCASE_REQUIRED);
  });

  it('should contain at least one lowercase letter', () => {
    expect(isInvalidPassword('A!1223AAAAA')).toBe(PASSWORD_LOWERCASE_REQUIRED);
  });

  it('should contain at least one of the characters !@#$%^&*', () => {
    expect(isInvalidPassword('Aa12345Aa')).toBe(PASSWORD_SPECIAL_CHARACTER_REQUIRED);
  });

  it('should pass as correct, without warning messages', () => {
    expect(isInvalidPassword('Aa12345!')).toBe('');
  });
});
