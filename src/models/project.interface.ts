import { File } from './file.interface';
import { Folder } from './folder.interface';
import { UserInfo } from './user.interface';

export interface Project {
  id: number;
  owner?: UserInfo;
  projectName: string,
  numberOfFiles?: number;
  projectSize?: number;
  directoryName?: string;
  project?: any
  writePermission?: boolean
}

export interface ProjectContent {
  files: File[],
  folders: Folder[],
  project: Project,
  writePermission?: boolean
}
export interface FilePath {
  path: string;
}

export interface ProjectUsers {
  id: number,
  project: Project,
  user: { email: string },
  writePermission: boolean
}

export interface ProjectInfo {
  project: Project;
  numberOfShares: number;
}
