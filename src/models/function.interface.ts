import { Project } from './project.interface';
import { UserInfo } from './user.interface';

export interface NavesFunctionInfo {
  functionId?: number,
  functionLink?: string | undefined,
  functionName: string,
  language?: string,
  publicAccess: boolean,
  archiveFile?: File,
  trigger: string,
  owner?: UserInfo,
  projectIds?: (number | undefined)[]
  projects?: Project[]
}
