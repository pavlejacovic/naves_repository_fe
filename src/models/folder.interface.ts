export interface Folder {
  folderPath: string;
  folderSize: number;
  folderName: string;
}
