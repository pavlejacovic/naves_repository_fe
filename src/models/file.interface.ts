import { Project } from './project.interface';

export interface File {
  filePath: string;
  fileSize: number;
  parentProject: Project;
  uploadedBy: string;
  createdDate: string;
  projectName?: string;
  fileName: string;
}

export interface FilesForUpload {
  files: object[];
  path: string;
}

export interface FileData {
  lastModified?: number;
  lastModifiedDate?: string;
  name: string;
  size: number;
  type: string;
}
