export interface OptionValueString {
  value: string;
  label: string;
}

export interface OptionValueBoolean {
  value: boolean;
  label: string;
}

export interface OptionValueNumber {
  value: number | undefined;
  label: string;
}
