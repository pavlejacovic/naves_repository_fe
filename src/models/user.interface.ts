export interface UserInfo {
  email?: string;
  password?: string;
  provider?: string,
  name?: string;
  surname?: string;
  passwordResetToken?:string | null;
  subscribed?: boolean;
  terms?: boolean;
}

export interface ReceivedUsersInfo {
  email?: string;
  name: string;
  surname: string;
  subscribed: boolean;
}

export interface OldAndNewPassword {
  oldPassword: string;
  newPassword: string;
}

export interface Token {
  token: string | null;
}
