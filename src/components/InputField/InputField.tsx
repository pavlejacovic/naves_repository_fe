import React, { ChangeEvent } from 'react';
import styles from './InputField.module.scss';

interface InputFieldProps {
  id: string;
  name: string;
  customStyles?: string;
  value?: string | number;
  type?: string;
  min?: number;
  max?: number;
  placeholder?: string;
  maxlength?: number | undefined;
  inputWarning?: boolean | string;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  dataTestId?: string;
}

const InputField: React.FC<InputFieldProps> = ({
  id = 'text', name = 'text', type = 'text', placeholder = 'Text', value = '', customStyles, inputWarning, maxlength = 255, onChange, dataTestId, min, max
}: InputFieldProps): JSX.Element => (
  <input
    id={id}
    name={name}
    className={`${styles['Input']} ${inputWarning && styles['Input_warning']} ${customStyles}`}
    type={type}
    min={min}
    max={max}
    placeholder={placeholder}
    maxLength={maxlength}
    value={value}
    onChange={onChange}
    data-test-id={dataTestId}
  />
);

export default InputField;
