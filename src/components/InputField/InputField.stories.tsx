import React from 'react';
import InputField from './InputField';

export default {
  title: 'InputField',
  component: InputField
};

export const Email = () => <InputField placeholder="Enter e-mail" id="email" type="text" name="email" />;
export const Password = () => <InputField placeholder="Enter epassword" id="password" type="password" name="password" />;
export const WrongInputValue = () => <InputField placeholder="Enter some text" id="warning" type="text" name="warning" inputWarning />;
