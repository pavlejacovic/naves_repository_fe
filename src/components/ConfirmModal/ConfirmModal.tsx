import React, { MouseEvent } from 'react';
import { Col, Container, Row } from 'react-grid-system';
import { Button } from '../Button/Button';
import Modal from '../Modal/Modal';
import styles from './ConfirmModal.module.scss';

export interface ConfirmModalProps {
  setModalIsVisible: React.Dispatch<React.SetStateAction<boolean>>;
  modalIsVisible: boolean;
  onConfirm: (event: MouseEvent<HTMLButtonElement>) => void;
  onClose: (event: MouseEvent<HTMLButtonElement>) => void;
  text: string;
}

const ConfirmModal: React.FC<ConfirmModalProps> = (
  {
    modalIsVisible,
    setModalIsVisible,
    onConfirm,
    onClose,
    text
  }
) => (
  <Modal modalIsVisible={modalIsVisible} setModalIsVisible={setModalIsVisible}>
    <Container fluid>
      <Row align="center" justify="center" className={styles['ConfirmModal-Row-Text']}><p>{text}</p></Row>
      <Row align="center" justify="center" className={styles['ConfirmModal-Row']}>
        <Col md="content"><Button type="button" onClick={onConfirm} dataTestId="confirmBtn">Yes</Button></Col>
        <Col md="content"><Button type="button" onClick={onClose} dataTestId="declineBtn">No</Button></Col>
      </Row>
    </Container>
  </Modal>
);

export default ConfirmModal;
