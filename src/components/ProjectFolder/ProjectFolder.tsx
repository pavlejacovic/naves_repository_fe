import React, {
  MouseEvent,
  useContext,
  useState
} from 'react';
import { Col, Container, Row } from 'react-grid-system';
import { MdMoreVert } from 'react-icons/md';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Link } from 'react-router-dom';
import styles from './ProjectFolder.module.scss';
import folderLogo from '../../assets/img/folder_icon.svg';
import DropdownButton from '../DropdownButton/DropdownButton';
import SaveModal from '../SaveModal/SaveModal';
import { ProjectsContext } from '../../Contexts/ProjectsContext';
import { Button } from '../Button/Button';
import ConfirmModal from '../ConfirmModal/ConfirmModal';
import BoxService from '../../services/box.service';
import FileSaverService from '../../services/file-saver.service';
import Modal from '../Modal/Modal';
import InputField from '../InputField/InputField';
import Select from '../Select/Select';
import AlertMessage, { AlertMessageStyle } from '../AlertMessage/AlertMessage';
import { ProjectUsers } from '../../models/project.interface';
import { FIELD_IS_REQUIRED, NO_PERMISSION_WERE_GIVEN } from '../../utils/constants/validationMessages/validation-messages';
import Permission from '../Permission/Permission';

interface ProjectFolderProps {
  linkTo: string;
  projectId: number;
  projectName: string;
  projectOwner?: string;
  writePermission?: boolean,
  owner?: boolean
}

const ProjectFolder: React.FC<ProjectFolderProps> = (
  {
    linkTo,
    projectId,
    projectName,
    projectOwner,
    writePermission,
    owner
  }
): JSX.Element => {
  const [renameProjectModalIsVisible, setRenameProjectModalIsVisible] = useState<boolean>(false);
  const [confirmModalIsVisible, setConfirmModalIsVisible] = useState<boolean>(false);
  const [shareModalIsVisible, setShareModalIsVisible] = useState<boolean>(false);
  const [userEmail, setUserEmail] = useState<string>('');
  const [sharePermission, setSharePermission] = useState<string>('WRITE');
  const [emptyFieldMessage, setEmptyFieldMessage] = useState<string>('');
  const {
    fetchProjectsOnPage,
    fetchAllMyProjects,
    fetchProjectUsers,
    fetchAllSharedProjects,
    fetchSharedWithMeProjectsOnPage,
    projectUsers
  } = useContext(ProjectsContext);

  const handleCloseConfirmModalClick = () => {
    setConfirmModalIsVisible(false);
  };

  const handleRenameProject = (
    newProjectName: string,
    setInputText: React.Dispatch<React.SetStateAction<string>>
  ): void => {
    BoxService.renameProject(projectId, newProjectName).then(
      (response) => {
        setRenameProjectModalIsVisible(false);
        setInputText('');
        fetchProjectsOnPage();
        fetchAllSharedProjects();
        fetchSharedWithMeProjectsOnPage();
        toast.success(response.data);
      }
    );
  };

  const openConfirmDeleteModal = (event: MouseEvent) => {
    event.preventDefault();
    setConfirmModalIsVisible(true);
  };

  const downloadProjectZIP = (e: MouseEvent) => {
    e.preventDefault();
    BoxService.downloadAsZip(projectId, [{ path: '/' }]).then(
      (response) => {
        FileSaverService.downloadZip(response.data, projectName);
      }
    );
  };

  const handleDeleteProjectClick = (event: MouseEvent) => {
    event.preventDefault();
    BoxService.deleteProject(projectId).then(
      (response) => {
        if (response.status === 200) {
          toast.success(response.data);
          setConfirmModalIsVisible(false);
          fetchProjectsOnPage();
          fetchAllMyProjects();
          fetchAllSharedProjects();
          fetchSharedWithMeProjectsOnPage();
        }
      }
    ).catch((error) => {
      toast.error(error.response.data);
      setConfirmModalIsVisible(false);
      fetchProjectsOnPage();
    });
  };

  const openRenameProjectModal = (e: MouseEvent): void => {
    e.preventDefault();
    setRenameProjectModalIsVisible(true);
  };

  const handleOpenShareModalClick = (e: MouseEvent): void => {
    e.preventDefault();
    setShareModalIsVisible(true);
    setEmptyFieldMessage('');
    fetchProjectUsers(projectId);
  };

  const handleUserEmailChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setUserEmail(event.target.value);
  };

  const handleSetPermissionChange = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    setSharePermission(event.target.value);
  };

  const handleAddPermissionClick = (): void => {
    const data: { email: string, permission: string }[] = [{
      email: userEmail.trim(),
      permission: sharePermission
    }];
    setEmptyFieldMessage('');
    if (!userEmail.trim()) {
      setEmptyFieldMessage(FIELD_IS_REQUIRED);
      return;
    }
    BoxService.shareProject(projectId, data)
      .then((response) => {
        fetchProjectUsers(projectId);
        response.data.map((info: { successful: boolean, message: string }) => toast.info(info.message));
      })
      .catch((error) => error.response.data
        .map((info: { successful: boolean, message: string }) => toast.error(info.message)));
  };

  return (
    <>
      <Link data-test-id={projectName} className={styles['ProjectFolder']} to={linkTo} title={projectName}>
        <div className={styles['ProjectFolder-MoreActions']}>
          {!owner && <p className={styles['ProjectFolder-permission']}>{writePermission ? 'write' : 'read'}</p>}
          <DropdownButton text={<MdMoreVert className={styles['ProjectFolder-ButtonIcon']} />} customStyles={styles['ProjectFolder-Button']}>
            <ul>
              <li>
                <Button dataTestId="downloadBtn" type="button" customStyles={styles['ProjectFolder-Action']} onClick={downloadProjectZIP}>Download</Button>
              </li>
              {writePermission
              && (
              <>
                <li>
                  <Button dataTestId="renameBtn" type="button" customStyles={styles['ProjectFolder-Action']} onClick={openRenameProjectModal}>Rename</Button>
                </li>
                <li>
                  <Button dataTestId="openConfirmDeleteModalBtn" type="button" customStyles={styles['ProjectFolder-Action']} onClick={openConfirmDeleteModal}>Delete</Button>
                </li>
                { owner
                && (
                <li>
                  <Button dataTestId="openShareModalBtn" type="button" customStyles={styles['ProjectFolder-Action']} onClick={handleOpenShareModalClick}>Share</Button>
                </li>
                )}
              </>
              )}
            </ul>
          </DropdownButton>
        </div>
        <img alt="filesLogo" src={folderLogo} className={styles['ProjectFolder-Img']} />
        <div className={styles['ProjectFolder-Info']}>
          <p className={styles['ProjectFolder-Info_projectName']}>
            {`Project: ${projectName}`}
          </p>
          {projectOwner && (
            <p className={styles['ProjectFolder-Info_projectOwner']}>
              {`${projectOwner}`}
            </p>
          )}
        </div>
      </Link>
      <ConfirmModal
        text="Are you sure you want to delete this project?"
        modalIsVisible={confirmModalIsVisible}
        setModalIsVisible={setConfirmModalIsVisible}
        onConfirm={handleDeleteProjectClick}
        onClose={handleCloseConfirmModalClick}
      />
      <SaveModal
        title="Rename project"
        inputPlaceholder="New name"
        modalIsVisible={renameProjectModalIsVisible}
        setModalIsVisible={setRenameProjectModalIsVisible}
        onSave={handleRenameProject}
      >
        <Button type="submit" dataTestId="renameProjectBtn">Rename project</Button>
      </SaveModal>
      <Modal
        setModalIsVisible={setShareModalIsVisible}
        modalIsVisible={shareModalIsVisible}
      >
        <Container>
          <Row align="center" justify="center" className={styles['ProjectFolder-ShareModal-AddPermision-Row']}>
            <Col md={7}>
              <InputField
                id="searchUser"
                name="searchUser"
                type="text"
                placeholder="Enter the user's email"
                dataTestId="enterUserEmailTest"
                value={userEmail}
                onChange={handleUserEmailChange}
              />
            </Col>
            <Col md={3}>
              <Select id="select permision" customStyle={styles['ProjectFolder-ShareModal-Select']} onChange={handleSetPermissionChange}>
                <option value="WRITE">Write</option>
                <option value="READ_ONLY">Read</option>
              </Select>
            </Col>
            <Col md={2}>
              <Button dataTestId="userEmailTestBtn" type="button" customStyles={styles['ProjectFolder-ShareModal-AddBtn']} onClick={handleAddPermissionClick}>Add</Button>
            </Col>
          </Row>
          {emptyFieldMessage && (
            <Row justify="center" align="center"><AlertMessage messageStyle={AlertMessageStyle.WARNING}>{emptyFieldMessage}</AlertMessage></Row>
          )}
        </Container>
        <Container fluid className={styles['ProjectFolder-ShareModal-UsersContainer']}>
          {projectUsers.length
            ? (projectUsers.map((project: ProjectUsers) => (
              <Permission
                key={project.user.email}
                project={project}
                projectId={projectId}
                setEmptyFieldMessage={setEmptyFieldMessage}
              />
            ))
            )
            : (
              <Row align="center" justify="center">
                <Col><AlertMessage customStyles={styles['ProjectFolder-ShareModal-AlertMessage']}>{NO_PERMISSION_WERE_GIVEN}</AlertMessage></Col>
              </Row>
            )}
        </Container>
      </Modal>
    </>
  );
};

export default ProjectFolder;
