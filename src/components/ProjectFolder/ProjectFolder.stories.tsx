import React from 'react';
import { ComponentStory } from '@storybook/react';
import { Router } from 'react-router-dom';
import history from '../../utils/constants/history/history';
import ProjectFolder from './ProjectFolder';

export default {
  title: 'ProjectFolder',
  component: ProjectFolder
};
const Template: ComponentStory<typeof ProjectFolder> = ({
  linkTo,
  projectId,
  projectName,
  projectOwner
}) => (
  <Router history={history}>
    <ProjectFolder linkTo={linkTo} projectId={projectId} projectName={projectName} projectOwner={projectOwner} />
  </Router>
);

export const Project = Template.bind({});
Project.args = {
  linkTo: '/project',
  projectName: 'Project',
  projectOwner: 'Milos'
};
export const MySampleProject = () => <Router history={history}><ProjectFolder linkTo="/project1" projectId={1} projectName="Naves"> </ProjectFolder></Router>;

export const SharedWithMeSampleProject = () => <Router history={history}><ProjectFolder linkTo="/project2" projectId={1} projectName="Naves" projectOwner="Milos"> </ProjectFolder></Router>;
