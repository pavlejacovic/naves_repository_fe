import React, { ReactNode, useRef } from 'react';
import { Col, Container, Row } from 'react-grid-system';
import { MdClose } from 'react-icons/md';
import useOutsideClick from '../../utils/hooks/useOutsideClick';
import { Button, ButtonStyle } from '../Button/Button';
import styles from './Modal.module.scss';

export interface ModalProps {
  setModalIsVisible: React.Dispatch<React.SetStateAction<boolean>>;
  modalIsVisible: boolean;
  actions?: ReactNode;
  customStyles?: string;
}

const Modal: React.FC<ModalProps> = ({
  setModalIsVisible,
  modalIsVisible,
  actions,
  children,
  customStyles
}) => {
  const ref = useRef(null);

  const closeModal = () => {
    setModalIsVisible(false);
  };

  useOutsideClick(ref, () => {
    if (modalIsVisible) {
      setModalIsVisible(false);
    }
  });

  if (!modalIsVisible) {
    return null;
  }

  return (
    <div className={styles['Modal']}>
      <Container fluid>
        <div ref={ref}>
          <Row align="center" justify="end" className={styles['Modal-Close']}>
            <Col md="content">
              <Row>
                <Col md="content" sm={12}>
                  <Button type="button" buttonStyle={ButtonStyle.SECONDARY} customStyles={styles['Modal-Button']} onClick={closeModal}>
                    <MdClose className={styles['Modal-ButtonIcon']} />
                  </Button>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row align="center" justify="center" className={`${styles['Modal-Content']} ${customStyles}`}>
            <Col md="content">
              {children}
            </Col>
          </Row>
          <Row align="center" justify="end" className={styles['Modal-Actions']}>
            <Col md="content">
              {actions && (
                <Row>
                  {actions}
                </Row>
              )}
            </Col>
          </Row>
        </div>
      </Container>
    </div>
  );
};

export default Modal;
