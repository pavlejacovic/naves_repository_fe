import React, { useState } from 'react';
import { Col } from 'react-grid-system';
import { Button } from '../Button/Button';
import Modal from './Modal';
import styles from './Modal.module.scss';

export const termsAndCondition = () => {
  const [termsModalIsVisible, setTermsModalIsVisible] = useState<boolean>(false);
  const readTermsAndConditions = (): void => {
    setTermsModalIsVisible(true);
  };
  return (
    <div>
      <Button type="button" onClick={readTermsAndConditions}>Read terms and conditions</Button>
      <Modal modalIsVisible={termsModalIsVisible} setModalIsVisible={setTermsModalIsVisible}>
        Custom terms and conditions
      </Modal>
    </div>
  );
};

export const modalWithActions = () => {
  const [modalIsVisible, setModalIsVisible] = useState<boolean>(false);
  const openModal = (): void => {
    setModalIsVisible(true);
  };
  return (
    <div>
      <Button type="button" onClick={openModal}>Open modal</Button>
      <Modal
        modalIsVisible={modalIsVisible}
        setModalIsVisible={setModalIsVisible}
        actions={
          (
            <>
              <Col md="content" sm={12}><Button type="button" customStyles={styles['Modal-Button']}>Save</Button></Col>
              <Col md="content" sm={12}><Button type="button" customStyles={styles['Modal-Button']}>Delete</Button></Col>
            </>
          )
        }
      >
        Custom modal with custom actions.
      </Modal>
    </div>
  );
};

export default {
  title: 'Modal',
  component: Modal
};
