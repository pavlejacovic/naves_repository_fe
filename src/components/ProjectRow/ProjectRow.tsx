import React, { useEffect, useState } from 'react';
import { Col, Row } from 'react-grid-system';
import { ProjectInfo, ProjectUsers } from '../../models/project.interface';
import BoxService from '../../services/box.service';
import bytesToMegaBytes from '../../utils/functions/unitsConversions/unitsConversions';
import { Button } from '../Button/Button';
import Modal from '../Modal/Modal';
import styles from './ProjectRow.module.scss';

interface ProjectRowProps {
  dataTestId: string,
  projectInfo: ProjectInfo,
}

const ProjectRow: React.FC<ProjectRowProps> = ({ dataTestId, projectInfo }): JSX.Element => {
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
  const [sharedWithInfo, setSharedWithInfo] = useState<ProjectUsers[]>([]);

  useEffect(() => {
    BoxService.getSharedWithProjectForAdmin(projectInfo.project.id).then((response) => {
      setSharedWithInfo(response.data);
    });
  }, []);

  const handledModalIsVisible = () => {
    setIsModalVisible(true);
  };

  return (
    <>
      <li data-test-id={dataTestId} className={styles['ProjectRow']}>
        <Row justify="between" align="center">
          <Col lg={3} className={styles['Project-Text']}>{projectInfo.project?.projectName}</Col>
          <Col lg={3} className={styles['Project-Text']}>{projectInfo.project?.owner?.email}</Col>
          <Col lg={3} className={styles['Project-Text']}>
            {projectInfo.project?.projectSize ? bytesToMegaBytes(projectInfo.project?.projectSize) : '0 MB'}
          </Col>
          <Col lg={3} className={styles['ProjectRow-Text']}>
            {!projectInfo.numberOfShares ? 'No users' : (
              <Button type="button" onClick={handledModalIsVisible}>
                View (
                {projectInfo.numberOfShares}
                )
              </Button>
            )}
          </Col>
        </Row>
      </li>
      <Modal
        modalIsVisible={isModalVisible}
        setModalIsVisible={setIsModalVisible}
      >
        <div className={styles['ProjectRow-SharedWithModal']}>
          {sharedWithInfo.map((userInfo) => (
            <Row justify="start" align="center" key={userInfo.id} className={styles['ProjectRow-SharedWithUser']}>
              <Col key={userInfo.id}>
                {userInfo.user.email}
                (
                {userInfo.writePermission ? 'write' : 'read'}
                )
              </Col>
            </Row>
          ))}
        </div>
      </Modal>
    </>
  );
};

export default ProjectRow;
