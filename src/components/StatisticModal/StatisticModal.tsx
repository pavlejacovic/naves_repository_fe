import React, { ChangeEvent, useEffect, useState } from 'react';
import { Col, Container, Row } from 'react-grid-system';
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  Tooltip,
  CartesianGrid
} from 'recharts';
import NavesFunctionsService from '../../services/naves-functions.service';
import { MAXIMUM_DAYS_REQUIRED } from '../../utils/constants/validationMessages/validation-messages';
import AlertMessage, { AlertMessageStyle } from '../AlertMessage/AlertMessage';
import { Button } from '../Button/Button';
import InputField from '../InputField/InputField';
import Modal from '../Modal/Modal';
import styles from './StatisticModal.module.scss';

const MAX_NUMBER_OD_DAYS_FOR_STATISTIC: number = 365;

export interface StatisticModalProps {
  functionId: number | undefined;
  title: string;
  modalIsVisible: boolean;
  setModalIsVisible: React.Dispatch<React.SetStateAction<boolean>>;
  customStyle?: string;
}

const StatisticModal: React.FC<StatisticModalProps> = (
  {
    functionId,
    title,
    modalIsVisible,
    setModalIsVisible,
    customStyle
  }
): JSX.Element => {
  const [statisticForPreviousDays, setStatisticForPreviousDay] = useState<number>(0);
  const [statistic, setStatistic] = useState<Array<object> | null>(null);

  const handleDaysChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setStatistic(null);
    setStatisticForPreviousDay(parseInt(event.target.value, 10));
  };

  const handleGetStatisticClick = (): void => {
    if (functionId && statisticForPreviousDays <= MAX_NUMBER_OD_DAYS_FOR_STATISTIC) {
      NavesFunctionsService.getStatisticForPreviousDays(functionId, statisticForPreviousDays)
        .then((response) => setStatistic(response.data))
        .catch(() => setModalIsVisible(false));
    }
  };

  useEffect(() => {
    setStatisticForPreviousDay(0);
    setStatistic(null);
  }, []);

  return (
    <Modal modalIsVisible={modalIsVisible} setModalIsVisible={setModalIsVisible} customStyles={customStyle}>
      <Container fluid>
        <Row justify="center" align="center">
          <h3>{title}</h3>
        </Row>
        <Row justify="center" align="center">
          <Col lg={7}>
            <label htmlFor="days">Show statistic for previous days</label>
          </Col>
          <Col lg={3}>
            <InputField id="days" name="days" type="number" placeholder="" value={statisticForPreviousDays} min={1} max={999} onChange={handleDaysChange} customStyles={styles['StatisticModal-InputField']} />
          </Col>
        </Row>
        {statisticForPreviousDays > MAX_NUMBER_OD_DAYS_FOR_STATISTIC && (
          <Row justify="center" align="center">
            <Col lg={10}>
              <AlertMessage messageStyle={AlertMessageStyle.WARNING} customStyles={styles['StatisticModal-AlertMessage']}>{MAXIMUM_DAYS_REQUIRED}</AlertMessage>
            </Col>
          </Row>
        )}
        {statistic && statistic?.length !== 0 && (
          <Row justify="start" align="center">
            <Col lg={10}>
              <LineChart
                width={400}
                height={350}
                data={statistic}
              >
                <Line type="monotone" dataKey="numberOfExecutions" stroke="#8884d8" />
                <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
                <Tooltip />
                <XAxis dataKey="date" xlinkTitle="Date" />
                <YAxis dataKey="numberOfExecutions" />
              </LineChart>
            </Col>
          </Row>
        )}
        {statistic && statistic?.length === 0 && (
          <Row justify="center" align="center">
            <Col>
              <AlertMessage customStyles={styles['StatisticModal-AlertMessage']}>{`This function has not been executed for the last ${statisticForPreviousDays} days.`}</AlertMessage>
            </Col>
          </Row>
        )}
        <Row justify="center" align="center">
          <Col lg={12}>
            <Button type="submit" dataTestId="get-statistic" onClick={handleGetStatisticClick} customStyles={styles['StatisticModal-Button']}>Get statistic</Button>
          </Col>
        </Row>
      </Container>
    </Modal>
  );
};

export default StatisticModal;
