import React, { useState, useEffect } from 'react';
import { Col, Container, Row } from 'react-grid-system';
import authenticationService from '../../services/authentication.service';
import boxService from '../../services/box.service';
import navesFunctionsService from '../../services/naves-functions.service';
import styles from './Statistics.module.scss';

interface StatisticsProps {
  customStyles?: string | undefined;
}

const Statistics: React.FC<StatisticsProps> = (customStyles): JSX.Element => {
  const [users, setUsers] = useState<number>(0);
  const [functions, setFunctions] = useState<number>(0);
  const [projects, setProjects] = useState<number>(0);

  const fetchAllUsers = (): void => {
    authenticationService.getNumberOfClients()
      .then((response) => setUsers(response.data));
  };

  const fetchAllFunctions = (): void => {
    navesFunctionsService.getNumberOfAllFunctions()
      .then((response) => setFunctions(response.data));
  };

  const fetchAllProjects = (): void => {
    boxService.getNumberOfAllProjects()
      .then((response) => setProjects(response.data));
  };

  useEffect(() => {
    fetchAllUsers();
    fetchAllFunctions();
    fetchAllProjects();
  }, []);

  return (
    <Container className={`${styles['Statistics']} ${customStyles}`}>
      <Row justify="around" align="center">
        <Col lg={4} className={styles['Statistics-Col']} title={users.toString()}>{`Users: ${users}`}</Col>
        <Col lg={4} className={styles['Statistics-Col']} title={functions.toString()}>{`Functions: ${functions}`}</Col>
        <Col lg={4} className={styles['Statistics-Col']} title={projects.toString()}>{`Projects: ${projects}`}</Col>
      </Row>
    </Container>
  );
};

export default Statistics;
