import React from 'react';
import { ComponentStory } from '@storybook/react';
import { Col, Row } from 'react-grid-system';
import Header from './Header';
import { Button, ButtonStyle } from '../Button/Button';

const Template: ComponentStory<typeof Header> = ({ title, children }) => (
  <Header title={title}>
    {children}
  </Header>
);

export const MyProjects = Template.bind({});
MyProjects.args = {
  title: 'My projects',
  children:
  <Row>
    <Col md="content" sm={6} xs={12}><Button type="button" dataTestId="test" buttonStyle={ButtonStyle.SECONDARY}>Create new project</Button></Col>
  </Row>
};

export const SharedWithMe = Template.bind({});
SharedWithMe.args = {
  title: 'Shared with me',
  children:
  <Row>
    <Col md="content" sm={6} xs={12}><Button dataTestId="test1" type="button" buttonStyle={ButtonStyle.SECONDARY}>Upload</Button></Col>
    <Col md="content" sm={6} xs={12}><Button dataTestId="test2" type="button" buttonStyle={ButtonStyle.SECONDARY}>Download</Button></Col>
    <Col md="content" sm={6} xs={12}><Button dataTestId="test3" type="button" buttonStyle={ButtonStyle.SECONDARY}>Delete</Button></Col>
  </Row>
};

export default {
  title: 'Header',
  component: Header
};
