import React, { ReactNode } from 'react';
import { Col, Container, Row } from 'react-grid-system';
import styles from './Header.module.scss';

export interface HeaderProps {
  title: ReactNode;
}

const Header: React.FC<HeaderProps> = ({ title, children }): JSX.Element => (
  <Container fluid className={styles['Header']}>
    <Row className={styles['Header-Row']} align="center">
      <Col className={styles['Header-Title']}>{title}</Col>
      <Col md="content" sm={12}>{children}</Col>
    </Row>
  </Container>
);

export default Header;
