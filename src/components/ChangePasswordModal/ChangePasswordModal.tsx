import React, { ChangeEvent, useEffect, useState } from 'react';
import { Col, Container, Row } from 'react-grid-system';
import { toast } from 'react-toastify';
import { OldAndNewPassword } from '../../models/user.interface';
import AuthenticationService from '../../services/authentication.service';
import { ALL_FILEDS_REQUIRED, PASSWORD_DONT_MATCH } from '../../utils/constants/validationMessages/validation-messages';
import isInvalidPassword from '../../utils/validations/inputValidationsForPassword/inputValidationsPassword';
import AlertMessage, { AlertMessageStyle } from '../AlertMessage/AlertMessage';
import { Button } from '../Button/Button';
import InputField from '../InputField/InputField';
import Modal from '../Modal/Modal';
import styles from './ChangePasswordModal.module.scss';

export interface ChangePasswordModalProps {
  setModalIsVisible: React.Dispatch<React.SetStateAction<boolean>>;
  modalIsVisible: boolean;
  text: string;
}

const ChangePasswordModal: React.FC<ChangePasswordModalProps> = (
  {
    modalIsVisible,
    setModalIsVisible,
    text
  }
) => {
  const [oldPassword, setOldPassword] = useState<string>('');
  const [newPassword, setNewPassword] = useState<string>('');
  const [confirmedPassword, setConfirmedPassword] = useState<string>('');
  const [validationMessage, setValidationMessage] = useState<string>('');
  const [errorMessage, setErrorMessage] = useState<string>('');

  const handleOldPasswordChange = (event: ChangeEvent<HTMLInputElement>): void => setOldPassword(event.target.value);
  const handleNewPasswordChange = (event: ChangeEvent<HTMLInputElement>): void => setNewPassword(event.target.value);
  const handleConfirmedPasswordChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setConfirmedPassword(event.target.value);
  };

  const resetErrorMessages = ():void => {
    setValidationMessage('');
    setErrorMessage('');
  };

  const changePassword = ():void => {
    const data: OldAndNewPassword = {
      oldPassword: oldPassword,
      newPassword: newPassword
    };
    AuthenticationService.changePassword(data)
      .then((response) => {
        toast.success(response.data);
      })
      .finally(() => setModalIsVisible(false));
  };

  const handleChangePasswordClick = ():void => {
    if (!oldPassword || !newPassword || !confirmedPassword) {
      setErrorMessage(ALL_FILEDS_REQUIRED);
      return;
    }
    if (isInvalidPassword(newPassword)) {
      resetErrorMessages();
      setValidationMessage(isInvalidPassword(newPassword));
      return;
    }
    if (newPassword !== confirmedPassword) {
      resetErrorMessages();
      setErrorMessage(PASSWORD_DONT_MATCH);
      return;
    }
    changePassword();
  };

  useEffect(() => {
    setOldPassword('');
    setNewPassword('');
    setConfirmedPassword('');
    resetErrorMessages();
  }, []);

  return (
    <Modal modalIsVisible={modalIsVisible} setModalIsVisible={setModalIsVisible} customStyles={styles['ChangePasswordModal']}>
      <Container fluid>
        <Row align="center" justify="center" className={styles['ChangePasswordModal-Row']}><b>{text}</b></Row>
        <Row align="center" justify="center" className={styles['ChangePasswordModal-Row']}>
          <Col lg={10} className={styles['ChangePasswordModal-Col']}>
            <InputField id="old-password" name="old-password" type="password" maxlength={50} placeholder="Old password" value={oldPassword} onChange={handleOldPasswordChange} />
          </Col>
          <Col lg={10} className={styles['ChangePasswordModal-Col']}>
            <InputField id="new-password" name="new-password" type="password" maxlength={50} placeholder="New password" value={newPassword} onChange={handleNewPasswordChange} />
          </Col>
          {validationMessage && (
            <AlertMessage messageStyle={AlertMessageStyle.WARNING}>{validationMessage}</AlertMessage>
          )}
          <Col lg={10} className={styles['ChangePasswordModal-Col']}>
            <InputField id="confirmed-password" name="confirmed-password" type="password" maxlength={50} placeholder="Confirm password" value={confirmedPassword} onChange={handleConfirmedPasswordChange} />
          </Col>
          {errorMessage && (
            <AlertMessage messageStyle={AlertMessageStyle.WARNING}>{errorMessage}</AlertMessage>
          )}
        </Row>
        <Row align="center" justify="center" className={styles['ChangePasswordModal-Row']}>
          <Button type="submit" onClick={handleChangePasswordClick}>Save</Button>
        </Row>
      </Container>
    </Modal>
  );
};

export default ChangePasswordModal;
