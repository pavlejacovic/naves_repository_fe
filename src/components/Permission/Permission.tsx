import React, { useContext, useState } from 'react';
import { Col, Row } from 'react-grid-system';
import { toast } from 'react-toastify';
import { ProjectsContext } from '../../Contexts/ProjectsContext';
import { ProjectUsers } from '../../models/project.interface';
import boxService from '../../services/box.service';
import { QUESTION_DELETING_PERMISSION } from '../../utils/constants/questions/questions';
import { PERMISSION_VALUES } from '../../utils/constants/selectObjects/selectObjects';
import { Button } from '../Button/Button';
import ConfirmModal from '../ConfirmModal/ConfirmModal';
import Select from '../Select/Select';
import styles from './Permission.module.scss';

interface PermissionProps {
  project: ProjectUsers;
  projectId: number;
  setEmptyFieldMessage: (value: string) => void
}

const Permission: React.FC<PermissionProps> = ({ project, projectId, setEmptyFieldMessage }) => {
  const [sharePermission, setSharePermission] = useState<string>('');
  const [confirmModalIsVisible, setConfirmModalIsVisible] = useState<boolean>(false);
  const {
    fetchProjectUsers
  } = useContext(ProjectsContext);

  const changePermission = (): void => {
    const data: { email: string, permission: string }[] = [{
      email: project.user.email,
      permission: sharePermission
    }];
    boxService.shareProject(projectId, data)
      .then((response) => {
        fetchProjectUsers(projectId);
        response.data.map((info: { successful: boolean, message: string }) => toast.info(info.message));
      })
      .catch((error) => error.response.data
        .map((info: { successful: boolean, message: string }) => toast.error(info.message)));
  };

  const handleChangePermissionClick = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    setSharePermission(event.target.value);
    fetchProjectUsers(projectId);
    setEmptyFieldMessage('');
  };

  const handlePermissionChangeClick = (): void => {
    if (!sharePermission) return;
    if (sharePermission === 'DELETE') {
      setConfirmModalIsVisible(true);
    } else {
      changePermission();
      fetchProjectUsers(projectId);
    }
    setEmptyFieldMessage('');
  };

  const handleCloseConfirmModalClick = () => {
    setConfirmModalIsVisible(false);
  };

  const handleRemovePermissionClick = () => {
    changePermission();
    fetchProjectUsers(projectId);
  };

  return (
    <>
      <Row align="center" justify="start" key={project.id} className={styles['Permission-Row']}>
        <Col md={6} className={styles['Permission-Info']} title={project.user['email']}>
          {project.user['email']}
        </Col>
        {project.writePermission
          ? <Col md={1} className={styles['Permission-Info_Editor']}>write</Col>
          : <Col md={1} className={styles['Permission-Info_ReadOnly']}>read</Col>}
        <Col md={3}>
          <Select id="select permision" customStyle={styles['Permission-Select']} onChange={handleChangePermissionClick}>
            <option hidden defaultValue="">Change:</option>
            {Object.entries(PERMISSION_VALUES)
              .map(([value, label]: string[]) => <option key={`${value}key`} value={value}>{label}</option>)}
          </Select>
        </Col>
        <Col md={2}>
          <Button type="button" dataTestId="ChangePermissionBtn" customStyles={styles['ProjectFolder-ShareModal-AddBtn']} onClick={handlePermissionChangeClick}>Save</Button>
        </Col>
      </Row>
      <ConfirmModal
        text={`${QUESTION_DELETING_PERMISSION}${project.user.email}?`}
        modalIsVisible={confirmModalIsVisible}
        setModalIsVisible={setConfirmModalIsVisible}
        onConfirm={handleRemovePermissionClick}
        onClose={handleCloseConfirmModalClick}
      />
    </>
  );
};

export default Permission;
