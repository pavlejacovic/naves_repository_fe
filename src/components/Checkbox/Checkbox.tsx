import React, { ChangeEvent } from 'react';

interface CheckboxProps {
  dataTestId: string,
  checked: boolean,
  value?: string,
  handleOnChange: (e: ChangeEvent<HTMLInputElement>) => void,
  customStyles?: string
}

const Checkbox:React.FC<CheckboxProps> = ({
  dataTestId, checked = false, value, handleOnChange, customStyles
}):JSX.Element => (
  <input type="checkbox" key={dataTestId} data-test-id={dataTestId} checked={checked} value={value} onChange={handleOnChange} className={customStyles} />
);

export default Checkbox;
