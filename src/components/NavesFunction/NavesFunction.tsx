import React, {
  ChangeEvent,
  useContext,
  useEffect,
  useState
} from 'react';
import { toast } from 'react-toastify';
import { Col, Row } from 'react-grid-system';
import { MdContentCopy, MdFileUpload, MdMoreHoriz } from 'react-icons/md';
import DropdownButton from '../DropdownButton/DropdownButton';
import styles from './NavesFunction.module.scss';
import smallLogo from '../../assets/img/small_logo.png';
import { NavesFunctionInfo } from '../../models/function.interface';
import { Button, ButtonStyle } from '../Button/Button';
import ConfirmModal from '../ConfirmModal/ConfirmModal';
import { QUESTION_DELETING_NAVES_FUNCTION } from '../../utils/constants/questions/questions';
import { NavesFunctionsContext } from '../../Contexts/NavesFunctionsContext';
import SaveFunctionModal from '../SaveFunctionModal/SaveFunctionModal';
import { FILES_ARE_NOT_SPECIFIED, LANGUAGE_IS_NOT_SELECTED } from '../../utils/constants/validationMessages/validation-messages';
import Modal from '../Modal/Modal';
import AlertMessage, { AlertMessageStyle } from '../AlertMessage/AlertMessage';
import NavesFunctionsService from '../../services/naves-functions.service';
import StatisticModal from '../StatisticModal/StatisticModal';
import RadioButton from '../RadioButton/RadioButton';

const STATISTIC_MODAL_TITLE: string = 'Number of executions per day';

interface NavesFunctionProps {
  navesFunction: NavesFunctionInfo;
  index: number;
}

const NavesFunction: React.FC<NavesFunctionProps> = ({
  navesFunction,
  index
}): JSX.Element => {
  const { fetchFunctionsOnPage, fetchAllMyFunctions } = useContext(NavesFunctionsContext);
  const [isConfirmModalVisible, setIsConfirmModalVisible] = useState<boolean>(false);
  const [isModalWithNewLinkVisible, setIsModalWithNewLinkVisible] = useState<boolean>(false);
  const [newLink, setNewLink] = useState<string | undefined>('');
  const [isSaveFunctionModalVisible, setIsSaveFunctionModalVisible] = useState<boolean>(false);
  const [isStatisticModalVisible, setIsStatisticModalVisible] = useState<boolean>(false);
  const [isUploadJarModalVisible, setIsUploadJarModalVisible] = useState<boolean>(false);
  const [jarFileNotUploaded, setJarFileNotUploaded] = useState<boolean>(false);
  const [chosenFile, setChosenFile] = useState<FileList | null>(null);

  const [isRadioBtnSelectedMessage, setIsRadioBtnSelectedMessage] = useState<boolean>(false);
  const [selectedRadioBtn, setSelectedRadioBtn] = useState<string>('');
  const isRadioSelected = (value: string): boolean => selectedRadioBtn === value;

  const handleRadioBtnValueChange = (e: ChangeEvent<HTMLInputElement>): void => {
    setSelectedRadioBtn(e.currentTarget.value);
    setIsRadioBtnSelectedMessage(false);
  };

  const handleConfirmModalCloseClick = (): void => setIsConfirmModalVisible(false);

  const handleDeleteButtonClick = (): void => setIsConfirmModalVisible(true);

  const handleGenerateNewLinkClick = (): void => {
    if (navesFunction.functionId) {
      NavesFunctionsService.generateNewLink(navesFunction.functionId)
        .then((response) => {
          setNewLink(response.data);
          fetchAllMyFunctions();
          toast.success('New link has been successfully created!');
          setIsModalWithNewLinkVisible(true);
        });
    }
  };

  const handleDeleteNavesFunctionClick = (): void => {
    if (navesFunction.functionId) {
      NavesFunctionsService.deleteNavesFunction(navesFunction.functionId)
        .then((response) => {
          toast.success(response.data);
          fetchFunctionsOnPage();
          fetchAllMyFunctions();
        })
        .finally(() => setIsConfirmModalVisible(false));
    }
  };

  const displayMessageWhenLinkHasBeenCopied = (link: string | undefined): void => {
    if (link) {
      navigator.clipboard.writeText(link);
      toast.info('Link has been successfully copied.');
    }
  };

  const copyLink = (): void => displayMessageWhenLinkHasBeenCopied(newLink);

  const makeButtonForLinkOfTheFunction = () => (
    <Button
      type="button"
      buttonStyle={ButtonStyle.SECONDARY}
      customStyles={`${styles['NavesFunction-Button']} ${styles['NavesFunction-Button-Link']}`}
      onClick={copyLink}
    >
      <MdContentCopy />
      Copy link
    </Button>
  );

  const handleChosenJarFileChange = (event: ChangeEvent<HTMLInputElement>) => {
    setChosenFile(event.target.files);
    setJarFileNotUploaded(false);
  };

  const handleUpdateExecutionButtonClick = () => {
    setIsUploadJarModalVisible(true);
    setChosenFile(null);
    setJarFileNotUploaded(false);
    setIsRadioBtnSelectedMessage(false);
    setSelectedRadioBtn('');
  };

  const handleUploadFile = () => {
    if (navesFunction.functionId && chosenFile && selectedRadioBtn) {
      const fileForUpload: File = chosenFile[0];
      NavesFunctionsService.updateNavesFunction(navesFunction.functionId, fileForUpload, selectedRadioBtn)
        .then((response) => {
          toast.success(response.data);
          setChosenFile(null);
        });
      setIsUploadJarModalVisible(false);
    }
    if (!chosenFile) {
      setJarFileNotUploaded(true);
    }
    if (!selectedRadioBtn) {
      setIsRadioBtnSelectedMessage(true);
    }
  };

  const handleUpdatePropertiesButtonClick = (): void => setIsSaveFunctionModalVisible(true);
  const handleStatisticButtonClick = (): void => setIsStatisticModalVisible(true);

  useEffect(() => {
    if (navesFunction) {
      setNewLink(navesFunction.functionLink);
    }
  }, []);

  return (
    <li data-test-id={`${navesFunction.functionName}${index}`} className={`${styles['NavesFunction']} ${styles['NavesFunction-Link']}`}>
      <Row justify="between" align="center">
        <Col lg={1}>
          <Row justify="end">
            <Col lg={5}><img src={smallLogo} alt="naves logo" /></Col>
          </Row>
        </Col>
        <Col lg={3} className={styles['NavesFunction-Text']}>{navesFunction.functionName}</Col>
        <Col lg={2} className={styles['NavesFunction-Text']}>{navesFunction.publicAccess ? 'public' : 'private'}</Col>
        <Col lg={2} className={styles['NavesFunction-Text']}>{navesFunction.trigger}</Col>
        <Col lg={3} className={styles['NavesFunction-Text']}>
          {(navesFunction.trigger === 'POST' || navesFunction.trigger === 'GET') && makeButtonForLinkOfTheFunction()}
        </Col>
        <Col lg={1}>
          <DropdownButton text={<MdMoreHoriz className={styles['NavesFunction-ButtonIcon']} />} customStyles={styles['NavesFunction-DropdownButton']}>
            <ul className={styles['NavesFunction-DropdownButton-List']}>
              {(navesFunction.trigger === 'POST' || navesFunction.trigger === 'GET') && (
                <li>
                  <Button dataTestId="generateNavesLinkBtn" type="button" customStyles={styles['NavesFunction-DropdownButton-List-Action']} onClick={handleGenerateNewLinkClick}>Generate link</Button>
                </li>
              )}
              <li>
                <Button dataTestId="updtNavesFunctionBtn" type="button" customStyles={styles['NavesFunction-DropdownButton-List-Action']} onClick={handleUpdateExecutionButtonClick}>Update execution</Button>
              </li>
              <li>
                <Button dataTestId="updateNavesFunctionBtn" type="button" customStyles={styles['NavesFunction-DropdownButton-List-Action']} onClick={handleUpdatePropertiesButtonClick}>Update properties</Button>
              </li>
              <li>
                <Button dataTestId="deleteNavesFunctionBtn" type="button" customStyles={styles['NavesFunction-DropdownButton-List-Action']} onClick={handleDeleteButtonClick}>Delete</Button>
              </li>
              <li>
                <Button dataTestId="statisticNavesFunctionBtn" type="button" customStyles={styles['NavesFunction-DropdownButton-List-Action']} onClick={handleStatisticButtonClick}>Statistic</Button>
              </li>
            </ul>
          </DropdownButton>
        </Col>
      </Row>
      <ConfirmModal
        text={QUESTION_DELETING_NAVES_FUNCTION}
        modalIsVisible={isConfirmModalVisible}
        setModalIsVisible={setIsConfirmModalVisible}
        onConfirm={handleDeleteNavesFunctionClick}
        onClose={handleConfirmModalCloseClick}
      />
      <StatisticModal
        title={STATISTIC_MODAL_TITLE}
        modalIsVisible={isStatisticModalVisible}
        setModalIsVisible={setIsStatisticModalVisible}
        functionId={navesFunction.functionId}
        customStyle={styles['NavesFunction-Modal']}
      />
      <Modal
        modalIsVisible={isModalWithNewLinkVisible}
        setModalIsVisible={setIsModalWithNewLinkVisible}
      >
        <Button
          type="button"
          buttonStyle={ButtonStyle.SECONDARY}
          customStyles={styles['NavesFunction-Button']}
          onClick={copyLink}
        >
          <MdContentCopy />
          {newLink}
        </Button>
      </Modal>
      <Modal
        modalIsVisible={isUploadJarModalVisible}
        setModalIsVisible={setIsUploadJarModalVisible}
        actions={
          (
            <>
              <Col md="content" sm={12}><Button type="button" dataTestId="sendChosenFilesBtn" customStyles={styles['NavesFunction-Modal-Button']} onClick={handleUploadFile}>Upload</Button></Col>
            </>
          )
        }
      >
        <div className={styles['NavesFunction-Modal']}>
          <input id="uploadFile" type="file" className={styles['NavesFunction-Modal-InputField']} onChange={handleChosenJarFileChange} />
          <label htmlFor="uploadFile" className={styles['NavesFunction-Modal-InputField-Label']}>
            <MdFileUpload />
            Browse
          </label>
          {jarFileNotUploaded
            && <AlertMessage messageStyle={AlertMessageStyle.WARNING} customStyles={styles['NavesFunction-Modal-Message']}>{FILES_ARE_NOT_SPECIFIED}</AlertMessage>}
          {chosenFile && (
            <>
              <ul className={styles['NavesFunction-Modal-ChosenFiles']}>
                <li>Chosen file:</li>
                {
                  Object.keys(chosenFile).map((file: any) => (
                    <li key={chosenFile[file].name} data-test-id={chosenFile[file].name} className={styles['NavesFunction-Modal-ChosenFiles-File']}>{chosenFile[file].name}</li>
                  ))
                }
              </ul>
              <div>
                <p>Select language:</p>
                <div>
                  <RadioButton id="java" name="programmingLanguage" value="JAVA" checked={isRadioSelected('JAVA')} dataTestId="radioBtnJava" onChange={handleRadioBtnValueChange} />
                  <label htmlFor="JAVA">Java</label>
                </div>
                <div>
                  <RadioButton id="csharp" name="programmingLanguage" value="CSHARP" checked={isRadioSelected('CSHARP')} dataTestId="radioBtnCsharp" onChange={handleRadioBtnValueChange} />
                  <label htmlFor="CSHARP">  C#</label>
                </div>
              </div>
              {isRadioBtnSelectedMessage
            && <AlertMessage messageStyle={AlertMessageStyle.WARNING} customStyles={styles['NavesFunction-Modal-Message']}>{LANGUAGE_IS_NOT_SELECTED}</AlertMessage>}
            </>
          )}
        </div>
      </Modal>
      {
        navesFunction && (
          <SaveFunctionModal
            modalIsVisible={isSaveFunctionModalVisible}
            setModalIsVisible={setIsSaveFunctionModalVisible}
            title="Update function properties"
            navesFunction={navesFunction}
          />
        )
      }
    </li>
  );
};

export default NavesFunction;
