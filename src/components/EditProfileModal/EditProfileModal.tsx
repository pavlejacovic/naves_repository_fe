import React, { ChangeEvent, useEffect, useState } from 'react';
import { Col, Container, Row } from 'react-grid-system';
import { toast } from 'react-toastify';
import { ReceivedUsersInfo } from '../../models/user.interface';
import AuthenticationService from '../../services/authentication.service';
import { ALL_FILEDS_REQUIRED } from '../../utils/constants/validationMessages/validation-messages';
import AlertMessage, { AlertMessageStyle } from '../AlertMessage/AlertMessage';
import { Button } from '../Button/Button';
import Checkbox from '../Checkbox/Checkbox';
import InputField from '../InputField/InputField';
import Modal from '../Modal/Modal';
import styles from './EditProfileModal.module.scss';

export interface EditProfileModalProps {
  setModalIsVisible: React.Dispatch<React.SetStateAction<boolean>>;
  modalIsVisible: boolean;
  text: string;
  user: ReceivedUsersInfo;
}

const EditProfileModal: React.FC<EditProfileModalProps> = (
  {
    modalIsVisible,
    setModalIsVisible,
    text,
    user
  }
) => {
  const [name, setName] = useState<string>(user.name);
  const [surname, setSurname] = useState<string>(user.surname);
  const [subscribed, setSubscribed] = useState<boolean>(user.subscribed);
  const [allFieldsRequired, setAllFieldsRequired] = useState<string>('');

  const handleNameChange = (event: ChangeEvent<HTMLInputElement>): void => setName(event.target.value);
  const handleSurnameChange = (event: ChangeEvent<HTMLInputElement>): void => setSurname(event.target.value);
  const handleSubcsribeChange = (event: ChangeEvent<HTMLInputElement>): void => setSubscribed(event.target.checked);
  const handleEditProfileClick = (): void => {
    if (name && surname) {
      const data: ReceivedUsersInfo = {
        name: name,
        surname: surname,
        subscribed: subscribed
      };
      AuthenticationService.postUserInformation(data)
        .then((response) => {
          toast.success(response.data);
        })
        .finally(() => setModalIsVisible(false));
      return;
    }
    setAllFieldsRequired(ALL_FILEDS_REQUIRED);
  };

  useEffect(() => {
    setName(user.name);
    setSurname(user.surname);
    setSubscribed(user.subscribed);
    setAllFieldsRequired('');
  }, [modalIsVisible]);

  return (
    <Modal modalIsVisible={modalIsVisible} setModalIsVisible={setModalIsVisible} customStyles={styles['EditProfileModal']}>
      <Container fluid>
        <Row align="center" justify="center" className={styles['EditProfileModal-Row']}><b>{text}</b></Row>
        <Row align="center" justify="center" className={styles['EditProfileModal-Row']}>
          <Col lg={3} className={styles['EditProfileModal-Col']}>Name: </Col>
          <Col lg={9} className={styles['EditProfileModal-Col']}>
            <InputField id="name" name="name" type="text" maxlength={225} placeholder="" inputWarning={!name} value={name} onChange={handleNameChange} />
          </Col>
          <Col lg={3} className={styles['EditProfileModal-Col']}>Surname: </Col>
          <Col lg={9} className={styles['EditProfileModal-Col']}>
            <InputField id="surname" name="surname" type="text" maxlength={225} placeholder="" inputWarning={!surname} value={surname} onChange={handleSurnameChange} />
          </Col>
          {allFieldsRequired && (
            <Col lg={12} className={styles['EditProfileModal-Col']}>
              <AlertMessage messageStyle={AlertMessageStyle.WARNING} customStyles={styles['EditProfileModal-AlertMessage']}>{ALL_FILEDS_REQUIRED}</AlertMessage>
            </Col>
          )}
          <Col lg={3} className={styles['EditProfileModal-Col']}>Subscribed: </Col>
          <Col lg={9} className={styles['EditProfileModal-Col']}>
            <Checkbox dataTestId="subscribed-yes" checked={subscribed} handleOnChange={handleSubcsribeChange} />
          </Col>
        </Row>
        <Row align="center" justify="center" className={styles['EditProfileModal-Row']}>
          <Button type="submit" onClick={handleEditProfileClick}>Save</Button>
        </Row>
      </Container>
    </Modal>
  );
};

export default EditProfileModal;
