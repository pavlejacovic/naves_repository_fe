import React, { useEffect, useState } from 'react';
import jwtDecode from 'jwt-decode';
import { Link, useHistory } from 'react-router-dom';
import logo from '../../assets/img/logo.png';
import localStorageService from '../../services/local-storage.service';
import ListItem from '../ListItem/ListItem';
import styles from './NavigationBar.module.scss';
import { Button, ButtonStyle } from '../Button/Button';
import ConfirmModal from '../ConfirmModal/ConfirmModal';
import { SIGN_OUT_QUESTION } from '../../utils/constants/questions/questions';
import authenticationService from '../../services/authentication.service';

const NavigationBar: React.FC = (): JSX.Element => {
  const [role, setRole] = useState<number>(0);
  const pathName: string = decodeURIComponent(window.location.pathname);
  const token: string = localStorageService.getToken();
  const decodedToken: any = jwtDecode(token);
  const user: string = decodedToken.sub;
  const [isSignOutConfirmationModalVisible, setIsSignOutConfirmationModalVisible] = useState<boolean>(false);
  const history = useHistory();

  const handleModalIsVisible = (): void => setIsSignOutConfirmationModalVisible(!isSignOutConfirmationModalVisible);

  const handleSignOutUser = (): void => {
    localStorageService.removeToken();
    history.push('/login');
  };

  const getUserRole = (): void => {
    authenticationService.getUserType()
      .then((response) => setRole(response.data.type));
  };

  useEffect(() => {
    getUserRole();
  });

  return (
    <div className={styles['NavigationBar']}>
      <img className={styles['NavigationBar-Logo']} src={logo} alt="Naves logo" />
      <div className={styles['NavigationBar-Items']}>
        <ul>
          <ListItem dataTestId="myProjectsLink" active={pathName.includes('/projects')} text="My projects" linkTo="/projects" />
          <ListItem dataTestId="sharedProjectsWithMe" active={pathName === '/shared-with-me'} text="Shared projects with me" linkTo="/shared-with-me" />
          <ListItem dataTestId="myNavesFunctions" active={pathName === '/naves-functions'} text="My Naves functions" linkTo="/naves-functions" />
        </ul>
      </div>
      <div className={styles['NavigationBar-User']}>
        {role === 2 && (
          <Link to="/admin" className={`${styles['NavigationBar-Admin']} ${pathName.includes('/admin') ? 'NavigationBar_active' : ''}`}>Dashboard</Link>
        )}
        {role === 1 && (
          <Link to="/profile" className={`${styles['NavigationBar-User-Profile']} ${pathName.includes('/profile') ? 'NavigationBar_active' : ''}`}>Profile</Link>
        )}
        <span className={styles['NavigationBar-UserName']}>{user}</span>
        <Button type="button" dataTestId="signOutBtn" buttonStyle={ButtonStyle.SECONDARY} onClick={handleModalIsVisible}>Sign out</Button>
      </div>
      <ConfirmModal
        text={SIGN_OUT_QUESTION}
        onClose={handleModalIsVisible}
        onConfirm={handleSignOutUser}
        modalIsVisible={isSignOutConfirmationModalVisible}
        setModalIsVisible={setIsSignOutConfirmationModalVisible}
      />
    </div>
  );
};

export default NavigationBar;
