import React, { useMemo } from 'react';
import {
  Col,
  Container,
  Hidden,
  Row,
  Visible
} from 'react-grid-system';
import {
  MdFirstPage,
  MdLastPage,
  MdNavigateBefore,
  MdNavigateNext
} from 'react-icons/md';
import { Link } from 'react-router-dom';
import { STARTING_PAGE, MAX_PAGES } from '../../utils/constants/paginationConsants/paginationConstants';
import countNumberOfPages from '../../utils/functions/paginationFunctions/paginationFunctions';
import { changeURLParamsLink } from '../../utils/functions/queryParamsFunctions/queryParamsFunctions';
import useUpdateCurrentPage from '../../utils/hooks/useLastPage';
import styles from './Pagination.module.scss';

interface PaginationProps {
  itemsPerPage: number;
  totalNumberOfItems: number;
  pageNumber: number;
}

const getPagingRange = (currentPage: number, totalPages: number): number[] => {
  const numberOfPagesToDisplay = MAX_PAGES < totalPages ? MAX_PAGES : totalPages;

  const delta = Math.floor(numberOfPagesToDisplay / 2);

  let firstPage = currentPage - delta;

  firstPage = Math.max(firstPage, STARTING_PAGE);

  const lastPossibleFirstPage = (totalPages + STARTING_PAGE) - numberOfPagesToDisplay;
  firstPage = Math.min(firstPage, lastPossibleFirstPage);

  return Array.from({ length: numberOfPagesToDisplay }, (_, i) => firstPage + i);
};

const Pagination: React.FC<PaginationProps> = ({
  itemsPerPage,
  totalNumberOfItems,
  pageNumber
}) => {
  const totalNumberOfPages = useMemo(() => countNumberOfPages(totalNumberOfItems, itemsPerPage),
    [totalNumberOfItems, itemsPerPage]);
  const PAGE: string = 'page';

  useUpdateCurrentPage(totalNumberOfPages, pageNumber);

  return (
    <div className={styles.Pagination}>
      <Container fluid>
        <Row justify="center" align="center">
          <Col xs={1} md="content">
            <Link to={changeURLParamsLink(PAGE, STARTING_PAGE.toString())} className={styles['Pagination-Link']}>
              <Visible xxl>First</Visible>
              <Hidden xxl><MdFirstPage /></Hidden>
            </Link>
          </Col>
          <Col xs={1} md="content">
            <Link to={changeURLParamsLink(PAGE, (pageNumber - 1).toString())} className={styles['Pagination-Link']}>
              <Visible xxl>Previous</Visible>
              <Hidden xxl><MdNavigateBefore /></Hidden>
            </Link>
          </Col>
          {getPagingRange(pageNumber, totalNumberOfPages).map((number) => (
            <Col xs={1} md="content" key={number}>
              <Link to={changeURLParamsLink(PAGE, number.toString())} className={`${styles['Pagination-Link']} ${(pageNumber === number) && styles['Pagination-Link_active']}`}>
                {number}
              </Link>
            </Col>
          ))}
          <Col xs={1} md="content">
            <Link to={changeURLParamsLink(PAGE, Math.min(pageNumber + 1, totalNumberOfPages).toString())} className={`${styles['Pagination-Link']}`}>
              <Visible xxl>Next</Visible>
              <Hidden xxl><MdNavigateNext /></Hidden>
            </Link>
          </Col>
          <Col xs={1} md="content">
            <Link to={changeURLParamsLink(PAGE, totalNumberOfPages.toString())} className={styles['Pagination-Link']}>
              <Visible xxl>Last</Visible>
              <Hidden xxl><MdLastPage /></Hidden>
            </Link>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Pagination;
