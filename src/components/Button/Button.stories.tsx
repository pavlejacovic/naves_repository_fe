import React from 'react';
import { Button, ButtonStyle } from './Button';

export default {
  title: 'Button',
  component: Button
};

export const defaultButton = () => <Button dataTestId="default" type="button">Default</Button>;
export const secondary = () => <Button dataTestId="secondary" buttonStyle={ButtonStyle.SECONDARY} type="button">Secondary</Button>;
