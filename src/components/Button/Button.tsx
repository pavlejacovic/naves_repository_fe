import React, { MouseEvent } from 'react';
import { Link } from 'react-router-dom';
import getClassName from '../../utils/functions/classesHelper/classesHelper';
import styles from './Button.module.scss';

export enum ButtonStyle {
  SECONDARY = 'Button_secondary',
  GOOGLE = 'Button_google'
}

interface Props {
  linkTo?: string;
  customStyles?: string;
  buttonStyle?: string;
  type: 'button' | 'submit' | 'reset' | undefined;
  dataTestId?: string;
  onClick?: (event: MouseEvent<HTMLButtonElement>) => void;
  id?: string;
}

const Button: React.FC<Props> = (
  {
    buttonStyle = '', type, onClick, linkTo, children, customStyles = '', dataTestId
  }
): JSX.Element => {
  if (linkTo) {
    return (
      <Link className={getClassName(styles['Button'], customStyles, styles[buttonStyle])} to={linkTo}>{children}</Link>
    );
  }
  return (
    <button className={getClassName(styles['Button'], customStyles, styles[buttonStyle])} type={type} data-test-id={dataTestId} onClick={onClick}>
      {children}
    </button>
  );
};

export { Button };
