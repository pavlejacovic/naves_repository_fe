import React from 'react';
import Folder from './Folder';

export default {
  title: 'Folder component',
  component: Folder
};

export const folder = () => <Folder projectId={1} dataTestId="2" folder={{ folderPath: 'xxx', folderSize: 5, folderName: 'folder2' }} />;
