import React, { MouseEvent, useContext, useState } from 'react';
import { Col, Row } from 'react-grid-system';
import { MdFolder, MdMoreHoriz } from 'react-icons/md';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { Folder as FolderModel } from '../../models/folder.interface';
import BoxService from '../../services/box.service';
import fileSaverService from '../../services/file-saver.service';
import bytesToMegaBytes from '../../utils/functions/unitsConversions/unitsConversions';
import { Button } from '../Button/Button';
import DropdownButton from '../DropdownButton/DropdownButton';
import SaveModal from '../SaveModal/SaveModal';
import styles from './Folder.module.scss';
import { QUESTION_DELETING_FOLDER } from '../../utils/constants/questions/questions';
import { ProjectPageContext } from '../../Contexts/ProjectPageContext';
import { AuthorizedPagesContext } from '../../Contexts/AuthorizedPagesContext';
import ConfirmModal from '../ConfirmModal/ConfirmModal';

interface FolderProps {
  dataTestId: string,
  folder: FolderModel,
  projectId: number,
  writePermission?: boolean | undefined
}

const Folder: React.FC<FolderProps> = ({
  dataTestId,
  folder,
  projectId,
  writePermission
}) => {
  const { fetchFoldersAndFiles } = useContext(ProjectPageContext);
  const { pathToTheSource } = useContext(AuthorizedPagesContext);
  const path = folder?.folderPath || folder?.folderName;
  const [isRenameFolderModalVisible, setIsRenameFolderModalVisible] = useState<boolean>(false);
  const [isConfirmModalVisible, setIsConfirmModalVisible] = useState<boolean>(false);

  const handleDownloadFolderClick = (event: React.MouseEvent<HTMLButtonElement>): void => {
    event.preventDefault();
    if (projectId) {
      BoxService.downloadAsZip(projectId, [{ path: path }]).then(
        (response) => {
          fileSaverService.downloadZip(response.data, folder.folderName);
        }
      ).catch((error) => toast.error(error.response.data));
    }
  };

  const handleDeleteButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    setIsConfirmModalVisible(true);
  };

  const handleDeleteFolderClick = () => {
    const requestBody = [{ path: folder.folderPath }];
    if (projectId) {
      BoxService.deleteFileOrFolder(projectId, requestBody).then(
        (response) => {
          toast.info(response.data[0].message);
          setIsConfirmModalVisible(false);
          fetchFoldersAndFiles(pathToTheSource);
        }
      ).catch((error) => toast.error(error.response.data[0].message));
    }
  };

  const handleRenameBtnClick = (e: MouseEvent): void => {
    e.preventDefault();
    setIsRenameFolderModalVisible(true);
  };

  const handleRenameFolderSave = (
    newName: string,
    setInputText: React.Dispatch<React.SetStateAction<string>>
  ): void => {
    BoxService.renameFileOrFolder(projectId, path, newName).then((response) => {
      toast.success(response.data);
      setInputText('');
      setIsRenameFolderModalVisible(false);
      fetchFoldersAndFiles(pathToTheSource);
    });
  };

  const handleConfirmModalCloseClick = () => setIsConfirmModalVisible(false);

  return (

    <>
      <li data-test-id={dataTestId} className={styles['Folder']}>
        <Link to={`${pathToTheSource}/${folder?.folderName}`} className={styles['Folder-Link']}>
          <Row justify="between" align="center">
            <Col lg={1}>
              <Row justify="end">
                <Col lg={5}><MdFolder /></Col>
              </Row>
            </Col>
            <Col lg={9} className={styles['Folder-Text']}>{folder?.folderName}</Col>
            <Col lg={1} className={styles['Folder-Text']}>{folder?.folderSize ? bytesToMegaBytes(folder?.folderSize) : '0 MB'}</Col>
            <Col lg={1}>
              <DropdownButton text={<MdMoreHoriz className={styles['Folder-ButtonIcon']} />} customStyles={styles['Folder-Button']}>
                <ul>
                  <li>
                    <Button dataTestId="downloadBtn" type="button" customStyles={styles['Folder-Action']} onClick={handleDownloadFolderClick}>Download</Button>
                  </li>
                  {writePermission
                && (
                  <>
                    <li>
                      <Button dataTestId="renameBtn" type="button" customStyles={styles['Folder-Action']} onClick={handleRenameBtnClick}>Rename</Button>
                    </li>
                    <li>
                      <Button dataTestId="deleteBtn" type="button" customStyles={styles['Folder-Action']} onClick={handleDeleteButtonClick}>Delete</Button>
                    </li>
                  </>
                )}
                </ul>
              </DropdownButton>
            </Col>
          </Row>
        </Link>
      </li>
      <SaveModal
        title="Rename folder"
        inputPlaceholder="New name"
        modalIsVisible={isRenameFolderModalVisible}
        setModalIsVisible={setIsRenameFolderModalVisible}
        onSave={handleRenameFolderSave}
      >
        <Button type="submit" dataTestId="renameFolderBtn">Rename folder</Button>
      </SaveModal>
      <ConfirmModal
        text={QUESTION_DELETING_FOLDER}
        modalIsVisible={isConfirmModalVisible}
        setModalIsVisible={setIsConfirmModalVisible}
        onConfirm={handleDeleteFolderClick}
        onClose={handleConfirmModalCloseClick}
      />
    </>
  );
};

export default Folder;
