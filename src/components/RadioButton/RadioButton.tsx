import React, { ChangeEvent } from 'react';

interface RadioButtonProps {
  dataTestId: string,
  id: string,
  checked: boolean,
  value?: string,
  name: string,
  onChange: (e: ChangeEvent<HTMLInputElement>) => void,
  customStyles?: string
}

const RadioButton: React.FC<RadioButtonProps> = ({
  dataTestId, checked = false, value, onChange, customStyles, id, name
}):JSX.Element => (
  <input type="radio" id={id} data-test-id={dataTestId} name={name} checked={checked} value={value} onChange={onChange} className={customStyles} />
);

export default RadioButton;
