import React, {
  FormEvent,
  useState,
  ReactNode
} from 'react';
import { Container, Row } from 'react-grid-system';
import { FIELD_IS_REQUIRED } from '../../utils/constants/validationMessages/validation-messages';
import AlertMessage, { AlertMessageStyle } from '../AlertMessage/AlertMessage';
import InputField from '../InputField/InputField';
import Modal from '../Modal/Modal';
import styles from './SaveModal.module.scss';

export interface SaveModalProps {
  title: ReactNode;
  inputPlaceholder?: string;
  modalIsVisible: boolean;
  setModalIsVisible: React.Dispatch<React.SetStateAction<boolean>>;
  onSave(
    text: string,
    setInputText: React.Dispatch<React.SetStateAction<string>>
  ): void;
  customStyle?: string;
  dataTestId?:string;
}

const SaveModal: React.FC<SaveModalProps> = (
  {
    title,
    inputPlaceholder,
    modalIsVisible,
    setModalIsVisible,
    onSave,
    children,
    customStyle,
    dataTestId
  }
): JSX.Element => {
  const [inputText, setInputText] = useState<string>('');
  const [emptyFieldMessage, setEmptyFieldMessage] = useState<string>('');

  const resetForm = () => {
    setEmptyFieldMessage('');
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setInputText(event.target.value);
    resetForm();
  };

  const handleFormSubmit = (e: FormEvent<HTMLFormElement>): void => {
    e.preventDefault();
    resetForm();
    if (!inputText.trim()) {
      setEmptyFieldMessage(FIELD_IS_REQUIRED);
      return;
    }
    onSave(inputText, setInputText);
  };

  return (
    <Modal modalIsVisible={modalIsVisible} setModalIsVisible={setModalIsVisible}>
      <form onSubmit={handleFormSubmit} className={`${customStyle}`}>
        <Container fluid>
          <Row justify="center" align="center" className={styles['SaveModal-Row']}>
            <h3>{title}</h3>
          </Row>
          <Row justify="center" align="center">
            <InputField
              id="newProject"
              name="newProject"
              type="text"
              placeholder={inputPlaceholder}
              value={inputText}
              onChange={handleInputChange}
              inputWarning={!!emptyFieldMessage}
              dataTestId={dataTestId}
            />
          </Row>
          {emptyFieldMessage && (
            <Row justify="center" align="center"><AlertMessage messageStyle={AlertMessageStyle.WARNING}>{emptyFieldMessage}</AlertMessage></Row>
          )}
          <Row justify="center" align="center" className={styles['SaveModal-Row']}>
            {children}
          </Row>
        </Container>
      </form>
    </Modal>
  );
};

export default SaveModal;
