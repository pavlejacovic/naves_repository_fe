import React from 'react';
import { Button, ButtonStyle } from '../Button/Button';
import SaveModal from './SaveModal';

export default {
  title: 'Create new project',
  component: SaveModal
};

const sampleFunction = (text: string): Promise<any> => Promise.resolve(text);

export const NewProject = () => (
  <SaveModal
    title="Create new project"
    inputPlaceholder="Enter project name here..."
    modalIsVisible
    setModalIsVisible={() => true}
    onSave={sampleFunction}
  >
    <Button type="submit" buttonStyle={ButtonStyle.SECONDARY}>Save project</Button>
  </SaveModal>
);
