import React, {
  ChangeEvent,
  ReactNode,
  useContext,
  useEffect,
  useState
} from 'react';
import { Col, Container, Row } from 'react-grid-system';
import { MdFileUpload } from 'react-icons/md';
import { toast } from 'react-toastify';
import Select from 'react-select';
import styles from './SaveFunctionModal.module.scss';
import Modal from '../Modal/Modal';
import InputField from '../InputField/InputField';
import { NavesFunctionInfo } from '../../models/function.interface';
import { ProjectsContext } from '../../Contexts/ProjectsContext';
import { Button } from '../Button/Button';
import isFunctionNameValid from '../../utils/validations/inputValidationsForFunctionName/inputValidationsForFunctionName';
import {
  FIELD_IS_REQUIRED,
  JAR_FILE_IS_REQUIRED,
  FUNCTION_NAME_REQUIREMENTS,
  LANGUAGE_IS_REQUIRED,
  FILE_EXTENSION_FOR_JAVA,
  FILE_EXTENSION_FOR_CSHARP
} from '../../utils/constants/validationMessages/validation-messages';
import AlertMessage, { AlertMessageStyle } from '../AlertMessage/AlertMessage';
import { NavesFunctionsContext } from '../../Contexts/NavesFunctionsContext';
import { Project } from '../../models/project.interface';
import { OptionValueBoolean, OptionValueNumber, OptionValueString } from '../../models/optionValue.interface';
import {
  ACCESS_OPTIONS,
  BUCKET_TYPE_OPTIONS,
  LANGUAGE_OPTIONS,
  TRIGGER_OPTIONS
} from '../../utils/constants/optionsConstants/optionsConstants';
import bucketOption from '../../utils/constants/bucketTypeConstants/bucketTypeConstants';
import NavesFunctionsService from '../../services/naves-functions.service';

const EmptyAccessOption: OptionValueBoolean = { value: false, label: 'PRIVATE' };

export interface SaveFunctionModalProps {
  title: ReactNode;
  modalIsVisible: boolean;
  setModalIsVisible: React.Dispatch<React.SetStateAction<boolean>>;
  navesFunction?: NavesFunctionInfo;
  customStyle?: string;
}

const SaveFunctionModal: React.FC<SaveFunctionModalProps> = (
  {
    title,
    modalIsVisible,
    setModalIsVisible,
    navesFunction,
    customStyle
  }
): JSX.Element => {
  const { fetchFunctionsOnPage, fetchAllMyFunctions } = useContext(NavesFunctionsContext);
  const [chosenFile, setChosenFile] = useState<FileList | null>(null);
  const [jarFileIsNotSpecified, setJarFileIsNotSpecified] = useState<string>('');
  const [functionName, setFunctionName] = useState<string>('');
  const [badFunctionName, setBadFunctionName] = useState<string>('');
  const { projects } = useContext(ProjectsContext);
  const projectsOptions: OptionValueNumber[] = projects.map(
    (project: Project) => ({ value: project.id, label: project.projectName })
  );
  const [selectedLanguageOption, setSelectedLanguageOption] = useState<OptionValueString | null>(null);
  const [languageIsNotSpecified, setLanguageIsNotSpecified] = useState<string>('');
  const [selectedTriggerOption, setSelectedTriggerOption] = useState<OptionValueString | null>(null);
  const [selectedAccessOption, setSelectedAccessOption] = useState<OptionValueBoolean | null>(null);
  const [selectedBucketTypeOption, setSelectedBucketTypeOption] = useState<OptionValueString | null>(null);
  const [selectedProjectOption, setSelectedProjectOption] = useState<OptionValueNumber[]>([]);
  const [fieldIsRequired, setFieldIsRequired] = useState<string>('');
  const projectsForFunction = navesFunction?.projects?.map(
    (project: Project): OptionValueNumber => ({ value: project.id, label: project.projectName })
  );
  let projectsIds: (number | undefined)[] = selectedProjectOption?.map((project) => project.value);

  const handleFunctionNameOnChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setBadFunctionName('');
    setFunctionName(event.target.value);
  };

  const handleLanguageOptionChange = (value: any): void => {
    setLanguageIsNotSpecified('');
    setSelectedLanguageOption(value as OptionValueString);
  };

  const handleChosenFilesChange = (event: ChangeEvent<HTMLInputElement>) => {
    setJarFileIsNotSpecified('');
    setFieldIsRequired('');
    setSelectedTriggerOption(null);
    setSelectedAccessOption(null);
    setSelectedBucketTypeOption(null);
    setSelectedProjectOption([]);
    setChosenFile(event.target.files);
  };

  const handleTriggerOptionChange = (value: any) => {
    setFieldIsRequired('');
    setSelectedAccessOption(null);
    setSelectedBucketTypeOption(null);
    setSelectedProjectOption([]);
    setSelectedTriggerOption(value as OptionValueString);
  };

  const handleAccessOptionChange = (value: any) => {
    setFieldIsRequired('');
    setSelectedAccessOption(value as OptionValueBoolean);
  };

  const handleBucketTypeOptionChange = (value: any) => {
    setFieldIsRequired('');
    setSelectedAccessOption(EmptyAccessOption);
    setSelectedBucketTypeOption(value as OptionValueString);
  };

  const handleProjectChange = (value: any) => {
    setFieldIsRequired('');
    projectsIds = value.map((project: OptionValueNumber) => project.value);
    setSelectedProjectOption(value as OptionValueNumber[]);
  };

  const resetForm = (): void => {
    setFieldIsRequired('');
    setFunctionName('');
    setBadFunctionName('');
    setSelectedLanguageOption(null);
    setLanguageIsNotSpecified('');
    setChosenFile(null);
    setJarFileIsNotSpecified('');
    setSelectedAccessOption(EmptyAccessOption);
    setSelectedTriggerOption(null);
    setSelectedBucketTypeOption(null);
    setSelectedProjectOption([]);
  };

  const requireField = (value: any) => {
    if (!value) {
      setFieldIsRequired(FIELD_IS_REQUIRED);
    }
  };

  const setExistingFunctionValues = (navesFn: NavesFunctionInfo): void => {
    setFunctionName(navesFn.functionName);
    if (bucketOption(navesFn.trigger)) {
      setSelectedTriggerOption({ value: 'BUCKET', label: 'BUCKET' });
      setSelectedAccessOption({ value: false, label: 'PRIVATE' });
      setSelectedBucketTypeOption({
        value: navesFn.trigger,
        label: bucketOption(navesFn.trigger)
      });
      if (projectsForFunction) {
        setSelectedProjectOption(projectsForFunction);
      }
    } else if (navesFn.trigger === 'POST' || navesFn.trigger === 'GET') {
      setSelectedTriggerOption({ value: navesFn.trigger, label: navesFn.trigger });
      setSelectedAccessOption({ value: navesFn.publicAccess, label: navesFn.publicAccess ? 'PUBLIC' : 'PRIVATE' });
    }
  };

  const createNewNavesFunction = (trigger: string, project: (number | undefined)[] | undefined): void => {
    if (chosenFile && selectedLanguageOption && selectedAccessOption) {
      const data: NavesFunctionInfo = {
        functionName: functionName,
        language: selectedLanguageOption?.value,
        archiveFile: chosenFile[0],
        trigger: trigger,
        publicAccess: selectedAccessOption.value,
        projectIds: project
      };
      NavesFunctionsService.createNewNavesFunction(data)
        .then((response) => {
          toast.success(response.data);
          fetchAllMyFunctions();
          fetchFunctionsOnPage();
        })
        .finally(() => setModalIsVisible(false));
    }
  };

  const updatePropertiesForNavesFunction = (
    trigger: string, project: (number | undefined)[] | undefined
  ): void => {
    if (selectedAccessOption) {
      const data: NavesFunctionInfo = {
        functionName: functionName,
        trigger: trigger,
        publicAccess: selectedAccessOption.value,
        projectIds: project
      };
      if (navesFunction?.functionId) {
        NavesFunctionsService.updatePropsForNavesFunction(navesFunction?.functionId, data)
          .then(() => {
            toast.success(`Successfuly updated function ${data.functionName}`);
            fetchAllMyFunctions();
            fetchFunctionsOnPage();
          })
          .finally(() => setModalIsVisible(false));
      }
    }
  };

  const checkIfTriggerAccessAndProjectsAreEmpty = () => {
    requireField(selectedTriggerOption);
    if (selectedTriggerOption?.label === 'POST' || selectedTriggerOption?.label === 'GET') {
      requireField(selectedAccessOption);
      return;
    }
    requireField(selectedBucketTypeOption);
    requireField(selectedProjectOption.length);
  };

  const sendData = (
    trigger: string | undefined,
    dataManipulator: (trigger: string, projects: (number | undefined)[] | undefined) => void
  ): void => {
    if (trigger) {
      if (trigger === 'POST' || trigger === 'GET') {
        dataManipulator(trigger, undefined);
        return;
      }
      if (selectedBucketTypeOption && selectedProjectOption.length) {
        dataManipulator(selectedBucketTypeOption?.value, projectsIds);
      }
    }
  };

  const hanleSaveFunctionClick = (): void => {
    if (!isFunctionNameValid(functionName)) {
      setBadFunctionName(FUNCTION_NAME_REQUIREMENTS);
      return;
    }
    if (!navesFunction) {
      if (!chosenFile?.length) {
        setJarFileIsNotSpecified(JAR_FILE_IS_REQUIRED);
        return;
      }
      if (!selectedLanguageOption) {
        setLanguageIsNotSpecified(LANGUAGE_IS_REQUIRED);
        return;
      }
      checkIfTriggerAccessAndProjectsAreEmpty();
      sendData(selectedTriggerOption?.value, createNewNavesFunction);
    }
    checkIfTriggerAccessAndProjectsAreEmpty();
    sendData(selectedTriggerOption?.value, updatePropertiesForNavesFunction);
  };

  useEffect(() => {
    if (navesFunction) {
      setExistingFunctionValues(navesFunction);
      return;
    }
    resetForm();
  }, [modalIsVisible]);

  return (
    <Modal modalIsVisible={modalIsVisible} setModalIsVisible={setModalIsVisible} customStyles={styles['SaveFunctionModal']}>
      <Container fluid>
        <Row justify="center" align="center">
          <Col lg="content">
            <h3>{title}</h3>
          </Col>
        </Row>
        <Row justify="center" align="center" className={customStyle}>
          <Col lg="content">
            <InputField
              id="new-function"
              name="new-function"
              customStyles={styles['SaveFunctionModal-Filed']}
              type="text"
              placeholder={functionName || 'Enter function name here...'}
              maxlength={50}
              value={functionName}
              onChange={handleFunctionNameOnChange}
              inputWarning={badFunctionName}
            />
            {badFunctionName && <AlertMessage messageStyle={AlertMessageStyle.WARNING} customStyles={styles['SaveFunctionModal-AlertMessage']}>{badFunctionName}</AlertMessage>}
            {!navesFunction && (
              <Select
                className={styles['SaveFunctionModal-Select']}
                name="language"
                value={selectedLanguageOption}
                onChange={handleLanguageOptionChange}
                options={LANGUAGE_OPTIONS}
                placeholder="Select language"
              />
            )}
            {languageIsNotSpecified && <AlertMessage messageStyle={AlertMessageStyle.WARNING} customStyles={styles['SaveFunctionModal-AlertMessage']}>{languageIsNotSpecified}</AlertMessage>}
            {!navesFunction && (
              <div className={styles['SaveFunctionModal-Filed']}>
                <input id="uploadJar" type="file" className={styles['SaveFunctionModal-Filed-InputField']} onChange={handleChosenFilesChange} />
                <label htmlFor="uploadJar" className={styles['SaveFunctionModal-Filed-Label']}>
                  <MdFileUpload />
                  Browse
                </label>
                {chosenFile && (
                  Object.keys(chosenFile).map((file: any) => (
                    <p key={chosenFile[file].name} data-test-id={chosenFile[file].name} className={styles['SaveFunctionModal-ChosenFile-File']}>{chosenFile[file].name}</p>
                  ))
                )}
              </div>
            )}
            {(!chosenFile?.length && !navesFunction) && (
              <AlertMessage customStyles={styles['SaveFunctionModal-AlertMessage']}>
                {FILE_EXTENSION_FOR_JAVA}
                <br />
                {FILE_EXTENSION_FOR_CSHARP}
              </AlertMessage>
            )}
            {(!navesFunction && jarFileIsNotSpecified) && (
              <AlertMessage messageStyle={AlertMessageStyle.WARNING} customStyles={styles['SaveFunctionModal-AlertMessage']}>{jarFileIsNotSpecified}</AlertMessage>
            )}
            {(chosenFile?.length || navesFunction) && (
              <Select
                className={styles['SaveFunctionModal-Select']}
                name="trigger"
                value={selectedTriggerOption}
                onChange={handleTriggerOptionChange}
                options={TRIGGER_OPTIONS}
                placeholder="Select trigger"
              />
            )}
            {(selectedTriggerOption?.label === 'POST' || selectedTriggerOption?.label === 'GET') && (
              <Select
                className={styles['SaveFunctionModal-Select']}
                name="access"
                value={selectedAccessOption}
                onChange={handleAccessOptionChange}
                options={ACCESS_OPTIONS}
                placeholder="Select access"
              />
            )}
            {(selectedTriggerOption?.label === 'BUCKET') && (
              <Select
                className={styles['SaveFunctionModal-Select']}
                name="type-of-bucket"
                value={selectedBucketTypeOption}
                onChange={handleBucketTypeOptionChange}
                options={BUCKET_TYPE_OPTIONS}
                placeholder="Select type of bucket"
              />
            )}
            {(selectedBucketTypeOption?.label && selectedTriggerOption?.label === 'BUCKET') && (
              <Select
                className={styles['SaveFunctionModal-Select_projects']}
                name="project"
                value={selectedProjectOption}
                isMulti
                onChange={handleProjectChange}
                options={projectsOptions}
                placeholder="Select or search project"
                isSearchable
                maxMenuHeight={110}
              />
            )}
            {fieldIsRequired && <AlertMessage messageStyle={AlertMessageStyle.WARNING} customStyles={styles['SaveFunctionModal-AlertMessage']}>{fieldIsRequired}</AlertMessage>}
            <Button type="button" dataTestId="SaveNewFunction" customStyles={styles['SaveFunctionModal-Button']} onClick={hanleSaveFunctionClick}>Save function</Button>
          </Col>
        </Row>
      </Container>
    </Modal>
  );
};

export default SaveFunctionModal;
