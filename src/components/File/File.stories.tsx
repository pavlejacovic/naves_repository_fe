import React from 'react';
import File from './File';

export default {
  title: 'File component',
  component: File
};

export const file = () => (
  <File
    projectId={1}
    dataTestId="44"
    file={
      {
        projectName: 'xxx',
        fileName: 'xxx',
        createdDate: 'xxx',
        filePath: 'xxx',
        fileSize: 55,
        parentProject:
        {
          projectName: 'qqq',
          id: 1,
          numberOfFiles: 4,
          owner: {},
          projectSize: 55
        },
        uploadedBy: 'xxx'
      }
    }
    text="..."
  />
);
