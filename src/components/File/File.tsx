import React, { MouseEvent, useContext, useState } from 'react';
import { toast } from 'react-toastify';
import { Col, Row } from 'react-grid-system';
import { MdMoreHoriz, MdDescription } from 'react-icons/md';
import { File as FileModel } from '../../models/file.interface';
import BoxService from '../../services/box.service';
import fileSaverService from '../../services/file-saver.service';
import {
  getFileExtensionFromFile,
  trimPath
} from '../../utils/functions/stringManipulations/stringManipulations/stringManipulations';
import dateConversion from '../../utils/functions/dateManipulations/dateManipulations';
import { Button } from '../Button/Button';
import DropdownButton from '../DropdownButton/DropdownButton';
import styles from './File.module.scss';
import ConfirmModal from '../ConfirmModal/ConfirmModal';
import { QUESTION_DELETING_FILE } from '../../utils/constants/questions/questions';
import bytesToMegaBytes from '../../utils/functions/unitsConversions/unitsConversions';
import { ProjectPageContext } from '../../Contexts/ProjectPageContext';
import SaveModal from '../SaveModal/SaveModal';
import { AuthorizedPagesContext } from '../../Contexts/AuthorizedPagesContext';

interface FileProps {
  dataTestId: string,
  text?: string,
  file: FileModel,
  projectId: number,
  writePermission?: boolean | undefined
}

const File: React.FC<FileProps> = ({
  dataTestId,
  file,
  projectId,
  writePermission
}): JSX.Element => {
  const { fetchFoldersAndFiles } = useContext(ProjectPageContext);
  const { pathToTheSource } = useContext(AuthorizedPagesContext);
  const fileExtension: string | null = getFileExtensionFromFile(file.fileName);
  const [isConfirmModalVisible, setIsConfirmModalVisible] = useState<boolean>(false);
  const [isRenameFileModalVisible, setIsRenameFileModalVisible] = useState<boolean>(false);

  const handleDownloadFileClick = (e: MouseEvent) => {
    e.preventDefault();
    BoxService.downloadFile(projectId, file.filePath).then(
      (response) => {
        fileSaverService.downloadFile(response.data, file.fileName);
      }
    );
  };

  const handleDeleteButtonClick = (event: MouseEvent) => {
    event.preventDefault();
    setIsConfirmModalVisible(true);
  };

  const handleDeleteFileClick = () => {
    const requestBody = [{ path: file.filePath }];
    if (projectId) {
      BoxService.deleteFileOrFolder(projectId, requestBody).then(
        (response) => {
          toast.info(response.data[0].message);
          setIsConfirmModalVisible(false);
          fetchFoldersAndFiles(pathToTheSource);
        }
      ).catch((error) => toast.error(error.response.data[0].message));
    }
  };

  const handleRenameBtnClick = (e: MouseEvent): void => {
    e.preventDefault();
    setIsRenameFileModalVisible(true);
  };

  const handleRenameFileSave = (
    newName: string,
    setInputText: React.Dispatch<React.SetStateAction<string>>
  ): void => {
    BoxService.renameFileOrFolder(projectId, file.filePath, `${fileExtension ? (`${newName}.${fileExtension}`) : (newName)}`).then((response) => {
      toast.success(response.data);
      setInputText('');
      setIsRenameFileModalVisible(false);
      if (fetchFoldersAndFiles) {
        fetchFoldersAndFiles(pathToTheSource);
      }
    });
  };

  const handleCloseConfirmModalClick = () => setIsConfirmModalVisible(false);

  return (
    <>
      <li data-test-id={dataTestId} className={`${styles['File']} ${styles['File-Link']}`}>
        <Row justify="between" align="center">
          <Col lg={1}>
            <Row justify="end">
              <Col lg={5}><MdDescription /></Col>
            </Row>
          </Col>
          <Col lg={3} className={styles['File-Text']}>{trimPath(file?.filePath)}</Col>
          <Col lg={3} className={styles['File-Text']}>{file?.uploadedBy}</Col>
          <Col lg={3} className={styles['File-Text']}>{dateConversion(file?.createdDate || null)}</Col>
          <Col lg={1} className={styles['File-Text']}>{file?.fileSize ? bytesToMegaBytes(file?.fileSize) : '0 MB'}</Col>
          <Col lg={1}>
            <DropdownButton text={<MdMoreHoriz className={styles['File-ButtonIcon']} />} customStyles={styles['File-Button']}>
              <ul>
                <li>
                  <Button dataTestId="downloadBtn" type="button" customStyles={styles['File-Action']} onClick={handleDownloadFileClick}>Download</Button>
                </li>
                {writePermission
                && (
                  <>
                    <li>
                      <Button dataTestId="renameBtn" type="button" customStyles={styles['File-Action']} onClick={handleRenameBtnClick}>Rename</Button>
                    </li>
                    <li>
                      <Button dataTestId="deleteBtn" type="button" customStyles={styles['File-Action']} onClick={handleDeleteButtonClick}>Delete</Button>
                    </li>
                  </>
                )}
              </ul>
            </DropdownButton>
          </Col>
        </Row>
      </li>
      <SaveModal
        title="Rename file"
        inputPlaceholder="New name"
        modalIsVisible={isRenameFileModalVisible}
        setModalIsVisible={setIsRenameFileModalVisible}
        onSave={handleRenameFileSave}
      >
        <Button type="submit" dataTestId="renameFolderBtn">Rename file</Button>
      </SaveModal>
      <ConfirmModal
        text={QUESTION_DELETING_FILE}
        modalIsVisible={isConfirmModalVisible}
        setModalIsVisible={setIsConfirmModalVisible}
        onConfirm={handleDeleteFileClick}
        onClose={handleCloseConfirmModalClick}
      />
    </>
  );
};

export default File;
