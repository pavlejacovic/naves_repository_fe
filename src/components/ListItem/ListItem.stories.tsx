import React from 'react';
import ListItem from './ListItem';

export default {
  title: 'List Item',
  component: ListItem
};

export const listItemDefault = () => <ListItem dataTestId="test" />;
export const listItemDefaultActive = () => <ListItem dataTestId="test" active />;
