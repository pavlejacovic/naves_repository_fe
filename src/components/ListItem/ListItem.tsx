import React from 'react';
import { Col, Row } from 'react-grid-system';
import { Link } from 'react-router-dom';
import styles from './ListItem.module.scss';

interface ListItemProps {
  dataTestId: string | number,
  active?: boolean,
  linkTo?: string,
  text?: string,
  customStyles?: string;
}

const ListItem: React.FC<ListItemProps> = ({
  dataTestId,
  active,
  linkTo = '',
  text,
  customStyles
}) => (
  <li
    data-test-id={dataTestId}
    className={`${styles['ListItem']} ${active && styles['ListItem_active']} ${customStyles}`}
  >
    <Link to={linkTo} className={`${styles['ListItem-Link']} ${active && styles['ListItem-Link_active']}`}>
      <Row justify="center">
        <Col className={styles['ListItem-Text']}>{text}</Col>
      </Row>
    </Link>
  </li>
);

export default ListItem;
