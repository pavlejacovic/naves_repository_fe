import React from 'react';
import styles from './Select.module.scss';

interface SelectProps {
  onChange?: (event: React.ChangeEvent<HTMLSelectElement>) => void;
  id: string;
  customStyle?: string;
}

const Select: React.FC<SelectProps> = ({
  onChange,
  children,
  id,
  customStyle
}) => (
  <select onChange={onChange} className={`${styles.Select} ${customStyle}`} id={id}>
    {children}
  </select>
);

export default Select;
