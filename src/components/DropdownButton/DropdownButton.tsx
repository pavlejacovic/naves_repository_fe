import React, {
  MouseEvent, ReactNode, useRef, useState
} from 'react';
import useOutsideClick from '../../utils/hooks/useOutsideClick';
import { Button, ButtonStyle } from '../Button/Button';
import styles from './DropdownButton.module.scss';

export interface DropdownButtonProps {
  text: ReactNode;
  customStyles?: string;
}

const DropdownButton: React.FC<DropdownButtonProps> = ({ text, customStyles, children }): JSX.Element => {
  const [actionsVisible, setActionsVisible] = useState<boolean>(false);

  const ref = useRef(null);

  const handleSetActionsVisibilityClick = (e: MouseEvent) => {
    e.preventDefault();
    setActionsVisible(!actionsVisible);
  };

  useOutsideClick(ref, () => {
    if (actionsVisible) {
      setActionsVisible(false);
    }
  });

  return (
    <div className={styles['DropdownButton']}>
      <Button type="button" onClick={handleSetActionsVisibilityClick} customStyles={customStyles} buttonStyle={ButtonStyle.SECONDARY} dataTestId="dropdownBtn">{text}</Button>
      {
        actionsVisible && (
          <div ref={ref} className={styles['DropdownButton-Actions']}>
            {children}
          </div>
        )
      }

    </div>
  );
};

export default DropdownButton;
