import React from 'react';
import ListItem from '../ListItem/ListItem';
import DropdownButton from './DropdownButton';

export default {
  title: 'DropdownButton',
  component: DropdownButton
};

export const DropdownExample = () => (
  <div>
    <DropdownButton text="Click">
      <ul>
        <ListItem dataTestId="opt1">Option 1</ListItem>
        <ListItem dataTestId="opt1">Option 2</ListItem>
        <ListItem dataTestId="opt1">Option 3</ListItem>
      </ul>
    </DropdownButton>
    <p>Random text below the button</p>
  </div>
);
