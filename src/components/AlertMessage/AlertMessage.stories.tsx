import React from 'react';
import AlertMessage, { AlertMessageStyle } from './AlertMessage';

export default {
  title: 'AlertMessage',
  component: AlertMessage
};

export const generalMessage = () => (
  <AlertMessage>Hello World</AlertMessage>
);
export const warningMessage = () => (
  <AlertMessage messageStyle={AlertMessageStyle.WARNING}>Hello World</AlertMessage>
);
export const successMessage = () => (
  <AlertMessage messageStyle={AlertMessageStyle.SUCCESS}>Hello World</AlertMessage>
);
