import React from 'react';
import styles from './AlertMessage.module.scss';

export enum AlertMessageStyle {
  WARNING = 'AlertMessage_warning',
  SUCCESS = 'AlertMessage_success'
}

interface AlertMessageProps {
  messageStyle?: AlertMessageStyle,
  customStyles?: string
}

const AlertMessage:React.FC<AlertMessageProps> = ({ messageStyle, children, customStyles }):JSX.Element => (
  <p className={`${styles['AlertMessage']} ${messageStyle && styles[messageStyle] ? styles[messageStyle] : ''} ${customStyles}`}>
    {children}
  </p>
);

export default AlertMessage;
