import React from 'react';
import { Redirect, Route, RouteProps } from 'react-router-dom';
import LocalStorageService from '../services/local-storage.service';
import AuthenticationLayout from '../layouts/AuthenticationLayout/AuthenticationLayout';

const AuthenticationRoute: React.FC<RouteProps> = (props) => {
  if (LocalStorageService.hasToken()) {
    return <Redirect to="/" />;
  }
  return (
    <AuthenticationLayout>
      <Route {...props} />
    </AuthenticationLayout>
  );
};

export default AuthenticationRoute;
