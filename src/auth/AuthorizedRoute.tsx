import React from 'react';
import { Redirect, Route, RouteProps } from 'react-router-dom';
import AuthorizedPagesContext from '../Contexts/AuthorizedPagesContext';
import PageWrapper from '../layouts/PageWrapper/PageWrapper';
import LocalStorageService from '../services/local-storage.service';

const AuthorizedRoute: React.FC<RouteProps> = (props) => {
  if (!LocalStorageService.hasToken()) {
    return <Redirect to="/login" />;
  }
  return (
    <AuthorizedPagesContext>
      <PageWrapper>
        <Route {...props} />
      </PageWrapper>
    </AuthorizedPagesContext>
  );
};

export default AuthorizedRoute;
