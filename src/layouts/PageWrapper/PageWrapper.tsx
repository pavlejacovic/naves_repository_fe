import React from 'react';
import NavigationBar from '../../components/NavigationBar/NavigationBar';
import styles from './PageWrapper.module.scss';

interface PageWrapperProps {
  children: any
}

const PageWrapper: React.FC<PageWrapperProps> = ({ children }): JSX.Element => (
  <div className={styles['PageWrapper']}>
    <NavigationBar />
    {children}
  </div>
);

export default PageWrapper;
