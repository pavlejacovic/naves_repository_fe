import React from 'react';
import logo from '../../assets/img/logo.png';
import styles from './AuthenticationLayout.module.scss';

interface AuthenticationLayoutProps {
  children: any;
}

const message = 'Naves runs client-written code, so the user does not have to maintain and set up the server-side of the application. We provide a serverless solution that allows you to run code without provisioning or managing servers, maintaining event integrations and runtime management. We enable storing and editing of data as well as sharing it with others. Within the Naves box, the data is immediately accessible. We offer a variety of subscription programs that provide you with individualized service tailored to your specific needs.';

const AuthenticationLayout = ({ children }: AuthenticationLayoutProps) => (
  <div className={styles['AuthenticationLayout']}>
    <div className={styles['AuthenticationLayout-Left']}>
      <div className={styles['AuthenticationLayout-About']}>
        <img className={styles['AuthenticationLayout-Logo']} alt="logo" src={logo} />
        <span className={styles['AuthenticationLayout-Description']}>
          {message}
        </span>
      </div>
    </div>
    <div className={styles['AuthenticationLayout-Right']}>
      {children}
    </div>
  </div>
);

export default AuthenticationLayout;
