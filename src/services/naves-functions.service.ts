import { AxiosResponse } from 'axios';
import http from '../api/axiosHttpClient.instance';
import { NavesFunctionInfo } from '../models/function.interface';

class NavesFunctionsService {
  private static instance: NavesFunctionsService;

  private constructor() { }

  public static getInstance(): NavesFunctionsService {
    if (!NavesFunctionsService.instance) {
      NavesFunctionsService.instance = new NavesFunctionsService();
    }
    return NavesFunctionsService.instance;
  }

  public getAllMyFunctions = (): Promise<AxiosResponse<Array<NavesFunctionInfo>>> => http.get('/lambda/functions');

  public fetchAllMyFunctions = (): Promise<AxiosResponse<Array<NavesFunctionInfo>>> => http.get('/lambda/functions');

  public createNewNavesFunction = (data: NavesFunctionInfo): Promise<AxiosResponse<any>> => {
    const formData = new FormData();
    formData.append('functionName', data.functionName);
    if (data.language) {
      formData.append('language', data.language);
    }
    if (data.archiveFile) {
      formData.append('archiveFile', data.archiveFile);
    }
    formData.append('trigger', data.trigger);
    formData.append('publicAccess', data.publicAccess.toString());
    if (data.projectIds) {
      formData.append('projectIds', data.projectIds.toString());
    }
    return http.post('/lambda/', formData);
  };

  public updatePropsForNavesFunction = (id: number, data: NavesFunctionInfo): Promise<AxiosResponse<any>> => {
    const formData = new FormData();
    formData.append('functionName', data.functionName);
    formData.append('trigger', data.trigger);
    formData.append('publicAccess', data.publicAccess.toString());
    if (data.projectIds) {
      formData.append('projectIds', data.projectIds.toString());
    }
    return http.put(`/lambda/functions/properties/${id}`, formData);
  };

  public deleteNavesFunction = (id: number): Promise<AxiosResponse<any>> => http.delete(`/lambda/functions/${id}`);

  public generateNewLink = (id: number): Promise<AxiosResponse<any>> => http.patch(`/lambda/functions/${id}/link`);

  public getFunctionsPerPage = (size: number, page: number, sort: string): Promise<AxiosResponse<any>> => http.get(`/lambda/functions/page/?size=${size}&page=${page}&sort=${sort}`)

  public updateNavesFunction = (id: number, file: string | Blob, language: string): Promise<AxiosResponse<any>> => {
    const formData = new FormData();
    formData.append('archiveFile', file);
    formData.append('language', language);
    return http.put(`/lambda/functions/execution/${id}`, formData);
  };

  public getStatisticForPreviousDays = (id: number, days: number): Promise<AxiosResponse<any>> => http.get(`/lambda/statistics/${id}/${days}`);

  public getNumberOfAllFunctions = (): Promise<AxiosResponse<any>> => http.get('/lambda/admin');
}

export default NavesFunctionsService.getInstance();
