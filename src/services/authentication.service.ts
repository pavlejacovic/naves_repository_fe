import { AxiosResponse } from 'axios';
import {
  OldAndNewPassword,
  ReceivedUsersInfo,
  Token,
  UserInfo
} from '../models/user.interface';
import http from '../api/axiosHttpClient.instance';

class AuthenticationService {
  private static instance: AuthenticationService;

  private constructor() { }

  public static getInstance(): AuthenticationService {
    if (!AuthenticationService.instance) {
      AuthenticationService.instance = new AuthenticationService();
    }
    return AuthenticationService.instance;
  }

  public registerUser = (user: UserInfo): Promise<AxiosResponse<any>> => http.post('/auth/register', user);

  public verifyUser = (token: Token): Promise<AxiosResponse<any>> => http.post('/auth/confirm-account', token);

  public loginUser = (user: UserInfo): Promise<AxiosResponse<any>> => http.post('/auth/authenticate', user);

  public forgotPassword = (user: UserInfo): Promise<AxiosResponse<any>> => http.post('/auth/forgot-password', user);

  public resetPassword = (user: UserInfo): Promise<AxiosResponse<any>> => http.post('/auth/reset-password', user);

  public isAuthenticated = (): Promise<AxiosResponse<any>> => http.get('/auth/hello');

  public getUserInformation = (): Promise<AxiosResponse<any>> => http.get('/auth/profile');

  public postUserInformation = (data: ReceivedUsersInfo): Promise<AxiosResponse<any>> => http.put('/auth/profile', data);

  public changePassword = (data: OldAndNewPassword): Promise<AxiosResponse<any>> => http.put('/auth/change-password', data);

  public getNumberOfClients = ():Promise<AxiosResponse<any>> => http.get('/auth/admin');

  public getUserType = ():Promise<AxiosResponse<any>> => http.get('auth/role');
}

export default AuthenticationService.getInstance();
