class LocalStorageService {
  private static instance: LocalStorageService;

  private constructor() { }

  public static getInstance(): LocalStorageService {
    if (!LocalStorageService.instance) {
      LocalStorageService.instance = new LocalStorageService();
    }
    return LocalStorageService.instance;
  }

  public hasToken = (): boolean => !!localStorage.getItem('token');

  public getToken = (): string => {
    const data: string | null = localStorage.getItem('token');
    if (data !== null) {
      return data;
    }
    return '';
  };

  public setToken = (data: string): void => localStorage.setItem('token', data);

  public removeToken = (): void => localStorage.removeItem('token');
}

export default LocalStorageService.getInstance();
