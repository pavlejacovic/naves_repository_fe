import { AxiosResponse } from 'axios';
import http from '../api/axiosHttpClient.instance';
import {
  FilePath,
  Project,
  ProjectContent,
  ProjectInfo
} from '../models/project.interface';
import { FilesForUpload, File } from '../models/file.interface';
import { Folder } from '../models/folder.interface';

class BoxService {
  private static instance: BoxService;

  private constructor() { }

  public static getInstance(): BoxService {
    if (!BoxService.instance) {
      BoxService.instance = new BoxService();
    }
    return BoxService.instance;
  }

  public createNewProject = (projectName: string): Promise<AxiosResponse<any>> => http.post('/box/projects', { projectName });

  public createDirectoryInsideProject = (path: string, folderName: string, projectId: number): Promise<AxiosResponse<any>> => http.post(`box/directories/${projectId}`,
    {
      path: path,
      folderName: folderName
    })

  public fetchAllMyProjects = (): Promise<AxiosResponse<Project[]>> => http.get('/box/projects');

  public deleteProject = (id: number): Promise<AxiosResponse<any>> => http.delete(`/box/projects/${id}`);

  public deleteFileOrFolder = (id: number, path: Array<{ path: Folder['folderPath'] | File['filePath'] }>): Promise<AxiosResponse<any>> => http.delete(`/box/projects/${id}/files`, { data: path });

  public renameProject = (projectId: number, projectName: string): Promise<AxiosResponse<any>> => http.put(`/box/projects/${projectId}`, { projectName });

  public renameFileOrFolder = (projectId: number, path: string, newName: string): Promise<AxiosResponse<any>> => http.put(`/box/projects/${projectId}/files`, { path, newName });

  public getAllSharedWithMeProjects = (): Promise<AxiosResponse<Project[]>> => http.get('box/projects/shared');

  public getProjectsPerPage = (size: number, page: number, sort: string): Promise<AxiosResponse<any>> => http.get(`box/projects/page?size=${size}&page=${page}&sort=${sort}`)

  public getSharedWithMeProjectsPerPage = (size: number, page: number, sort: string): Promise<AxiosResponse<any>> => http.get(`box/projects/shared/page?size=${size}&page=${page}&sort=${sort}`)

  public downloadAsZip = (projectId: number, paths: FilePath[]): Promise<AxiosResponse<any>> => http.post(`/box/projects/${projectId}/files/zip`,
    paths,
    {
      responseType: 'blob'
    });

  public downloadFile = (projectId: number, path: string): Promise<AxiosResponse<any>> => http.post(`/box/projects/${projectId}/file`,
    { path },
    {
      responseType: 'blob'
    });

  public fetchAllFoldersAndFilesInProject =
    (pathToTheSource: Array<string>): Promise<AxiosResponse<ProjectContent>> => {
      if (pathToTheSource.length === 1) {
        return http.post(`/box/projects/${pathToTheSource[0]}/folders`, { path: '/' });
      }
      return http.post(`/box/projects/${pathToTheSource[0]}/folders`, { path: pathToTheSource[1] });
    };

  public getProjectSpace = (projectId: number, path: string): Promise<AxiosResponse<any>> => http.post(`/box/check/${projectId}`, { path: path });

  public uploadFiles = (projectId: number, data: FilesForUpload): Promise<AxiosResponse<any>> => {
    const formData = new FormData();
    data.files.forEach((file: any) => {
      formData.append('files', file);
    });
    formData.append('path', data.path);
    return http.post(`http://localhost:9191/box/upload/${projectId}`, formData);
  };

  public shareProject = (id: number, data: Array<{ email: string, permission: string }>): Promise<AxiosResponse<any>> => http.post(`/box/projects/${id}/share`, data);

  public getProjectUsers = (id: number): Promise<AxiosResponse<any>> => http.get(`/box/projects/${id}/share`);

  public getProjectsForAdmin = (): Promise<AxiosResponse<ProjectInfo[]>> => http.get('/box/admin/projects');

  public getProjectsForAdminPerPage = (page: number, size: number): Promise<AxiosResponse<ProjectInfo[]>> => http.get(`/box/admin/projects/page?page=${page}&size=${size}`)

  public getNumberOfAllProjects = ():Promise<AxiosResponse<any>> => http.get('/box/admin');

  public getSharedWithProjectForAdmin = (projectId: number):Promise<AxiosResponse<any>> => http.get(`/box/admin/shared-with/${projectId}`);
}

export default BoxService.getInstance();
