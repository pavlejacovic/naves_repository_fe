class FileSaver {
  private static instance: FileSaver;

  private constructor() { }

  public static getInstance(): FileSaver {
    if (!FileSaver.instance) {
      FileSaver.instance = new FileSaver();
    }
    return FileSaver.instance;
  }

  public downloadZip = (data: any, fileName: string): void => {
    const downloadUrl = window.URL.createObjectURL(new Blob([data]));
    const link = document.createElement('a');
    link.href = downloadUrl;
    link.setAttribute('download', `${fileName}.zip`);
    document.body.appendChild(link);
    link.click();
    link.remove();
  };

  public downloadFile = (data: any, fileName: string): void => {
    const downloadUrl = window.URL.createObjectURL(new Blob([data]));
    const link = document.createElement('a');
    link.href = downloadUrl;
    link.setAttribute('download', `${fileName}`);
    document.body.appendChild(link);
    link.click();
    link.remove();
  };
}

export default FileSaver.getInstance();
